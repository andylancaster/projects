# The program works by calculating the number of people that have entered the train and left the train at each station. By 
# calculating the number of people on the train at each station we then know the spaces remaining on the train. The two 
# calculations are then used to check against any impossible scenarios.

# 
# Example of how to read a train?.txt file.  The individual lines are 
# read into a list of lists.  This program just prints the list, but your
# program should process the list to find whether the observed counts are 
# possible or not.
#
# To process the file train1.txt, invoke the program as "python trains.py train1.txt"

from sys import argv, exit
from read_data import read_data

# argv contains the arguments that the program was invoked with
# If this is run as "python trains.py train1.txt" then argv[1] contains
# the string "train1.txt". (argv[0] contains "train.py")
if len(argv) < 2:
    print("No data file given")
    exit(1)                     # Exit from the program now
        
data, capacity = read_data(argv[1])

# get number of stations
no_of_stations = len(data)

#print("Train capacity", capacity)

def traindata():
    
    """Check if the train data fed into the function is possible or impossible."""
    
    n = 1
    
    capacity_at_station = 0;
    
    # Print the data, but your program should do something with it.
    for station in data:
        left = station[0]
        embarked = station[1]
        waiting = station[2]
        #print('Left', left, '  Entered', embarked, '  Waiting', waiting)

        # add people entered minus people that have left at the station
        capacity_at_station = capacity_at_station + embarked - left
        # calculate space remaining at each stop
        space_left = capacity - capacity_at_station  
        
        # if there are people waiting and space is available - waited in vain
        if waiting > 0 and space_left >= waiting:
            return False
               
        # if at first station and left is greater than 0
        if n == 1 and left > 0:
            return False
        
        # if passagers are waiting at the final station
        if n == no_of_stations and waiting > 0:
            return False
        
        # if passengers remain on the train at the final station
        if n == no_of_stations and capacity_at_station > 0:
            return False
        
        # if people waiting exceeds people entered
        if waiting > embarked:
            return False
        
        # if capacity at station is greater than train capacity or less that 0
        if capacity_at_station > capacity or capacity_at_station < 0: 
            return False
        
        # increase number of stops
        n = n + 1
        

# run function
if traindata() == False:
    print('impossible')
else:
    print('possible')
    
