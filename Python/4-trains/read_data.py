
def read_data(filename):
    """
    Read train data from the given file.
    Returns a list containing data for all stations and the capacity of the train.
    """
    with open(filename, 'r') as f:
        # Read the first line
        first = f.readline()
        C, n = first.split()
        C = int(C)
        n = int(n)
        
        # And the rest of the lines
        data = []
        for i in range(n):
            line = f.readline()
            data.append([int(i) for i in line.split()])
    return data, C


