from anagram import *
from sys import argv

def blanagram(word, anagramdict):
    
    """Returns a list of the blanagrams of a given word.
    
    word - str - word to be checked for blanagrams
    anagramdict - list - list of anagrams built from function make_anagram_dict in anagram.py
    """

    # convert word to list
    word_letters = list(word)
    # letters to replace each letter in word
    alphabet = list('abcdefghijklmnopqrstuvwxyz')
    # list for blanagrams
    blanagram = []
    # number of word letter to be changed by letter in alphabet
    n = 0
    
    for l in word:
        
        # loop through each letter in alphabet for each letter in word
        for a2z in alphabet:
            
            # replace letter in word with letter in alphabet
            word_letters[n] = a2z
            
            # arrange word alphabetically and join list
            word_sorted = "".join(sorted(word_letters))         
            
            # check if new word with replacement exists as a key in anagramdict
            try:
                blanagram.append(anagramdict[word_sorted])
            except:
                None
            
            # reset word to original to check on next loop
            word_letters = list(word)
        
        # increment letter
        n += 1
    
    # remove duplicate values from blanagram
    blanagram_strip = []
    for b in blanagram:
        for word in b:
            if word not in blanagram_strip:
                blanagram_strip.append(word)     
        
    return(sorted(blanagram_strip))


def largest_variants_by_word_length(wordlen, anagramdict):

    """Find blanagrams with most variants by word length.
    
    word - str - word length to be checked for blanagrams
    anagramdict - list - list of anagrams built from function make_anagram_dict in anagram.py
    """    
    
    # previous blanagram length
    prev_blanagram_len = 0
    
    # variants dictionary; key = len, value = list of words matching length
    var = {}
    
    # loop throught anagram dictionary
    for w in anagramdict:

        # match user length with word length
        if len(w) == wordlen:

            # feed word length e.g. 7 letter word into blanagram function
            blanagrams = blanagram(w, anagramdict)
            
            # get number of variants
            blanagrams_len = len( blanagrams )
            
            # check variants against previous
            if blanagrams_len >= prev_blanagram_len:
                # if word length already exists add matching length to dictionary
                try:
                    var[blanagrams_len].append(anagramdict[w])
                # add new key with variant length    
                except:
                    var[blanagrams_len] = [anagramdict[w]]
                    
                # set new longest length
                prev_blanagram_len = blanagrams_len

    # return words with most variants
    l = ''
    for wv in var[prev_blanagram_len]:
        l += wv[0] + '\n'
        
    return(l)

if __name__ == "__main__":
    
    # check word length arguement has been included in the cli
    if len(argv) > 1:
        
        # import words into dictionary
        dictionary = fileimport('words.txt')    
        anagram_dic = make_anagram_dict(dictionary)            
        
        # blanagram length in int
        uinput_word_len = int(argv[1])
        
        print('Blanagrams with most variants for', argv[1], 'letters:\n' + largest_variants_by_word_length(uinput_word_len, anagram_dic))
        
    else:
        
        print('Error! No word length specified.')    
    
