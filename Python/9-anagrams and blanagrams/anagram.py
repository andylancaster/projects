def fileimport(filename):
    
    """Import a text file, strip any new lines, set words to lowercase, 
    append each word into a dictionary and return a list of dictionary keys. 
    Dictionary used to ensure duplicate words are not imported.
    
    Arguments
    filename - str - filename to imported
    """

    file = open(filename, 'r')
    lines = file.readlines()
    words = {}
    # strip new lines from file and add to words to dictionary to avoid duplicates
    for word in lines:
        w = word.lower().strip()
        words[w] = None 

    return list( words.keys() )

def make_anagram_dict(words):
    
    """Build and return a dictionary of anagram words from an given list of words
    
    Arguments
    words - list - list of words with possible anagrams or blanagrams
    """    
    
    sortedwords = {}
    
    for word in words:
        
        # arrange word alphabetically and join list
        wordsorted = "".join(sorted(word))
            
        #try appending to key if alphabetically arranged word/anagram alreay exists in dictionary
        try:    
            sortedwords[wordsorted].append(word)
        # add new key of alphabetic arranged word    
        except:
            sortedwords[wordsorted] = [word]   
    
    return sortedwords

def largest_variants(indictionary):
    
    """Find most anagram variants in dictionary
    
    Arguments
    indictionary - list - list built from function make_anagram_dict
    """
    
    # set previous anagram length
    prev_anagramlen = 0
    # append next largest variant anagram to list
    mostanagram = []
    
    for word in indictionary:
    
        # get length of variants for current dictionary key
        anagramlen = len(indictionary[word])
        
        # if more variants than previous add to list
        if anagramlen >= prev_anagramlen:
            mostanagram.append(indictionary[word])
            prev_anagramlen = anagramlen
    
    # loop through largest variants and print only largest variants
    l = ''
    for w in mostanagram:
        
        if len(w) == prev_anagramlen:
            
            l += str(w) + '\n'
            
    return(l)

def longest_anagram(indictionary):    
    
    """Find lost anagram in dictionary
    
    Arguments
    indictionary - list - list of anagrams built from function make_anagram_dict
    """
    
    # set previous anagram length
    prev_wordlen = 0
    
    for word in indictionary:
        
        # get word length
        wordlen = len(word)
        
        # length greater than previous and a pair of anagrams at minimum
        if wordlen > prev_wordlen and len(indictionary[word]) > 1:
            longestanagram = indictionary[word]
            prev_wordlen = wordlen
            
    return(longestanagram)
    
if __name__ == "__main__":
    
    dictionary = fileimport('words.txt')    
    anagrams = make_anagram_dict(dictionary)
    
    print('Most variants:\n', largest_variants(anagrams))
    
    print('Longest anagram:\n', longest_anagram(anagrams))