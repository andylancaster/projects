from anagram import *
from blanagram import *

def anagram_blanagram_query(anagramdict):
    
    """Prompt the user to enter word(s) to print anagrams and blanagrams for given word. 
    Quit the prompt by entering an empty value
    
    Arguments
    anagramdict - list - list of anagrams built from function make_anagram_dict in anagram.py
    """
    
    while True:
        
        try:
            # prompt for user input
            uinput = input("Type your word?")
            if uinput == '':
                return False
            else:
                
                # sort user input to find dictionary key
                wordort = "".join(sorted(uinput))
                
                # print anagrams
                anagrams = anagramdict[wordort]
                
                print( 'Anagrams: ' + '\t'.join(anagrams) )
                    
                # check for blanagrams and remove any that already exist in anagrams
                blanagrams = blanagram(uinput, anagramdict)
                blanagrams_minus_anagrams = []
                # remove anagrams from blanagrams result
                for b in blanagrams:
                    if b not in anagrams:
                        blanagrams_minus_anagrams.append(b)     
                        
                print( 'Blanagrams: ' + '\t'.join(blanagrams_minus_anagrams) )
                
        except:
            continue


# build anagram dictionary            
dictionary = fileimport('words.txt')
anagram_dic = make_anagram_dict(dictionary)            

anagram_blanagram_query(anagram_dic)            