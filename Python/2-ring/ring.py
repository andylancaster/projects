#  Simple test and demonstration of the exturtle code.

from exturtle import *          # Import all the exturtle functions 

from math import cos, sin

# draw a star
def star(turtle, x, y, points, R, r):
    
    """Draw a star.
    
    Arguments:
    turtle -- the turtle
    x -- number for x axis of starting position
    y -- number for y axis of starting position
    points -- number of points on the star
    R -- outer radius of star
    r -- inner radiu of star
    """    
    
    # go to x,y
    penup(turtle)
    goto(turtle, x, y)    
    pendown(turtle)    
        
    # angle
    a = 2*3.14159/points
    
    color(turtle, "#FFCC00")
    
    for n in range(points+1):
        
        # go to outer point with penup
        if n == 0:
            penup(turtle)
        else:
            pendown(turtle)
        
        # get x, y of outer circle for each point + the offset of x,y
        Px = R*cos(a*n)+x
        Py = R*sin(a*n)+y
        
        # get x, y of inner circle for each point + the offset of x,y
        a_i = r*cos((n + 0.5)*a)+x
        b_i = r*sin((n + 0.5)*a)+y
        
        # go to each x, y
        goto(turtle, Px, Py)
        goto(turtle, a_i, b_i)
    
# draw a row of stars
def row(turtle):
    
    """Draw a row of stars.
    
    Arguments:
    turtle -- the turtle
    """        
    
    for i in range(4):
        
        # position of x axis for each star
        movex = -220+(150*i)

        star(turtle, movex, 0, 5+i, 60, 30)
    

# draw a ring of stars    
def ring(turtle, cx, cy, Nstars, radius, points, R, r):

    """Draw a star.
    
    Arguments:
    turtle -- the turtle
    cx -- number for x axis for the centre of the ring
    cy -- number for y axis for the centre of the ring
    Nstars -- number of stars in the ring
    points -- number of points on the star
    R -- outer radius of star
    r -- inner radiu of star
    """    
	
    # angle
    a = 2*3.14159/Nstars
    
    for n in range(Nstars):
        
        # get x, y of outer circle for each point + the offset of x,y
        Px = radius*cos(a*n)+cx
        Py = radius*sin(a*n)+cy
        
        star(turtle, Px, Py, points, R, r)
    
    
bob = Turtle()
ts = getscreen(bob)
ts.bgcolor("#003399")

# print a row of stars
row(bob)

# turtle, center of x, center of y, no. of stars, ring radius, no. of points in star, outer star radii, inner star radii
ring(bob, 0, 0, 12, 300, 5, 60, 30)

mainloop()