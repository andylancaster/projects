import oracle

oracle = oracle.Oracle()

def highlow(tolerance):
    
    """Guess a random number within the set tolerance
    
    Argument
    tolerance - int or float - allowed tolerance the guess must be within
    """
    
    # set inital number
    L = [0, 100]
    guess = (L[0] + L[1]) / 2 

    # set current tolerance
    current_tolerance = 101

    while current_tolerance > tolerance:

        if oracle.is_greater(guess):
            # increase guess
            # e.g. 50, 100
            L[0] = guess
        else:
            # reduce guess
            # e.g. 0, 49
            L[1] = guess
        
        # calculate new guess and tolerance
        guess = (L[0] + L[1]) / 2
        current_tolerance = L[1] - L[0]
        
    return L  

# guess and oracle number
bracket = highlow(0.001)

print('Bracket:', bracket, 'Oracle number:', oracle.reveal())