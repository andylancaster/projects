def pig(word):
    
    """Translate english word(s) into pig latin
    
    Arguements
    word - str - english word(s) to be translated
    """
    
    # define vowels
    vowels = ['a', 'e', 'i', 'o', 'u']
    
    for i, w in enumerate(word):
        
        # y in the middle of a word
        if i != 0 and w == 'y':
            newword = (word[i:] + word[0:i] + 'ay')
            break
        
        # if vowel in word
        if w in vowels:
            # if leading letter a vowel
            if i == 0:
                newword = (word[0:] + 'way')
            # else use position and slice word    
            else:
                newword = (word[i:] + word[0:i] + 'ay')
            break;
    
    return newword

def test_pig(words, pigwords):
    
    """Test the pig latin translation with a list of english words and a list of the expected translation
    
    Arguements
    words - list - a list of words to be translated
    pigwords - list - the expected result of each word
    """
    
    for w, pw in zip(words, pigwords):
        transPigWord = pig(w)
        assert transPigWord == pw
        print ('Original: ' + w + ', Example: ' + pw + ', Translated: ' + transPigWord) 
    
def pig_translate():
    
    """Prompt the user to enter word(s) to be translated to pig latin. 
    Quit the prompt by entering an empty value"""
    
    while True:
        
        try:
            # prompt for user input
            uinput = input("Enter phrase to translate to pig latin?")
            if uinput == '':
                return False
            else:
                words = uinput.lower().split()
                PigLatinPhase = ''
                
                for word in words:
                    # translate and join each word together
                    PigLatinPhase = PigLatinPhase + ' ' + pig(word)

                print('\n' + PigLatinPhase.strip() + '\n')
                
        except:
            
            continue

    
w = ['happy', 'duck', 'glove', 'evil', 'eight', 'yowler', 'crystal']
p = ['appyhay', 'uckday', 'oveglay', 'evilway', 'eightway', 'owleryay', 'ystalcray']

#test_pig(w, p)
            
if __name__ == "__main__":            
    pig_translate()