from sys import argv

def histogram(x, percentage):
    
    """Draw a histogram from a list of numbers versus the numbers in x.
    The length of both lists must be the same.
    
    Arguments
    
    x - list - numbers in y-axis
    percentage - list - list of numbers to be calculated into percentages
    """
    
    # total of numbers in percentage arguement
    total = sum(percentage)
    
    for n, p in zip(x, percentage):
        
        # calculate percentage value for * and percentage to 4 decimal points of each number in list
        vpercentage = int(p/total*100)
        epercentage = round(p/total*100, 4)    
        print(n, '*'*vpercentage, "Occurrences: ", p, "Percentage: ", epercentage, "%")

        
def histogram_lenghts(L):
    
    """From L produce two lists, one of the length of values in L, one of number of occurrences of that length
    
    Arguments
    L - list - list of words
    """
    
    # use dictionary to define keys e.g. length of word
    H = {}
    for words in L:
        
        wordlen = len(words)
        
        # is legth of word already in dictionary
        if wordlen in H:
            # add 1 to existing directionary key
            H[wordlen] += 1
        else:
            # set directory key and set value to 1
            H[wordlen] = 1
    
    # seperate the directory keys and values in two lists. word lengths and word occurrences
    lengths = []
    occurrences = []
    for h in H:
        lengths.append(h)
        occurrences.append(H[h])
    
    return lengths, occurrences    


def fileimport(filename):
    
    """Import a text file, strip any new lines, append each word into a list and return a list of words.
    
    Arguments
    filename - filename to imported
    """

    file = open(filename, 'r')
    lines = file.readlines()
    words = []
    # strip new lines from file and add to words
    for word in lines:
        w = word.strip()
        words.append(w)   

    return words


if __name__ == "__main__":
    
    # check if a text file has been included in the cli
    if len(argv) > 1:
        words = fileimport(argv[1])
        # return variables for length and occurrences for file included
        wordlengths, wordlengthoccurrences = histogram_lenghts(words)
        # draw histogram
        histogram(wordlengths, wordlengthoccurrences)
    else:
        print('Error! No text file included.')