from histogram import histogram

import random

def add_to_11(visible):
    
    """Check if the list contains any two pairs within a list of intergers totaling to 11
        
    Arguments
    visible - list - list of facing cards
    """
    
    # cards to exclude
    jqkcards = [11,12,13]
    t = ()
    is11 = 0
    c1 = 0 # 1st card
    c2 = 0 # 2nd card
    while is11 != 11:
        
        # prevent same card adding together
        if c1 != c2:
            is11 = visible[c1] + visible[c2]
            t = (c1,c2)
        
        c2 += 1
        
        # if at the end of a list, go to next index and begin again
        if c2 == len(visible):
            c1 += 1 # increment the 1st card
            c2 = 0 # reset the 2nd card
        
        # if at the end of visible then return a blank tuple
        if c1 == len(visible):
            t = ()
            break
        
    return t

def jqk(visible):
    
    """Check if the list contains a jack, queen and king
    
    Arguments
    visible - list - list of facing cards
    """

    jqkcards = [11,12,13]
    t = ()
    jqkin = []
    for i,c in enumerate(visible):
        
        # if card is a jack,queen,king and not already in tuple
        if c not in jqkin and c in jqkcards:
            t += (i,)
            jqkin += (c,)

        # if tuple contains all 3 cards    
        if len(t) == 3:
            return(t)
        
def play(deck, verbose):
    
    """Play a game of Patience. 
    
    Arguments
    deck - list - deck of 52 cards. 1 - 13. 1 = Ace, 11,12,13 = Jack, Queen, King
    verbose - boolean - True or False. Print visible cards for each hand.
    """

    random.shuffle(deck)
    # cards dealt
    facing = []
    # card number being dealt
    c = 0
    # cards remaining
    cardsremaining = len(deck)
    while True:
           
        # if pile is greater than 9 or there a no more cards in the deck
        if len(facing) > 9 or cardsremaining == 0:
            break        
        elif jqk(facing):
            if cardsremaining >= 3:
                # change indexes matching jack,queen,king to next 3 cards
                cardstocover = jqk(facing)
                deck[cardstocover[0]] = deck[c]
                deck[cardstocover[1]] = deck[c+1]
                deck[cardstocover[2]] = deck[c+2]
                c += 3
                facing = deck[:len(facing)]
            else:
                # if remaining card exceeds 9
                facing.append(deck[c])
                c += 1              
        elif add_to_11(facing):
            if cardsremaining >= 2:
                # change indexes matching 11 to next 2 cards
                cardstocover = add_to_11(facing)
                deck[cardstocover[0]] = deck[c]
                deck[cardstocover[1]] = deck[c+1]
                c += 2
                facing = deck[:len(facing)]
            else:
                facing.append(deck[c])
                c += 1
        else:
            facing.append(deck[c])
            c += 1

        # cards still to be dealt
        if c <= 52:
            cardsremaining = len(deck) - c;

            
        if verbose:
            p = ' '.join(map(str, facing))
            print(p)
       
    return cardsremaining

def many_plays(N):
    
    """Play multiple games of Patience and return a list of remaining card occurrences
    
    Arguments
    N - int - number of times to play the game
    """
    
    # set a list with the number of possible cards remaining
    remaining = []
    for i in range(53):
        remaining.append([i, 0])
    
    # play the game N number of times
    for n in range(N):
        
        deck = [1,2,3,4,5,6,7,8,9,10,11,12,13,1,2,3,4,5,6,7,8,9,10,11,12,13,1,2,3,4,5,6,7,8,9,10,11,12,13,1,2,3,4,5,6,7,8,9,10,11,12,13]
        random.shuffle(deck)

        # return number of cards remain from each game
        noremainingcards = play(deck, False)
        # add remaing card count to remaining list
        remaining[noremainingcards][1] += 1
    
    # split remaining list into two lists to feed into histogram. card count remaining and number of times it occurred
    remaining_k = []
    remaining_v = []
    for r in remaining:
        
        remaining_k.append(r[0])
        remaining_v.append(r[1])
    
    return remaining_k, remaining_v  
    
cards = [1,2,3,4,5,6,7,8,9,10,11,12,13,1,2,3,4,5,6,7,8,9,10,11,12,13,1,2,3,4,5,6,7,8,9,10,11,12,13,1,2,3,4,5,6,7,8,9,10,11,12,13]

#print('Remaining', play(cards, True))

remaining_k,remaining_v = many_plays(10000)

histogram(remaining_k, remaining_v)