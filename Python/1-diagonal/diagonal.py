def diagonal(text, right_to_left=False):
    
    """Print text diagonally either left to right 
    or right to left
    
    Arguments:
    text -- the text to print
    right_to_left -- True or False
    """
    
    # break string into list
    text_list = list(text)
    
    # set starting space length
    # if right start at string length, else start at 0
    if right_to_left:
        count = len(text) - 1 # reduce by 1 to end at 0
    else:
        count = 0
    
    # loop through each letter
    for l in text_list:

        print(' '*count + l)
        
        # subtract if right to left and add if left to right
        if right_to_left:
            count = count - 1
        else:
            count = count + 1

diagonal ("slantwise")    

print('\n')

diagonal ("slantwise", right_to_left=True)