** Python projects completed as part of the degree apprenticeship. **

## 1. Diagonal

Print a string diaagonal left to right and then right to left.

## 2. Ring

Import the turtle module and draw a line and circle of stars (EU flag). Includes arguements for number of star points, radius of star and radius of out circle.

## 3. Validate

**Part 1. **

Validate a credit card number based upon below algorithm.

1. Starting with the penultimate digit, and working towards the rst digit, double alternate
digits.
2. Sum the doubled digits, treating 13 as 1 + 3, etc, and add the result to the sum of the
undoubled digits
3. If the sum is divisible by 10 the number is a valid credit card number.

**Part 2. **

Find possible missing number if credit card includes "X".

## 4. Trains

Validate passenger train data to see the data is possible or impossible based upon number of passengers, stops, and available seats.

*Run by: python trains.py %filename%*

*e.g. python trains.py train1.txt*

## 5. Pig

Modify english words to Pig Latin. http://en.wikipedia.org/wiki/Pig_Latin

## 6. Histogram

Return a histogram with the word lengths and percentages from the file dracula.

*Run by: python histogram.py dracula.txt*

## 7. Highlow

Discover the oracle's random number to a decimal of 0.01.

## 8. Patience

Function that plays a single game of Patience.

Function that plays many games of Patience and returns the number of times the game ends with 0 - 52 cards left in the desk.

Function that returns a histogram of the result of many games of Patience played.

## 9. Anagrams and Blanagrams

**anagram.py ** 

Function to return the anagram with the largest variants from the words.txt file.

**blanagram.py **

https://en.wikipedia.org/wiki/Blanagram

Return the Blanagram with the most varients for the given number of word lengths.

*Run by: python blanagram.py 7*

**anaquery.py **

Repeatly asks the user for a word and prints its anagrams and blanagrams that are not anagrams.

*e.g. orchestra or dammed*