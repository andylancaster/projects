# check if card number is valid
def is_valid(ccnumstr):

    """Check if credit card number is valid
    
    Arguments:
    ccnum -- credit card number as a str
    """
    ccnum = list(ccnumstr) # convert string to list
    ccnum.reverse() # reverse string
    ccnum_len = len(ccnum) # get length of list

    #set blank str for card number doubles and undoubles
    cardnostr = ''
   
    # loop through length of list and multiple each even index value by two
    n = 0
    while n < ccnum_len:
        
        ccnum[n] = int(ccnum[n]) # change to int to times value by two
        
        if (n%2 != 0):
            cardnostr = cardnostr + str(ccnum[n]*2) # multiple each even no. by 2 and add to string
        else:
            cardnostr = cardnostr + str(ccnum[n]) # add to string
            
        n = n+1
    
    # convert new string to list
    cardnostr_list = list(cardnostr)
    prev_sumcardno = 0
    
    # add each each digit in card number together
    for sumcardno in cardnostr_list:
        
        sumcardno = int(sumcardno) + prev_sumcardno
        prev_sumcardno = sumcardno # set to new sum value
    
    # is the value divisible by 10
    if (sumcardno % 10 == 0):
        # reverse card number to readable format
        ccnum.reverse()
        print('\nVALID! The credit card number is ', ccnum, '\nThe divisible number is', sumcardno)
        return True
    else:
        return False        

    
# find X in card number
def findx (ccnum):
    
    """Find X's within a credit card number
    
    Arguments:
    ccnum -- credit card number as a str
    """    
    
    # count number of X's in card number
    countx = ccnum.count('X')
    
    # calculate number of combinations by 10 digits (0-9)
    combinations = 10**countx
    
    combination_numbers = ''
    
    num=0

    while num < combinations:
        
        # add leading zeros for maximum number of combinations e.g. 00, 01, 02 or 000, 001, 002 etc
        str_num = str(num).zfill(countx)
        
        # produce a string of possible numbers
        combination_numbers = combination_numbers + str_num

        num = num+1
        
    # convert string to list for each number to be a single index
    combination_list = list(combination_numbers)
   
    # reset num to 0 to loop through the number of combinations and check each combination
    num=0
    jump = 0    
    while num < combinations:
        
        if countx == 1:
            xchgs = ccnum.replace('X', str(combination_list[jump]), 1)
        else:
            xchgs = ccnum.replace('X', str(combination_list[jump]), 1)
            xchgs = xchgs.replace('X', str(combination_list[jump + 1]), 1)

        is_valid(xchgs)
        
        # jump the index number by the number of X's
        jump = jump + countx
        
        num = num + 1


# Run functions
ccno1 = "49927398716"
ccno2 = "48X926742"
ccno3 = "492X818708805X89"

is_valid(ccno1)

findx(ccno2)

findx(ccno3)