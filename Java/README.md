** Java project completed as part of the degree apprenticeship. **

Ticket IT web site to allow visitors to browse and purhase event tickets.

Includes Java, JSP, CSS, MySQL. Built in Object Oritended

Video demo [https://www.youtube.com/watch?v=Ua6quQBEJtE]

## 1. Source Files

Java, JSP, CSS, and HTML

## 2. MySQL

MySQL database, tables and stored procedures

## 3. WAR

WAR file to import into Tomcat server.

MySQL database required.

