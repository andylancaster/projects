<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="java.util.*, ticketit.Booking"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="../css/style.css">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<title>Ticket IT</title>
</head>
<body>

<jsp:include page="../include/header.jsp"/>

<div id="body">

	<h2>Basket</h2>
	<hr>
	<%
	Booking booking = new Booking();
	if(session.getAttribute("sessioncart") != null) {
		
		LinkedHashMap<Integer, String[]> cart = (LinkedHashMap<Integer, String[]>)session.getAttribute("sessioncart");
		
		// if cart initialised but made empty
		if (cart.isEmpty()) {
			out.println("<h3>Cart currently empty!</h3>");
		} else {
		%>
		<div class="float float-width-30">
			<table>
			<thead>
			<tr><td>Total</td></tr></thead>
			<tbody>
			<tr>
			<td>
			<b><% out.println("�" + String.format("%.2f",booking.getTotalCost( cart ))); %></b>
			<hr>
			<p><a href="payment.jsp">CHECKOUT</a></p>
			</td>
			</tr>
			</tbody>
			</table>
		</div>
		
		<div class="float float-width-70">
			<table>
			<thead>
			<tr>
			<td>&nbsp;</td><td>Event</td><td>Quantity</td><td>Unit Cost</td><td>Line Cost</td>
			</tr>
			</thead>
			<%
			for (Integer key : cart.keySet()) {
				
				double costitem = Double.parseDouble(cart.get(key)[6]);
				
				out.println("<tr>");
				out.println("<td><a href=\"basket-delete.jsp?ttid=" + key +"\"><i class=\"fa fa-trash\" aria-hidden=\"true\"></i></a></td>" + 
							"<td>"+ cart.get(key)[0] + "<p><b>Location:</b> " + cart.get(key)[1] + "</p><p><b>Seat:</b> " + cart.get(key)[2] + "</p><p><b>Date and Time:</b> " + cart.get(key)[3] + "</p></td>" +
							"<td>"+ cart.get(key)[4] + "</td>" +
							"<td>�"+ cart.get(key)[5] + "</td>" +
							"<td>�" + String.format("%.2f",costitem) + "</td>");
				out.println("</tr>");
			}		
			%>
			</table>
		</div>
		<%
		}
	} else {
		out.println("<h3>Cart currently empty!</h3>");
	}
	%>
</div>

<jsp:include page="../include/footer.jsp"/>

</body>
</html>