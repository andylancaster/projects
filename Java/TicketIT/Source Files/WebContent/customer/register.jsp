<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="../css/style.css">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Ticket IT</title>
</head>
<body>

<jsp:include page="../include/header.jsp"/>
<%
String formerror = ( session.getAttribute("formerror") == null ) ? "" : "<p class='error'>" + (String)session.getAttribute("formerror") + "</p>";
String[] reg_formvalues = ( session.getAttribute("reg-formvalues") == null ) ? new String[] {"","","","","",""} : (String[])session.getAttribute("reg-formvalues");
%>
<div id="body">
<h2>Register</h2>
<hr>
<p>Please complete the form below to register as a customer for Ticket IT.</p> 
<p>Required fields are marked with a *</p> 
<% 
out.println(formerror);
session.removeAttribute("formerror");
%>
<form method="post" action="../Servlet">
<label>Title</label>
<select name="reg-title">
<option value="Mr">Mr</option>
<option value="Mrs">Mrs</option>
<option value="Miss">Miss</option>
<option value="Dr">Dr</option>
<option value="Prof">Prof</option>
</select>
<label>First Name *</label>
<input type="text" name="reg-fname" id="reg-fname" value="<% out.println(reg_formvalues[0]); %>"/>
<label>Surname *</label>
<input type="text" name="reg-surname" id="reg-surname" value="<% out.println(reg_formvalues[1]); %>"/>
<label>Email Address *</label>
<input type="text" name="reg-email" id="reg-email" value="<% out.println(reg_formvalues[2]); %>"/>
<label>Street *</label>
<input type="text" name="reg-street" id="reg-street" value="<% out.println(reg_formvalues[3]); %>"/>
<label>City *</label>
<input type="text" name="reg-city" id="reg-city" value="<% out.println(reg_formvalues[4]); %>"/>
<label>Post Code *</label>
<input type="text" name="reg-pcode" id="reg-pcode" value="<% out.println(reg_formvalues[5]); %>"/>
<label>Password *</label>
<input type="password" name="reg-password" id="reg-password"/>
<p><button type="submit" name="reg-submit" id="reg-submit">REGISTER!</button></p>
</form>
</div>

<jsp:include page="../include/footer.jsp"/>

</body>
</html>