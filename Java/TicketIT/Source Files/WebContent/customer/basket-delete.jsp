<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@page import="java.util.*, ticketit.Booking"%>
<%

Booking booking = new Booking();
int ttid = Integer.parseInt( request.getParameter("ttid") );
LinkedHashMap<Integer, String[]> cart = (LinkedHashMap<Integer, String[]>)session.getAttribute("sessioncart");

booking.removeTicketFromCart(cart, ttid);
response.sendRedirect("basket.jsp");
%>