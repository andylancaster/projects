<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="../css/style.css">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Ticket IT</title>
</head>
<body>

<jsp:include page="../include/header.jsp"/>

<div id="body">
<h2>Login</h2>
<hr>
<p>Please complete the form below to log into Ticket IT.</p>  
<% 
// show login form error
String formerror = ( session.getAttribute("formerror") == null ) ? "" : "<p class='error'>" + (String)session.getAttribute("formerror") + "</p>";
out.println(formerror); 
session.removeAttribute("formerror");
%>
<form method="post" action="../Servlet">

<label>Email Address</label>
<input type="text" name="login-email" id="login-email"/>

<label>Password</label>
<input type="password" name="login-password" id="login-password"/>
<p><button type="submit" name="login-submit" id="login-submit">Login!</button></p>
</form>
</div>

<jsp:include page="../include/footer.jsp"/>

</body>
</html>