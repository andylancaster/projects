<%
// remove sessions related to user but not basket
session.removeAttribute("logincustomerid");
session.removeAttribute("userdetails");
session.removeAttribute("userpaymentdetails");
session.removeAttribute("loginstoredcard");
session.removeAttribute("loginrole");
session.removeAttribute("pay-formvalues");
// redirect to homepage
response.sendRedirect("index.jsp");
%>