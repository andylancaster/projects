<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.*,java.util.*, ticketit.Common, ticketit.Event" %>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="../css/style.css">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Ticket IT</title>
</head>
<body>
<jsp:include page="../include/header.jsp"/>
<%
Event event = new Event();
ResultSet myRs = event.getAllEvents();
%>
<div id="body">
<h2>Events</h2>
<hr>
<table>
	<thead>
	<tr><td>Title</td><td>Venue</td><td>Date / Time</td><td>Availability</tr>
	</thead>
	<tbody>
	<%
	while (myRs.next()) {
		out.println("<tr>" +
					"<td>" + myRs.getString("title") + "</td>" + 
					"<td>" + myRs.getString("venue_name_address") + "</td>" + 
					"<td>" + myRs.getDate("datetime") + " " + myRs.getTime("datetime") + "</td>" +
					"<td><a href='event.jsp?eid=" + myRs.getString("id") + "'>More Info.</a></td>" +
					"</tr>");
	}
	myRs.close();
	%>
	</tbody>
</table>
</div>

<jsp:include page="../include/footer.jsp"/>

</body>
</html>