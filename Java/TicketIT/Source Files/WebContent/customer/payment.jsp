<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*, ticketit.Booking, ticketit.Customer, ticketit.Customer_Bank_Card" %>      
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="../css/style.css">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Ticket IT</title>
</head>
<body>

<jsp:include page="../include/header.jsp"/>
<%
// set cart as empty by default
LinkedHashMap<Integer, String[]> cart = (session.getAttribute("sessioncart") == null) ? new LinkedHashMap<Integer, String[]>() : (LinkedHashMap<Integer, String[]>)session.getAttribute("sessioncart");
// if guest set fields as blank or cached values
String[] reg_formvalues = ( session.getAttribute("pay-formvalues") == null ) ? new String[] {"", "", "", "", "", "", ""} : (String[])session.getAttribute("pay-formvalues");
// if guest set bank fields to blank
String[] reg_bank_formvalues = ( session.getAttribute("pay-formbankvalues") == null ) ? new String[] {"", "", "", "", ""} : (String[])session.getAttribute("pay-formbankvalues");

// get form errors
String reg_formerror = ( session.getAttribute("formerror") == null ) ? "" : "<p class='error'>" + (String)session.getAttribute("formerror") + "</p>";
// set customer details to read only if user logged in
String userdetails_readonly;
// set input boxes to readonly if user logged in
String carddetails_readonly;

// get user details if logged in.
if (session.getAttribute("logincustomerid") == null) {
	userdetails_readonly = "";
} else {
	// if user logged in get user details to populate form
	Customer user = new Customer();
	reg_formvalues = (String[])session.getAttribute("userdetails");
	userdetails_readonly = "readonly=\"readonly\"";
}
// set read only value to form if card details are stored
if (session.getAttribute("loginstoredcard") == null || session.getAttribute("loginstoredcard").equals(0)) {
	carddetails_readonly = "";
} else {
	carddetails_readonly = "readonly=\"readonly\"";
	// get store card details
	reg_bank_formvalues = (String[])session.getAttribute("userpaymentdetails");
}
%>
<div id="body">
<h2>Payment and Delivery</h2>
<hr>
<p>Please complete the form below to complete payment.</p>  
<p>Required fields are marked with a *</p> 
<% 
out.println(reg_formerror);
session.removeAttribute("formerror");
%>
<form action="../Servlet" method="POST">
	<label>Title: </label>
	<select name="title" id="title" <% out.println(userdetails_readonly); %>>
	<%
	// set values for select inputs in form
	String[] user_titles = {"Mr", "Mrs", "Miss", "Dr", "Prof"};
	for(int i=0; i<user_titles.length; i++) {
		if (reg_formvalues[0].equals(user_titles[i])) {
			out.println("<option value=\"" + user_titles[i] + "\" selected>" + user_titles[i] + "</option>");
		} else {
			out.println("<option value=\"" + user_titles[i] + "\">" + user_titles[i] + "</option>");	
		}
	}
	%>
	</select>
	<label>First Name *</label>
	<input type="text" name="firstname" id="firstname" value="<% out.println(reg_formvalues[1]); %>" <% out.println(userdetails_readonly); %> />
	<label>Last Name *</label>
	<input type="text" name="lastname" id="lastname" value="<% out.println(reg_formvalues[2]); %>" <% out.println(userdetails_readonly); %>/>
	<label>Email *</label>
	<input type="text" name="email" id="email" value="<% out.println(reg_formvalues[3]); %>" <% out.println(userdetails_readonly); %>/>	
	<label>Address Line 1 *</label>
	<input type="text" name="address1" id="address1" value="<% out.println(reg_formvalues[4]); %>" <% out.println(userdetails_readonly); %>/>
	<label>City *</label>
	<input type="text" name="city" id="city" value="<% out.println(reg_formvalues[5]); %>" <% out.println(userdetails_readonly); %>/>
	<label>Post Code *</label>
	<input type="text" name="postcode" id="postcode" value="<% out.println(reg_formvalues[6]); %>" <% out.println(userdetails_readonly); %>/>
	<p><hr></p>
	<h2>Payment Details</h2>
	<label>Card Type *</label>
	<select name="cardtype" <% out.println(carddetails_readonly); %>>
	<%
	// loop card types and match if value in session
	String[] card_types = {"American Express", "Mastercard", "Visa","Visa Debit"};
	for(int i=0; i<card_types.length; i++) {
		if (reg_bank_formvalues[0].equals(card_types[i])) {
			out.println("<option value=\"" + card_types[i] + "\" selected>" + card_types[i] + "</option>");
		} else {
			out.println("<option value=\"" + card_types[i] + "\">" + card_types[i] + "</option>");	
		}
	}
	%>
	</select>
	<label>Card Number *</label>
	<input type="text" name="cardnumber" id="cardnumber" value="<% out.println(reg_bank_formvalues[1]); %>" <% out.println(carddetails_readonly); %>/>
	<label>Security Code *</label>
	<input type="text" name="cardseccode" id="cardseccode" value="<% out.println(reg_bank_formvalues[2]); %>" <% out.println(carddetails_readonly); %>/>
	<label>Expiry Date (mm / yy) </label>
	<select name="expirymonth" id="expirymonth" <% out.println(carddetails_readonly); %>>
		<% 
		// loop value and match if value in session
		for(int i = 1; i <= 12; i++) {
			if (reg_bank_formvalues[3].equals( Integer.toString(i) )) {
				out.println("<option value="+ i +" selected>" + i + "</option>");
			} else {
				out.println("<option value="+ i +">" + i + "</option>");	
			}
		}
		%>
	</select>
	<select name="expiryyear" id="expiryyear" <% out.println(carddetails_readonly); %>>
		<% 
		// loop value and match if value in session
		for(int i = 2017; i <= 2021; i++) { 
			if (reg_bank_formvalues[4].equals( Integer.toString(i) )) {
				out.println("<option value="+ i +" selected>" + i + "</option>");
			} else {
				out.println("<option value="+ i +">" + i + "</option>");	
			}
		}
		%>	
	</select>
	<%
	// if not guest, not previously save card details, then show option to store card details
	if (session.getAttribute("loginstoredcard") != null && session.getAttribute("loginstoredcard").equals(0)) {
	out.println("<label>Store card details</label>" +
	"<input type=\"checkbox\" name=\"storecarddetails\" id=\"storecarddetails\" value=\"1\">");
	}
	%>
	<p><hr></p>
	<h2>Delivery Method</h2>
	<select name="deliverymethod">
		<option value="Pickup">Pickup</option>
		<option value="Deliver">Deliver</option>
	</select>
	<p><button type="submit" name="payment_submit">Complete Payment</button></p>
</form>
</div>
<jsp:include page="../include/footer.jsp"/>

</body>
</html>