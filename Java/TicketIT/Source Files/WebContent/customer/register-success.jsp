<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="../css/style.css">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Ticket IT</title>
</head>
<body>

<jsp:include page="../include/header.jsp"/>
<%
String formerror = ( session.getAttribute("formerror") == null ) ? "" : "<p class='error'>" + (String)session.getAttribute("formerror") + "</p>";
%>
<div id="body">
<h2>Registration Successful</h2>
<hr>
<div class="success">
<h3>Registration Complete!</h3>
<p>You are now registered and logged into Ticket IT. Your details have been stored, allowing for quicker payment.</p>
</div>
</div>

<jsp:include page="../include/footer.jsp"/>

</body>
</html>