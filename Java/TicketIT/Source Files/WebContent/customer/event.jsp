<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.*, ticketit.Common, ticketit.Event, ticketit.Booking" %>       
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="../css/style.css">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Ticket IT</title>
</head>
<body>
<jsp:include page="../include/header.jsp"/>

<%
// get url event id
int eid = Integer.parseInt( request.getParameter("eid") );
// get ticket available for event
Event event = new Event();
// get event info		
ResultSet eventinfo = event.getEventById(eid);
// get availability for each ticket type in event
ResultSet myAvs = event.getAvailability(eid);
%>
<div id="body">
<%
if (eventinfo.next()) {
	out.println("<h2>" + eventinfo.getString("title") + "</h2><hr>");
	out.println("<p><b>Location</b>: " + eventinfo.getString("venue_name_address") + "</p>");
	out.println("<p><b>Date and time</b>: " + eventinfo.getDate("datetime") + " " + eventinfo.getTime("datetime") + "</p>");
}

// show error for invalid ticket quantity request
String formerror = ( session.getAttribute("formerror") == null ) ? "" : "<p class='error'>" + (String)session.getAttribute("formerror") + "</p>";
out.println(formerror); 
session.removeAttribute("formerror");
%>
<table>
<thead>
<tr>
<td>Ticket Type</td><td>Availability</td><td>Unit Price</td><td>&nbsp;</td><td>&nbsp;</td>
</tr>
</thead>
<%
while (myAvs.next()) {
	
	// default form elements for addinig to cart
	String buyinput = "<input type='text' name='quantity' id='quantity' value='1' maxlength='10' size='3' min='1'>";
	String buybutton = "<button type='submit' name='cartadd' id='cartadd'>Buy</button>";
	// if not sold out show input box
	if (myAvs.getInt("capacity") <= 0) {
		buyinput = "Sold Out!";
		buybutton = "";
	}
	
	out.println("<tr><form action='../Servlet' method='POST'>" + 
			"<input hidden type='text' name='eid' id='eid' value='" + eid + "' />" +
			"<input hidden type='text' name='ttid' id='ttid' value='" + myAvs.getString("id") + "' />" +
			"<td>" + myAvs.getString("description") + "</td>"  +
			"<td>" + myAvs.getInt("capacity") + "</td>" +
			"<td>�" + myAvs.getString("price") +"</td>" +
			"<td>" + buyinput + "</td>" +
			"<td>" + buybutton + "</td></form></tr>"
			);
}
myAvs.close();
%>
</table>
</div>

<jsp:include page="../include/footer.jsp"/>

</body>
</html>