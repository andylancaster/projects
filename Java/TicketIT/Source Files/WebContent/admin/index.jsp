<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.*, ticketit.Common, ticketit.Event, ticketit.Admin" %>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="../css/style.css">
<title>Ticket IT</title>
</head>
<body>
<%
// if admin role not logged in.
if(session.getAttribute("loginrole") == null || (int) session.getAttribute("loginrole") < 1) {
	response.sendRedirect("../customer/login.jsp");
}
 %>
<jsp:include page="../include/header.jsp"/>
<%
Admin admin = new Admin();
ResultSet myRs = admin.getBookings();
%>
<div id="body">
<h2>Admin - View Bookings</h2>
<hr>
	<%
	while (myRs.next()) {
		
		String ticketdetails = admin.getBookedTickets(myRs.getInt("id"));
		
		out.println("<table class='table-admin'>" +
					"<tbody>" +
					"<thead>" + 
					"<tr><td>Ref</td><td>" + myRs.getString("ref") + "</td></tr>" +
					"</thead>" +
					"<tr><td>Booking ID</td><td>" + myRs.getString("id") + "</td></tr>" + 
					"<tr><td>Booking Date & Time<td>" + myRs.getDate("datetime") + " " + myRs.getTime("datetime") + "</td></tr>" +
					"<tr><td>Customer<td>" + myRs.getString("title") + " " + myRs.getString("firstname") + " " + myRs.getString("lastname") + "</td></tr>" +
					"<tr><td>Email<td>" + myRs.getString("email") + "</td></tr>" +
					"<tr><td>Address<td>" + myRs.getString("address1") + ", " + myRs.getString("city") + ", " + myRs.getString("postcode") + "</td></tr>" +
					"<tr><td>Total Payment<td>&pound;" + myRs.getString("amount_paid") + "</td></tr>" +
					"<tr><td>Card Details<td>" +
					"Type: " + myRs.getString("cardtype") +
					"<p>Card No: " + myRs.getString("cardno") + "</p>" +
					"<p>Card Sec No: " + myRs.getString("cardsecno") + "</p>" +
					"<p>Card Expiry: " + myRs.getDate("expiry_date") + "</p>" +
					"</td></tr>" +
					"<tr><td>Ticket Details<td>" +
					ticketdetails + "</td>" +
					"</tr>" + 
					"</tbody>" +
					"</table><br>");
	}
	
	%>

</div>

<jsp:include page="../include/footer.jsp"/>

</body>
</html>