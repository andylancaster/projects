<%@page import="java.util.*, ticketit.Booking"%>
<div id="cont">

<div id="header"><h1>Ticket IT</h1></div>
<div id="nav">
<ul>
<li><a href="<%= request.getContextPath() %>/customer/index.jsp">Events</a></li>
<li><a href="<%= request.getContextPath() %>/customer/basket.jsp">Basket</a></li>
<%
// show adminstration link if admin logged in
if(session.getAttribute("loginrole") != null && (int) session.getAttribute("loginrole") == 1) {
	out.println("<li id='admin'><a href='" + request.getContextPath() + "/admin/index.jsp'>Administration</a></li>");
}
%>
<%
// show registration and login links if logged out
if(session.getAttribute("logincustomerid") == null) {
	out.println("<li><a href='" + request.getContextPath() + "/customer/register.jsp'>Register</a></li>" +
				"<li><a href='" + request.getContextPath() + "/customer/login.jsp'>Login</a></li>");
} else {
	out.println("<li><a href='" + request.getContextPath() + "/customer/logout.jsp'>Log out</a></li>");
}
%>
</ul>
</div>
<div id="basket">
<%
Booking booking = new Booking();
if(session.getAttribute("sessioncart") != null) {
	
	LinkedHashMap<Integer, String[]> cart = (LinkedHashMap<Integer, String[]>)session.getAttribute("sessioncart");
	out.println(booking.getCartSummary(cart));
}
%></div>