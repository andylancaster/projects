package ticketit;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Admin {
	
	Common common = new Common();
	
	// get last 10 bookings
	public ResultSet getBookings() throws SQLException, ClassNotFoundException {
		// create statement
		
		Connection myConn = common.dbC();
		
		CallableStatement cStmt = myConn.prepareCall("{call getAdminBooking()}");

		// execute prepared statement
		ResultSet myRs = cStmt.executeQuery();  	
		return myRs;
	}
	
	// get tickets within bookings
	public String getBookedTickets(int bookingid) throws SQLException, ClassNotFoundException {
		
		// create statement
		String TicketDetails = "";
		
		Connection myConn = common.dbC();
		
		CallableStatement cStmt = myConn.prepareCall("{call getAdminBookingDetail(?)}");
		cStmt.setInt("bookingid", bookingid);
		// execute prepared statement
		ResultSet myRs = cStmt.executeQuery();  
		
		while (myRs.next()) {
			
			TicketDetails = TicketDetails + "<p>" + myRs.getString("title") + ", " + myRs.getString("description") + ", " + myRs.getString("count") + "</p>";
			
		}
		
		return TicketDetails;
		
	}
	
}
