package ticketit;

import java.sql.*;

public class Event {
	
	private int event_id = 0; // event id
	private int ticket_type_id = 0; // ticket type id
	private int ticket_capacity = 0; // default ticket availability
	
	// get common methods e.g. db connection and form validation
	Common common = new Common();
	
	// get every event
	public ResultSet getAllEvents() throws SQLException, ClassNotFoundException {

		// prepare and execute SQL
		Connection myConn = common.dbC();
		CallableStatement cStmt = myConn.prepareCall("{call getAllEvents()}");
		ResultSet myRs = cStmt.executeQuery();
		
		// return results
		return myRs;
		
	}
	
	// get event by id
	public ResultSet getEventById(int eid) throws SQLException, ClassNotFoundException {

		this.event_id = eid;
		
		// prepare and execute SQL
		Connection myConn = common.dbC();
		CallableStatement cStmt = myConn.prepareCall("{call getEventByID(?)}");
		cStmt.setInt("eid", this.event_id);
		ResultSet myRs = cStmt.executeQuery();
		// return results
		return myRs;
		
	}	
	
	// get availability for each ticket type for the event
	public ResultSet getAvailability(int eid) throws SQLException, ClassNotFoundException {

		this.event_id = eid;
		
		// prepare and execute SQL
		Connection myConn = common.dbC();
		CallableStatement cStmt = myConn.prepareCall("{call getEventAvailability(?)}");
		cStmt.setInt("eid", this.event_id);
		ResultSet myRs = cStmt.executeQuery();
		// return results
		return myRs;
		
	}
	
	// get availability by the ticket type id to validate customer requirement
	protected int getAvailabilityByTicketType(int ttid) throws SQLException, ClassNotFoundException {
		
		this.ticket_type_id = ttid;
		
		// prepare and execute SQL
		Connection myConn = common.dbC();
		CallableStatement cStmt = myConn.prepareCall("{call getTicketTypeAvailability(?)}");
		cStmt.setInt("ttid", this.ticket_type_id);
		ResultSet myRs = cStmt.executeQuery();
		
		if(myRs.next()) {
			this.ticket_capacity = myRs.getInt("capacity");
		}
		
		// return available for ticket type selected
		return ticket_capacity;
		
	}
	
}
