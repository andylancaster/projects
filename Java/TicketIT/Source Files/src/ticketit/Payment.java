package ticketit;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Calendar;
import java.util.LinkedHashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Payment {

	// get common methods e.g. db connection and form validation
	Common common = new Common();
	Booking booking = new Booking();
	Customer customer = new Customer();
	Customer_Bank_Card customerbankcard = new Customer_Bank_Card();

	// ticket details in cart with cost, delivery method and stored card details
	private String ticket_type_ids;
	private String ticket_type_ids_quantity;
	private double totalcost = 0;
	private String delivery_method;
	private String storecarddetails;	
	
	// get current month and year to validate card expiry date
	 int currentmonth = Calendar.getInstance().get(Calendar.MONTH) + 1;
	 int currentyear = Calendar.getInstance().get(Calendar.YEAR);
	 
	 protected void setPayment(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			// TODO Auto-generated method stub
			HttpSession session = request.getSession(true);
			PrintWriter out = response.getWriter();		
			// get session cart
			@SuppressWarnings("unchecked")
			LinkedHashMap<Integer, String[]> cart = (LinkedHashMap<Integer, String[]>)session.getAttribute("sessioncart");
			
			int formok = 0;
			// set payment attributes
			this.ticket_type_ids = booking.getCartKeysandQuantity( cart )[0];
			this.ticket_type_ids_quantity = booking.getCartKeysandQuantity( cart )[1];
			this.totalcost = booking.getTotalCost( cart );
			this.delivery_method = request.getParameter("deliverymethod");
			this.storecarddetails = request.getParameter("storecarddetails");		
			
			// set customer attributes to payment details
			customer.setCustomerDetails(request.getParameter("title"),
															request.getParameter("firstname"),
															request.getParameter("lastname"),
															request.getParameter("email"),
															request.getParameter("address1"),
															request.getParameter("city"),
															request.getParameter("postcode"));
			
			customerbankcard.setCustomerPaymentDetails(	request.getParameter("cardtype"),
																							request.getParameter("cardnumber"),
																							request.getParameter("cardseccode"),
																							Integer.parseInt(request.getParameter("expirymonth")),
																							Integer.parseInt(request.getParameter("expiryyear")));

			
			// store form values in array to validate
			String[] formvalues = {customer.title, customer.fname, customer.surname, customer.email, customer.street, customer.city, customer.pcode, 
					customerbankcard.cardtype, customerbankcard.cardno, customerbankcard.cardsecno, Integer.toString(customerbankcard.exmonth), Integer.toString(customerbankcard.exyear)};
			session.setAttribute("pay-formvalues", formvalues );	
			
			// store bank details in separate array
			String[] formbankvalues  = {customerbankcard.cardtype, customerbankcard.cardno, customerbankcard.cardsecno, Integer.toString(customerbankcard.exmonth), Integer.toString(customerbankcard.exyear)};
			session.setAttribute("pay-formbankvalues", formbankvalues );	
			
			// error check for blank fields, invalid email, and bank details
			if(!common.formFieldRequired(formvalues)) {
				session.setAttribute("formerror", "Please complete all required fields" );
			} else if(!common.formfieldEmail(customer.email)) {
				session.setAttribute("formerror", "Please enter a valid email address" );
			} else if (!common.formfieldNum(customerbankcard.cardno)) {
				session.setAttribute("formerror", "Card number must be numeric" );
			} else if (!common.formfieldLen(customerbankcard.cardno, 16)) {
				session.setAttribute("formerror", "Card number must 16 digits in length" );
			} else if (!common.formfieldNum(customerbankcard.cardsecno)) {
				session.setAttribute("formerror", "Card security number must be numeric" );
			} else if (!common.formfieldLen(customerbankcard.cardsecno, 3)) {
				session.setAttribute("formerror", "Card security number must 3 digits in length" );
			} else if (customerbankcard.exyear == currentyear && customerbankcard.exmonth <= currentmonth) {
				session.setAttribute("formerror", "Please enter a valid expiry date" );
			}
			
			else {
				formok = 1;
			}
			
			// final result check
			if (formok == 0) {
				response.sendRedirect("customer/payment.jsp");
			} else {
				
				try {
					/* Complete payment
					 * Add customer if guest otherwise use registered customer ID
					 * Add booking reference
					 * Add each ticket and quantity added to booking_lines table
					 * Add payment details if not previously store, else use card details from card ID
					 * Store details to database if check box ticked
					 * Update ticket availability once statements are complete
					*/
					Connection myConn = common.dbC();
					CallableStatement cStmt = myConn.prepareCall("{call setPayment(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}");
					// set customer ID to null if not logged in.
					if (session.getAttribute("logincustomerid") == null) {
						cStmt.setNull("customerID", Types.INTEGER);
					} else {
						cStmt.setInt("customerID", (int) session.getAttribute("logincustomerid"));
					}
					cStmt.setString("custtitle", customer.title);
					cStmt.setString("custlname", customer.surname);
					cStmt.setString("custfname", customer.fname);
					cStmt.setString("custemail", customer.email);
					cStmt.setString("custstreet", customer.street);
					cStmt.setString("custcity", customer.city);
					cStmt.setString("custpc", customer.pcode);
					cStmt.setLong("bookingRef", booking.bookingref);
					cStmt.setString("delmethod", this.delivery_method);
					cStmt.setString("ttid", this.ticket_type_ids);
					cStmt.setString("ttcount", this.ticket_type_ids_quantity);
					cStmt.setDouble("totalpay", this.totalcost);
					if (session.getAttribute("loginstoredcard") == null) {
						cStmt.setNull("carddetailsid", Types.INTEGER);
					} else {
						cStmt.setInt("carddetailsid", (int) session.getAttribute("loginstoredcard"));
					}
					cStmt.setString("cardtype", customerbankcard.cardtype);
					cStmt.setString("cardno", customerbankcard.cardno);
					cStmt.setString("cardsecno", customerbankcard.cardsecno);
					cStmt.setString("cardexdate", customerbankcard.cardexdate);			
					cStmt.setString("storecarddetails", storecarddetails);	
					ResultSet rs = cStmt.executeQuery();
					
					// SQL statement successful
					if (rs.next()) {
						
						// --- construct body for email --- //
						String emailbody = customer.fname + ""
								+ "<p>Details of your booking are below</p>"
								+ "<p><b>Booking Reference Number:</b> " + booking.bookingref + "</p>"
								+ "<table border='1' width='100%'>";
						for (Integer key : cart.keySet()) {				
							
							double costitem = Double.parseDouble(cart.get(key)[6]);
							
							emailbody = emailbody + "<tr>" +
													"<td>"+ cart.get(key)[0] + "<p><b>Location:</b> " + cart.get(key)[1] + "</p><p><b>Seat:</b> " + cart.get(key)[2] + "</p><p><b>Date and Time:</b> " + cart.get(key)[3] + "</p></td>" +
													"<td>Quantity: "+ cart.get(key)[4] + "</td>" +
													"<td>Unit Cost: &pound;"+ cart.get(key)[5] + "</td>" +
													"<td>Line Cost: &pound;" + String.format("%.2f",costitem) + "</td>" +
													"</tr>";
							
						}
						emailbody = emailbody + "</table><p><b>Total Cost: &pound;" + String.format("%.2f",this.totalcost) + "</b>"
												+ "<p>Please ensure tickets are presented prior to attending the show.</p>"
												+ "<p>Kind Regards</p>"
												+ "<p>Ticket IT</p>";
						// --- end body constructions --- //
						
						// send confirmation email
						common.sendEmail(customer.email, "Ticket IT Booking Confirmation", emailbody);
						
						// clear basket and guest form values
						session.removeAttribute("sessioncart");
						session.removeAttribute("pay-formvalues");
						
						// redirect to successful payment
						response.sendRedirect("customer/payment-success.jsp");
					}
					
				
				} catch (ClassNotFoundException | SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					out.println("ERROR! " + e);
				}
				
			}
 
	 }
	
}
