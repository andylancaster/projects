package ticketit;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Servlet
 */
@WebServlet(asyncSupported = true, urlPatterns = { "/Servlet" })
public class Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		// get page name
		String referrer = new URL(request.getHeader("referer")).getPath(); 
		String[] urlparts = referrer.split("/");
		String pagename = urlparts[urlparts.length -1];

		// load method for the referred page
		switch (pagename) {
			case "event.jsp":	
				Booking booking = new Booking();	
				try {
					booking.addTicketToCart(request, response);
				} catch (NumberFormatException | ClassNotFoundException | SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}			
				break;
			case "login.jsp":
				Customer customerlogin = new Customer();
				customerlogin.setLogin(request, response);
				break;
			case "register.jsp":
				Customer customerregister = new Customer();	
			try {
				customerregister.setRegister(request, response);
			} catch (ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				break;
			case "payment.jsp":
				Payment payment = new Payment();
				payment.setPayment(request, response);
				break;			
		}
	
	}

}
