package ticketit;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Booking {
	
	private Map<Integer, String[]> cart = null; // set default cart
	
	protected long bookingref = new Date().getTime();

	// get common methods e.g. db connection and form validation
	Common common = new Common();
	// get event functionality to check capacity
	Event event = new Event();	
	

	// add tickets to cart
	@SuppressWarnings("unchecked")
	protected void addTicketToCart(HttpServletRequest request, HttpServletResponse response) throws IOException, NumberFormatException, ClassNotFoundException, SQLException {
		
		int formok = 0;
		
		HttpSession session = request.getSession(true);
		PrintWriter out = response.getWriter();
		
		// ticket type id
		Integer ttid = Integer.parseInt( request.getParameter("ttid") );
		
		// check form validation
		if (!common.formfieldNum( request.getParameter("quantity") )) {
			session.setAttribute("formerror", "Numeric numbers required for booking." );
		} else if ( Integer.parseInt( request.getParameter("quantity") ) <= 0) {
			session.setAttribute("formerror", "Please increase your ticket requirement." );
		} else if ( Integer.parseInt( request.getParameter("quantity") ) > event.getAvailabilityByTicketType(ttid)) { 
			session.setAttribute("formerror", "Please reduce your ticket requirement." );
		} else {
			formok = 1;
		}
		
		// if there's an error the redirect back to page
		if (formok == 0) {
			response.sendRedirect("customer/event.jsp?eid=" + request.getParameter("eid"));
		} else {
			
			// set form value to INT
			Integer quan = Integer.parseInt( request.getParameter("quantity") );			
			Integer eid = Integer.parseInt( request.getParameter("eid") );
			
			if(session.getAttribute("sessioncart") == null) {
				this.cart = new LinkedHashMap<Integer, String[]>();
			} else {
				this.cart = (LinkedHashMap<Integer, String[]>)session.getAttribute("sessioncart");
			}
			
			try {
				
				Connection myConn = common.dbC();
				
				// get ticket detail to add to cart inc. calucation for line cost
				CallableStatement cStmt = myConn.prepareCall("{call getBookingTicketDetail(?, ?, ?)}");
				cStmt.setInt("eid", eid);
				cStmt.setInt("ttid", ttid);
				cStmt.setInt("quan", quan);
				ResultSet myRs = cStmt.executeQuery();
				
				if(myRs.next()) {
					// set date and time to a string for the cart session
					String datetime = myRs.getDate("datetime") + " " +  myRs.getTime("datetime");
					// add ticket to cart array
					cart.put(ttid ,new String[] { myRs.getString("title"), myRs.getString("venue_name_address"), myRs.getString("description"), datetime, String.valueOf(quan), myRs.getString("price"), myRs.getString("LINECOST") } );
				}
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				out.println("ERROR Class not found!");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				out.println("Error SQL Exception" + e);
			}  		
			
			// set new array to session
			session.setAttribute("sessioncart", cart );
			// redirect to basket on adding ticket
			response.sendRedirect("customer/basket.jsp");
		
		}		
		
	}
	
	
	// remove ticket event from cart
	public void removeTicketFromCart(LinkedHashMap<Integer, String[]> sessioncart, int ttid) {
		
		sessioncart.remove(ttid);
		
	}
	
	// get total cost of tickets
	public double getTotalCost(LinkedHashMap<Integer, String[]> sessioncart) {		

		double totalcost = 0;
		
		for (Integer key : sessioncart.keySet()) {
			
			double linecost = Double.parseDouble(sessioncart.get(key)[6]);
			
			totalcost = totalcost + linecost;

		}		
		return totalcost;		
		
	}
	
	// get total number of tickets
	protected int getTotalTickets(LinkedHashMap<Integer, String[]> sessioncart) {		

		int tickettotal = 0;
	
		for (Integer key : sessioncart.keySet()) {
			
			int linetotal = Integer.parseInt(sessioncart.get(key)[4]);
			
			tickettotal = tickettotal + linetotal;

		}		
		return tickettotal;		
		
	}
	
	// get cart summary for header
	public String getCartSummary(LinkedHashMap<Integer, String[]> sessioncart) {
		
		String str = "Tickets: <b>" + getTotalTickets(sessioncart) + "</b> Events: <b>" + sessioncart.size() + "</b> Total Cost: <b>&pound;" + String.format("%.2f", getTotalCost(sessioncart)) + "</b>";
		
		return str;
		
	}
	
	// get cart keys and quantity from session cart for database add
	protected String[] getCartKeysandQuantity(LinkedHashMap<Integer, String[]> sessioncart) {
		
		// set array
		String[] arr = new String[2];
		
		arr[0] = ""; // ticket type ids
		arr[1] = ""; // quantity for each ticket type
		
		for (Integer key : sessioncart.keySet()) {
			
			arr[0] = arr[0] + key + ",";
			arr[1] = arr[1] + sessioncart.get(key)[4] + ",";
			
		}
		// trim final comma
		arr[0] = arr[0].replaceAll("\\,$", "");
		arr[1] = arr[1].replaceAll("\\,$", "");
		
		return arr;
		
	}

}
