package ticketit;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Customer {

	// get common methods e.g. db connection and form validation
	Common common = new Common();	
	Customer_Bank_Card customerbankcard = new Customer_Bank_Card();
	
	private int customerId;
	private int customerRole;

	//private int customerId = 0;
	protected String title;
	protected String fname;
	protected String surname;
	protected String email;
	protected String street;
	protected String city;
	protected String pcode;
	private String password;

	// array to store customer details for payment
	public String[] user_details = new String[7];
	
	// set customer attributes for payment
	protected void setCustomerDetails(String title, String fname, String lastname, String email, String street, String city, String pcode) throws ServletException, IOException {
		
		this.title = title;
		this.fname = fname;
		this.surname = lastname;
		this.email = email;
		this.street = street;
		this.city = city;
		this.pcode = pcode;

	}
	
	// register customer
	protected void setRegister(HttpServletRequest request, HttpServletResponse response) throws IOException, ClassNotFoundException, SQLException {
			
			HttpSession session = request.getSession(true);
			PrintWriter out = response.getWriter();
			
			// set form values to attributes
			int formok = 0;
			this.customerId = 0; 
			
			this.title = request.getParameter("reg-title");
			this.fname = request.getParameter("reg-fname");
			this.surname = request.getParameter("reg-surname");
			this.email = request.getParameter("reg-email");
			this.street = request.getParameter("reg-street");
			this.city = request.getParameter("reg-city");
			this.pcode = request.getParameter("reg-pcode");
			this.password = request.getParameter("reg-password");	
			
			// store values in array
			String[] formvalues = {fname, surname, email, street, city, pcode, password};
			// store in session to recall in jsp if error occurs
			session.setAttribute("reg-formvalues", formvalues );
			
			// form validation, required fields, and valid email address
			if(!common.formFieldRequired(formvalues)) {
				session.setAttribute("formerror", "Please complete all required fields" );
			} else if(!common.formfieldEmail(email)) {
				session.setAttribute("formerror", "Please enter a valid email address" );
			} else if(this.checkAccountRegistered(email)) {
				session.setAttribute("formerror", "Email already registered. Please choose another email account." );
			} else {
				formok = 1;
			} 
			
			// if form error occurs return to page
			if (formok == 0) {
				response.sendRedirect("customer/register.jsp");
			} else {
				
				try {
		    		
			    		Connection myConn = common.dbC();
					CallableStatement cStmt = myConn.prepareCall("{call customerRegistration(?, ?, ?, ?, ?, ?, ?, ?, ?)}");
					cStmt.setString("title", this.title);
					cStmt.setString("surname", this.surname);
					cStmt.setString("fname", this.fname);
					cStmt.setString("email", this.email);
					cStmt.setString("street", this.street);
					cStmt.setString("city", this.city);
					cStmt.setString("pcode", this.pcode);
					cStmt.setString("password", this.password);
					cStmt.registerOutParameter(9, java.sql.Types.INTEGER);
					// execute prepared statement
					cStmt.execute();  	
			    				
					this.customerId = cStmt.getInt(9);
		
					// clear session for form error and values
					session.removeAttribute("formerror");
					session.removeAttribute("reg-formvalues");
				
					// set logged in session, role and card details
					session.setAttribute("logincustomerid", customerId );
					session.setAttribute("loginrole", customerRole );
					session.setAttribute("loginstoredcard", 0 );
					this.setRegisteredCustomerDetail(title, fname, surname, email, street, city, pcode);
					customerbankcard.setRegisteredCustomerPaymentDetail("", "", "", 0, 0);
					session.setAttribute("userdetails", this.user_details );
					
					cStmt.close();
					
					// redirect back to home page
					response.sendRedirect("customer/register-success.jsp");
		
		    	} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					out.println("ERROR Class not found!");
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					out.println("Error SQL Exception" + e);
				}
		    	
		}		
			
	}	
	
	// customer login
	protected void setLogin(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		HttpSession session = request.getSession(true);
		int formok = 0;
		
		// set form values to attributes
		this.email = request.getParameter("login-email");
		this.password = request.getParameter("login-password");
		// store values in array
		String[] login_formvalues = {email, password};
		// store in session to recall in jsp if error occurs
		session.setAttribute("login-formvalues", login_formvalues );
		
		if(!common.formFieldRequired(login_formvalues)) {
			session.setAttribute("formerror", "Please complete all required fields" );
		} else if(!common.formfieldEmail(email)) {
			session.setAttribute("formerror", "Please enter a valid email address" );
		} else {
			formok = 1;
		}
		
		// if errors occur redirect back
		if (formok == 0) {
			response.sendRedirect("customer/login.jsp");
		} else {
			
			try {
				int count = 0;
				// prepared statement for customer login
				Connection myConn = common.dbC();
				CallableStatement cStmt = myConn.prepareCall("{call customerLogin(?, ?)}");
				cStmt.setString("cemail", this.email);
				cStmt.setString("cpwd", this.password);
				// execute prepared statement
				ResultSet myRs = cStmt.executeQuery();  	
		    				
				myRs.next();
				count = myRs.getInt("rowcount");

				// successful log in
				if (count >= 1) {
					
					// set logged in session, role and card details
					session.setAttribute("logincustomerid", myRs.getInt("id") );
					session.setAttribute("loginrole", myRs.getInt("role") );
					session.setAttribute("loginstoredcard", myRs.getInt("stored_card") );

					// set payment form values
					this.setRegisteredCustomerDetail(myRs.getString("title"), myRs.getString("firstname"), myRs.getString("lastname"), myRs.getString("email"), myRs.getString("address1"), myRs.getString("city"), myRs.getString("postcode"));
					// if card details stored
					if (myRs.getInt("stored_card") != 0) {
						customerbankcard.setRegisteredCustomerPaymentDetail(myRs.getString("cardtype"), myRs.getString("cardno"), myRs.getString("cardsecno"), myRs.getInt("exmonth"), myRs.getInt("exyear"));
					} else {
						customerbankcard.setRegisteredCustomerPaymentDetail("", "", "", 0, 0);
					}
					// store user sessions
					session.setAttribute("userdetails", this.user_details );	
					session.setAttribute("userpaymentdetails", customerbankcard.bank_detail );	
					response.sendRedirect("customer/index.jsp");
					
				// account and password not found
				} else {
					session.setAttribute("formerror", "Account and password not found" );
					response.sendRedirect("customer/login.jsp");
				}
				myRs.close();
				
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}		
	}
	
	// check if email account already registered as a customer
	public boolean checkAccountRegistered(String email) throws ClassNotFoundException, SQLException  {
		
		int noofaccounts;
		Connection myConn = common.dbC();
		CallableStatement cStmt = myConn.prepareCall("{call checkRegistered(?)}");
		cStmt.setString("email_input", email);
		// execute prepared statement
		ResultSet myRs = cStmt.executeQuery();  
		myRs.next();
		
		// number of accounts already registered to email address
		noofaccounts = myRs.getInt("noofaccounts");
		
		if (noofaccounts >= 1) {
			return true;
		} else {
			return false;
		}

	}
	
	// get user details for payment form
	public void setRegisteredCustomerDetail(String title, String fname, String lname, String email, String street, String city, String pcode) {
		this.user_details[0] = title;
		this.user_details[1] = fname;
		this.user_details[2] = lname;
		this.user_details[3] = email;
		this.user_details[4] = street;
		this.user_details[5] = city;
		this.user_details[6] = pcode;		
	}

}
