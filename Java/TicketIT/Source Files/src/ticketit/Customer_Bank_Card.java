package ticketit;

import java.io.IOException;

import javax.servlet.ServletException;

public class Customer_Bank_Card {
	
	protected String cardtype;
	protected String cardno;
	protected String cardsecno;
	protected int exmonth;
	protected int exyear;
	protected String cardexdate;
	
	public String[] bank_detail = new String[5];
	
	// set attributes for payment
	protected void setCustomerPaymentDetails(String cardtype, String cardno, String cardsecno, int exmonth, int exyear) throws ServletException, IOException {
		this.cardtype = cardtype;
		this.cardno = cardno;
		this.cardsecno = cardsecno;
		this.exmonth = exmonth;
		this.exyear = exyear;
		this.cardexdate = this.exyear + "-" + this.exmonth + "-" + "01";		
	}
	
	// get user card details to populate form
	public void setRegisteredCustomerPaymentDetail(String cardtype, String cardno, String cardsecno, int exmonth, int exyear) {
		
		this.bank_detail[0] = cardtype;
		this.bank_detail[1] = cardno;
		this.bank_detail[2] = cardsecno;
		this.bank_detail[3] = Integer.toString(exmonth);
		this.bank_detail[4] = Integer.toString(exyear);

	}

}
