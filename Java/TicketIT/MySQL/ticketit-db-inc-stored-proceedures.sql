-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 02, 2018 at 01:23 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ticketitv2`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `checkRegistered` (IN `email_input` VARCHAR(255))  NO SQL
BEGIN

-- check registered email account not already in use
SELECT COUNT(*) AS noofaccounts FROM customer WHERE email = email_input and PASSWORD IS NOT NULL;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `customerLogin` (IN `cemail` VARCHAR(255), IN `cpwd` VARCHAR(255))  NO SQL
BEGIN

-- User log in check.
SELECT *, COUNT(*) AS rowcount, MONTH(ccd.expiry_date) AS exmonth, YEAR(ccd.expiry_date) AS exyear FROM customer AS c LEFT JOIN customer_card_details AS ccd ON c.stored_card = ccd.cardid WHERE c.email = cemail AND c.password = cpwd;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `customerRegistration` (IN `title` VARCHAR(10), IN `surname` VARCHAR(255), IN `fname` VARCHAR(255), IN `email` VARCHAR(255), IN `street` VARCHAR(255), IN `city` VARCHAR(255), IN `pcode` VARCHAR(10), IN `password` VARCHAR(255), INOUT `customerID` INT)  NO SQL
BEGIN

-- register user to ticket it
INSERT INTO customer (title, lastname, firstname, email, address1, city, postcode, password) VALUES (title, surname, fname, email, street, city, pcode, password);

-- return customer ID for customer log in session
SET customerID = last_insert_id();

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getAdminBooking` ()  NO SQL
BEGIN

-- get last 10 customer bookings for admin page
SELECT * FROM booking AS b 
INNER JOIN customer AS c ON c.id = b.customer_id
INNER JOIN payment AS p ON p.booking_id = b.id
LEFT JOIN customer_card_details as ccd ON p.card_details_id = ccd.cardid
ORDER BY b.datetime DESC LIMIT 10;
                
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getAdminBookingDetail` (IN `bookingid` INT(11))  NO SQL
BEGIN

-- get ticket details for each booking in admin
SELECT * FROM booking_line AS bl
INNER JOIN ticket_type AS tt ON bl.ticket_type_id=tt.id
INNER JOIN event AS e ON tt.event_id=e.id
WHERE booking_id = bookingid;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getAllEvents` ()  NO SQL
BEGIN

-- get all events for events page
SELECT * FROM event ORDER BY datetime ASC, title DESC;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getBookingTicketDetail` (IN `quan` INT, IN `eid` INT, IN `ttid` INT)  NO SQL
BEGIN

-- get details for events in shopping cart inc. linecost
SELECT *, SUM(quan * tt.price) AS LINECOST FROM `event` AS e INNER JOIN ticket_type AS tt ON e.id = tt.event_id WHERE e.id = eid AND tt.id = ttid;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getEventAvailability` (IN `eid` INT)  NO SQL
BEGIN

-- get ticket types for each event include capacity available.
SELECT id, description, capacity, price FROM ticket_type WHERE event_id = eid;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getEventByID` (IN `eid` INT)  NO SQL
BEGIN

-- get event detail by ID
SELECT title, venue_name_address, datetime FROM event WHERE id = eid;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getTicketTypeAvailability` (IN `ttid` INT)  NO SQL
BEGIN

-- get capacity to check before adding to cart
SELECT capacity FROM ticket_type WHERE id = ttid;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `setPayment` (IN `ttid` TEXT, IN `ttcount` TEXT, IN `customerID` INT, IN `bookingRef` BIGINT, IN `totalpay` DOUBLE, IN `cardtype` VARCHAR(20), IN `cardno` VARCHAR(16), IN `cardsecno` VARCHAR(3), IN `custtitle` VARCHAR(10), IN `custlname` VARCHAR(255), IN `custfname` VARCHAR(255), IN `custstreet` VARCHAR(255), IN `custcity` VARCHAR(255), IN `custpc` VARCHAR(10), IN `custemail` VARCHAR(255), IN `storecarddetails` VARCHAR(2), IN `cardexdate` DATE, IN `carddetailsid` INT, IN `delmethod` VARCHAR(10))  NO SQL
BEGIN

    DECLARE bookingID int;
    DECLARE ttid_element text;
    DECLARE ttcount_element text;

    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
        ROLLBACK;
        SELECT 'An unpexpected error sprunged in your transaction.try again!' as 'Error';
    END;
    
    START TRANSACTION; 

	-- add customer if guest
    IF(customerID IS NULL or customerID='') THEN
    INSERT INTO `customer` (`id`, `title`, `lastname`, `firstname`, `email`, `address1`, `city`, `postcode`) VALUES (NULL, custtitle, custlname, custfname, custemail ,custstreet, custcity, custpc);
    SET customerID = last_insert_id();
    END IF;
    SELECT customerID;
    
    -- insert booking
    INSERT INTO booking (ref, datetime, delivery_method, customer_id) 
    VALUES (bookingRef, NOW(), delmethod, customerID);   
    SET bookingID = last_insert_id();
    
	-- insert each booked ticket
    TT: WHILE TRUE DO
    
        SET ttid_element = SUBSTRING_INDEX(ttid, ',', 1);   
        SET ttcount_element = SUBSTRING_INDEX(ttcount, ',', 1);   
    	
        -- Insert each ticket purchased
        INSERT INTO booking_line (booking_id, ticket_type_id, count) 
VALUES (bookingID, ttid_element, ttcount_element);

		-- Update ticket capacity
        UPDATE `ticket_type` SET `capacity` = `capacity` - ttcount_element WHERE `ticket_type`.`id` = ttid_element;

        IF LOCATE(',', ttcount) > 0 THEN
        SET ttid = SUBSTRING(ttid, LOCATE(',', ttid) + 1);
        SET ttcount = SUBSTRING(ttcount, LOCATE(',', ttcount) + 1);
        ELSE
            LEAVE TT;
        END IF;        
        
    END WHILE;
    
    -- add card details
    IF(carddetailsid IS NULL or carddetailsid = '0') THEN
    INSERT INTO `customer_card_details` (`cardtype`, `cardno`, `cardsecno`, `expiry_date`) VALUES (cardtype, cardno, cardsecno, cardexdate); 
        SET carddetailsid = last_insert_id();
    END IF;
    
    -- store card details
    IF(storecarddetails = '1') THEN
	UPDATE `customer` SET `stored_card` = carddetailsid WHERE `customer`.`id` = customerID;    
	END IF;
    
	-- add payment
    INSERT INTO payment (datetime, booking_id, amount_paid, card_details_id) 
    VALUES (NOW(), bookingID, totalpay, carddetailsid);    
    
    COMMIT;
    
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE `booking` (
  `id` int(11) NOT NULL,
  `ref` bigint(20) NOT NULL,
  `datetime` datetime NOT NULL,
  `delivery_method` varchar(10) NOT NULL,
  `customer_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`id`, `ref`, `datetime`, `delivery_method`, `customer_id`) VALUES
(1, 26452, '2017-02-27 07:34:00', '', 18),
(3, 26486, '2016-11-03 18:11:00', '', 51),
(4, 26503, '2017-04-29 10:18:00', '', 8),
(6, 26537, '2016-12-05 04:55:00', '', 64),
(7, 26554, '2017-03-17 14:13:00', '', 36),
(9, 26588, '2017-11-08 12:46:00', '', 4),
(11, 26622, '2017-08-31 15:53:00', '', 66),
(12, 26639, '2017-10-08 08:11:00', '', 52),
(13, 26656, '2017-08-29 02:24:00', '', 35),
(14, 26673, '2017-09-17 22:32:00', '', 53),
(17, 26724, '2017-05-14 13:32:00', '', 2),
(18, 26741, '2017-03-17 12:48:00', '', 5),
(19, 26758, '2016-12-02 09:35:00', '', 29),
(20, 26775, '2017-05-17 01:33:00', '', 87),
(22, 26809, '2017-02-01 23:16:00', '', 85),
(24, 26843, '2017-05-07 23:10:00', '', 10),
(26, 26877, '2017-07-05 17:55:00', '', 49),
(27, 26894, '2017-04-09 14:45:00', '', 96),
(28, 26911, '2017-04-29 13:01:00', '', 1),
(29, 26928, '2016-12-17 16:10:00', '', 93),
(31, 26962, '2017-08-05 13:32:00', '', 55),
(32, 26979, '2017-07-24 16:18:00', '', 45),
(36, 27047, '2017-09-23 21:05:00', '', 7),
(37, 27064, '2016-12-07 22:53:00', '', 38),
(39, 27098, '2017-05-29 23:45:00', '', 28),
(40, 27115, '2017-10-12 07:06:00', '', 34),
(41, 27132, '2017-01-08 09:52:00', '', 50),
(42, 27149, '2017-05-20 05:26:00', '', 60),
(44, 27183, '2016-10-23 09:07:00', '', 52),
(45, 27200, '2017-02-11 07:04:00', '', 55),
(46, 27217, '2017-08-27 01:54:00', '', 69),
(47, 27234, '2016-12-07 04:47:00', '', 58),
(48, 27251, '2017-08-03 19:57:00', '', 34),
(51, 27302, '2017-08-05 16:10:00', '', 59),
(54, 27353, '2016-10-31 11:45:00', '', 13),
(55, 27370, '2017-09-11 00:12:00', '', 33),
(59, 27438, '2017-07-17 06:20:00', '', 62),
(64, 27523, '2017-11-11 14:07:00', '', 25),
(66, 27557, '2016-11-26 10:03:00', '', 86),
(67, 27574, '2017-04-30 13:57:00', '', 10),
(68, 27591, '2017-04-11 07:14:00', '', 23),
(73, 27676, '2016-12-24 17:57:00', '', 61),
(78, 27761, '2017-05-05 06:03:00', '', 12),
(80, 27795, '2016-11-30 18:12:00', '', 63),
(81, 27812, '2017-03-14 21:13:00', '', 86),
(82, 27829, '2017-08-10 08:24:00', '', 80),
(84, 27863, '2017-02-25 14:25:00', '', 50),
(85, 27880, '2017-05-23 03:13:00', '', 62),
(86, 27897, '2017-05-13 20:45:00', '', 12),
(90, 27965, '2017-01-19 21:56:00', '', 67),
(92, 27999, '2017-01-03 04:19:00', '', 66),
(93, 28016, '2017-06-29 02:36:00', '', 57),
(95, 28050, '2017-06-03 13:33:00', '', 6),
(100, 28135, '2017-04-22 13:54:00', '', 68),
(103, 28279, '2017-03-20 11:33:32', '', 37),
(104, 28296, '2017-03-06 22:26:56', '', 64),
(106, 28330, '2017-07-11 09:57:46', '', 70),
(108, 28364, '2017-08-10 08:45:16', '', 20),
(109, 28381, '2017-05-03 00:03:19', '', 79),
(110, 28398, '2017-06-13 09:00:02', '', 95),
(112, 28432, '2017-07-28 00:10:43', '', 96),
(113, 28449, '2016-11-29 13:09:30', '', 26),
(114, 28466, '2017-10-17 17:48:48', '', 97),
(115, 28483, '2016-12-24 16:00:58', '', 43),
(117, 28517, '2016-12-02 10:42:24', '', 43),
(119, 28551, '2017-04-16 02:14:04', '', 47),
(123, 28619, '2017-10-27 17:31:25', '', 98),
(124, 28636, '2017-06-09 05:34:06', '', 20),
(125, 28653, '2016-12-04 00:41:45', '', 30),
(126, 28670, '2017-11-01 02:06:12', '', 99),
(127, 28687, '2017-05-19 23:18:33', '', 29),
(128, 28704, '2016-11-05 23:54:38', '', 97),
(130, 28738, '2017-03-13 04:06:33', '', 73),
(131, 28755, '2016-12-02 09:56:37', '', 39),
(132, 28772, '2017-05-24 20:51:51', '', 17),
(133, 28789, '2017-05-13 23:02:33', '', 87),
(134, 28806, '2017-11-08 00:31:13', '', 91),
(135, 28823, '2017-08-25 15:40:53', '', 41),
(141, 28925, '2017-09-25 15:11:05', '', 9),
(144, 28976, '2017-09-17 00:00:51', '', 91),
(146, 29010, '2017-06-03 18:41:20', '', 83),
(147, 29027, '2017-09-01 02:17:46', '', 28),
(148, 29044, '2017-01-29 06:44:27', '', 16),
(150, 29078, '2017-10-08 05:57:01', '', 57),
(154, 29146, '2017-07-24 19:52:44', '', 84),
(156, 29180, '2016-12-22 13:38:07', '', 25),
(158, 29214, '2017-09-27 00:32:38', '', 81),
(159, 29231, '2017-10-24 22:40:46', '', 49),
(160, 29248, '2017-08-06 23:18:37', '', 75),
(161, 29265, '2017-04-23 09:34:35', '', 43),
(164, 29316, '2017-01-26 08:24:20', '', 81),
(166, 29350, '2017-03-08 09:28:12', '', 26),
(167, 29367, '2017-06-16 09:05:12', '', 12),
(169, 29401, '2017-06-05 06:16:59', '', 94),
(171, 29435, '2017-03-24 10:46:58', '', 72),
(173, 29469, '2017-04-10 21:35:25', '', 92),
(175, 29503, '2017-05-18 01:14:14', '', 95),
(176, 29520, '2017-06-12 02:14:23', '', 44),
(177, 29537, '2017-08-04 17:17:14', '', 19),
(178, 29554, '2017-01-20 19:59:01', '', 29),
(180, 29588, '2017-03-18 00:04:08', '', 38),
(182, 29622, '2017-08-05 08:05:51', '', 4),
(185, 29673, '2017-02-14 15:19:24', '', 88),
(188, 29724, '2017-04-21 02:40:00', '', 23),
(189, 29741, '2017-10-29 13:21:58', '', 77),
(191, 29775, '2016-12-31 15:21:45', '', 97),
(192, 29792, '2017-04-28 03:05:43', '', 59),
(196, 29860, '2016-12-14 23:46:06', '', 91),
(197, 29877, '2017-07-03 22:33:08', '', 96),
(199, 29911, '2017-08-20 01:37:38', '', 3),
(205, 30067, '2016-12-17 05:38:53', '', 25),
(206, 30084, '2017-11-13 02:19:27', '', 40),
(209, 30135, '2017-09-01 07:27:54', '', 64),
(210, 30152, '2017-02-14 19:24:42', '', 37),
(212, 30186, '2017-11-05 19:29:17', '', 13),
(215, 30237, '2017-06-20 11:20:25', '', 35),
(216, 30254, '2017-10-25 07:07:23', '', 11),
(219, 30305, '2017-04-25 13:56:00', '', 60),
(222, 30356, '2016-12-28 08:58:03', '', 84),
(226, 30424, '2017-05-18 12:56:16', '', 28),
(227, 30441, '2017-03-25 08:18:45', '', 47),
(228, 30458, '2016-11-24 05:02:16', '', 6),
(231, 30509, '2017-06-24 10:54:46', '', 85),
(239, 30645, '2017-09-19 05:22:58', '', 37),
(240, 30662, '2017-06-28 14:16:11', '', 53),
(242, 30696, '2017-07-29 14:33:34', '', 9),
(246, 30764, '2017-02-05 22:35:54', '', 62),
(247, 30781, '2017-07-06 21:16:34', '', 24),
(248, 30798, '2017-10-15 10:43:26', '', 89),
(250, 30832, '2016-11-06 21:43:28', '', 51),
(251, 30849, '2017-01-15 10:53:29', '', 38),
(252, 30866, '2016-10-23 08:44:58', '', 64),
(254, 30900, '2017-04-12 22:27:31', '', 67),
(255, 30917, '2017-07-12 12:57:24', '', 38),
(257, 30951, '2017-01-19 02:23:07', '', 93),
(258, 30968, '2017-09-20 13:35:49', '', 17),
(262, 31036, '2017-02-23 13:46:55', '', 73),
(264, 31070, '2017-08-18 17:10:42', '', 53),
(265, 31087, '2017-01-10 19:48:26', '', 90),
(266, 31104, '2017-08-12 08:37:30', '', 50),
(267, 31121, '2016-11-18 04:57:47', '', 46),
(268, 31138, '2017-09-06 01:21:43', '', 62),
(269, 31155, '2017-04-18 08:50:50', '', 44),
(270, 31172, '2017-04-02 00:06:07', '', 97),
(272, 31206, '2017-05-13 16:01:12', '', 16),
(274, 31240, '2017-06-07 20:14:21', '', 48),
(276, 31274, '2017-09-20 14:57:17', '', 20),
(278, 31308, '2017-08-16 17:28:59', '', 5),
(290, 31512, '2017-01-25 16:49:30', '', 22),
(291, 31529, '2017-02-11 16:13:05', '', 23),
(292, 31546, '2016-12-20 13:42:12', '', 30),
(294, 31580, '2017-08-24 15:24:31', '', 71),
(296, 31614, '2017-04-01 15:11:58', '', 28),
(298, 31648, '2017-08-21 20:13:35', '', 83),
(299, 31665, '2017-04-12 04:45:46', '', 27),
(306, 1509711214, '2017-11-03 12:13:34', '', 114),
(307, 1509712074, '2017-11-03 12:27:54', '', 115),
(308, 1509712093, '2017-11-03 12:28:13', '', 116),
(309, 1509712127, '2017-11-03 12:28:47', '', 117),
(310, 1509712168, '2017-11-03 12:29:28', '', 118),
(311, 1509712210, '2017-11-03 12:30:10', '', 119),
(312, 1509712218, '2017-11-03 12:30:18', '', 120),
(313, 1509712233, '2017-11-03 12:30:33', '', 121),
(314, 1509712255, '2017-11-03 12:30:55', '', 122),
(315, 1509712314, '2017-11-03 12:31:54', '', 123),
(316, 1509712375, '2017-11-03 12:32:55', '', 124),
(317, 1509712594, '2017-11-03 12:36:34', '', 125),
(318, 1509712634, '2017-11-03 12:37:14', '', 126),
(319, 1509712659, '2017-11-03 12:37:39', '', 127),
(320, 1509713302, '2017-11-03 12:48:22', '', 128),
(321, 1509713361, '2017-11-03 12:49:21', '', 129),
(322, 1509718060, '2017-11-03 14:07:40', '', 131),
(323, 1509718318, '2017-11-03 14:11:58', '', 132),
(324, 1509718530, '2017-11-03 14:15:30', '', 133),
(326, 1509719301, '2017-11-03 14:28:21', '', 135),
(327, 1509719409, '2017-11-03 14:30:09', '', 136),
(328, 1509719454, '2017-11-03 14:30:54', '', 137),
(329, 1509719603, '2017-11-03 14:33:23', '', 138),
(330, 1509719629, '2017-11-03 14:33:49', '', 139),
(331, 1509721676, '2017-11-03 15:07:56', '', 140),
(332, 1509721802, '2017-11-03 15:10:02', '', 141),
(333, 1509721919, '2017-11-03 15:11:59', '', 142),
(334, 1509722021, '2017-11-03 15:13:41', '', 143),
(335, 1509722054, '2017-11-03 15:14:14', '', 144),
(336, 1509722184, '2017-11-03 15:16:24', '', 145),
(337, 1509722219, '2017-11-03 15:16:59', '', 146),
(338, 1509725580, '2017-11-03 16:13:00', '', 147),
(339, 1509725622, '2017-11-03 16:13:42', '', 148),
(340, 1509725680, '2017-11-03 16:14:40', '', 149),
(341, 1509963134, '2017-11-06 10:12:14', '', 150),
(342, 123987, '2017-11-06 11:11:01', '', 156),
(343, 123987, '2017-11-06 11:12:36', '', 157),
(344, 123987, '2017-11-06 11:20:59', '', 158),
(345, 123987, '2017-11-06 11:22:35', '', 159),
(346, 123987, '2017-11-06 11:26:15', '', 160),
(347, 123987, '2017-11-06 11:26:50', '', 161),
(348, 123987, '2017-11-06 11:34:16', '', 162),
(349, 123987, '2017-11-06 11:34:38', '', 163),
(350, 123987, '2017-11-06 11:34:55', '', 164),
(351, 123987, '2017-11-06 11:35:22', '', 165),
(352, 123987, '2017-11-06 11:36:38', '', 166),
(353, 123987, '2017-11-06 11:36:49', '', 167),
(354, 123987, '2017-11-06 11:37:05', '', 168),
(355, 123987, '2017-11-06 11:37:32', '', 169),
(356, 123987, '2017-11-06 11:37:51', '', 170),
(357, 123987, '2017-11-06 11:39:04', '', 171),
(358, 123987, '2017-11-06 11:39:21', '', 172),
(359, 123987, '2017-11-06 13:37:56', '', 173),
(360, 123987, '2017-11-06 13:38:06', '', 174),
(361, 123987, '2017-11-06 14:23:26', '', 176),
(363, 1234567890, '2017-11-13 12:31:44', '', 179),
(364, 1234567890, '2017-11-13 12:33:04', '', 179),
(365, 1234567890, '2017-11-13 12:35:16', '', 179),
(366, 1234567890, '2017-11-13 12:41:40', '', 179),
(367, 1234567890, '2017-11-13 13:32:10', '', 179),
(368, 1234567890, '2017-11-13 13:42:29', '', 179),
(370, 1234567890, '2017-11-16 10:54:27', '', 179),
(371, 1234567890, '2017-11-16 10:55:53', '', 179),
(372, 1234567890, '2017-11-16 11:40:09', '', 179),
(374, 1234567890, '2017-11-16 14:40:52', '', 179),
(375, 1234567890, '2017-11-16 14:41:54', '', 179),
(376, 1234567890, '2017-11-16 14:52:04', '', 179),
(380, 1234567890, '2017-11-16 15:45:02', '', 179),
(381, 1234567890, '2017-11-16 15:45:58', '', 179),
(383, 11111, '2017-11-17 09:43:52', '', 179),
(384, 852369741, '2017-11-17 09:47:05', '', 179),
(387, 1234567890, '2017-11-17 10:41:13', '', 179),
(389, 1234567890, '2017-11-17 12:00:58', '', 179),
(390, 1234567890, '2017-11-17 12:01:47', '', 179),
(391, 1234567890, '2017-11-17 12:06:11', '', 179),
(392, 1234567890, '2017-11-17 12:11:16', '', 179),
(393, 1234567890, '2017-11-17 12:11:40', '', 179),
(394, 1234567890, '2017-11-17 12:17:21', '', 179),
(395, 1234567890, '2017-11-17 12:17:33', '', 179),
(396, 1234567890, '2017-11-17 12:22:43', '', 179),
(397, 1234567890, '2017-11-17 12:23:53', '', 179),
(398, 1510928980996, '2017-11-17 12:24:10', '', 179),
(399, 1510929115559, '2017-11-17 14:31:55', '', 179),
(400, 1510929554827, '2017-11-17 14:39:15', '', 179),
(401, 1510929762385, '2017-11-17 14:42:42', '', 179),
(402, 1510930131840, '2017-11-17 14:48:51', '', 179),
(403, 1510930144776, '2017-11-17 14:49:05', '', 179),
(404, 1510930530276, '2017-11-17 14:55:30', '', 179),
(405, 1510930703408, '2017-11-17 14:58:23', '', 179),
(410, 1510934723407, '2017-11-17 16:05:23', 'Pickup', 179),
(411, 1510934931337, '2017-11-17 16:08:51', 'Deliver', 179),
(412, 1510936099825, '2017-11-17 16:28:19', 'Pickup', 179),
(414, 1510936941153, '2017-11-17 16:42:21', 'Pickup', 179),
(415, 1511172476373, '2017-11-20 10:07:56', 'Pickup', 179),
(416, 1511172476373, '2017-11-20 10:08:34', 'Pickup', 179),
(419, 1511186763340, '2017-11-20 14:06:49', 'Pickup', 108),
(420, 1511187084873, '2017-11-20 14:11:25', 'Pickup', 101),
(421, 1511188572315, '2017-11-20 14:36:12', 'Pickup', 109),
(422, 1511191755975, '2017-11-20 15:29:37', 'Pickup', 101),
(423, 1511446446509, '2017-11-23 14:14:31', 'Pickup', 101),
(424, 1511949586013, '2017-11-29 09:59:46', 'Pickup', 101),
(425, 1511949830575, '2017-11-29 10:03:50', 'Pickup', 101);

-- --------------------------------------------------------

--
-- Table structure for table `booking_line`
--

CREATE TABLE `booking_line` (
  `id` int(11) NOT NULL,
  `booking_id` int(11) NOT NULL,
  `ticket_type_id` int(11) NOT NULL,
  `count` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `booking_line`
--

INSERT INTO `booking_line` (`id`, `booking_id`, `ticket_type_id`, `count`) VALUES
(1, 1, 46, 3),
(3, 3, 65, 1),
(4, 4, 15, 1),
(6, 6, 52, 4),
(7, 7, 66, 2),
(9, 9, 52, 3),
(11, 11, 64, 3),
(12, 12, 41, 1),
(13, 13, 62, 1),
(14, 14, 3, 2),
(17, 17, 61, 7),
(18, 18, 62, 1),
(19, 19, 23, 3),
(20, 20, 71, 4),
(22, 22, 35, 2),
(24, 24, 71, 1),
(26, 26, 7, 4),
(27, 27, 29, 10),
(28, 28, 70, 2),
(29, 29, 3, 3),
(31, 31, 60, 3),
(32, 32, 49, 3),
(36, 36, 60, 1),
(37, 37, 75, 2),
(39, 39, 66, 2),
(40, 40, 42, 2),
(41, 41, 3, 2),
(42, 42, 37, 4),
(44, 44, 53, 2),
(45, 45, 63, 4),
(46, 46, 69, 2),
(47, 47, 7, 1),
(48, 48, 65, 4),
(51, 51, 57, 3),
(54, 54, 65, 1),
(55, 55, 76, 5),
(59, 59, 66, 2),
(64, 64, 70, 2),
(66, 66, 2, 2),
(67, 67, 68, 1),
(68, 68, 35, 2),
(73, 73, 12, 1),
(78, 78, 50, 1),
(80, 80, 67, 2),
(81, 81, 31, 1),
(82, 82, 31, 1),
(84, 84, 71, 3),
(85, 85, 21, 1),
(86, 86, 16, 2),
(90, 90, 67, 3),
(92, 92, 14, 3),
(93, 93, 10, 1),
(95, 95, 9, 1),
(100, 100, 48, 4),
(103, 103, 13, 8),
(104, 104, 47, 3),
(106, 106, 14, 2),
(108, 108, 26, 2),
(109, 109, 31, 2),
(110, 110, 64, 4),
(112, 112, 35, 4),
(113, 113, 25, 2),
(114, 114, 64, 4),
(115, 115, 2, 1),
(117, 117, 5, 3),
(119, 119, 54, 1),
(123, 123, 67, 2),
(124, 124, 21, 2),
(125, 125, 16, 1),
(126, 126, 11, 4),
(127, 127, 5, 4),
(128, 128, 72, 3),
(130, 130, 25, 3),
(131, 131, 15, 4),
(132, 132, 12, 4),
(133, 133, 42, 1),
(134, 134, 60, 3),
(135, 135, 31, 3),
(141, 141, 42, 4),
(144, 144, 76, 2),
(146, 146, 72, 3),
(147, 147, 16, 2),
(148, 148, 34, 2),
(150, 150, 68, 1),
(154, 154, 1, 3),
(156, 156, 72, 2),
(158, 158, 24, 2),
(159, 159, 39, 1),
(160, 160, 2, 1),
(161, 161, 34, 1),
(164, 164, 53, 3),
(166, 166, 66, 3),
(167, 167, 4, 2),
(169, 169, 10, 2),
(171, 171, 75, 3),
(173, 173, 6, 1),
(175, 175, 8, 1),
(176, 176, 20, 1),
(177, 177, 54, 1),
(178, 178, 38, 2),
(180, 180, 68, 4),
(182, 182, 19, 2),
(185, 185, 48, 4),
(188, 188, 14, 2),
(189, 189, 71, 2),
(191, 191, 73, 4),
(192, 192, 10, 3),
(196, 196, 23, 3),
(197, 197, 7, 2),
(199, 199, 27, 1),
(205, 205, 51, 2),
(206, 206, 69, 1),
(209, 209, 24, 2),
(210, 210, 7, 2),
(212, 212, 71, 3),
(215, 215, 21, 2),
(216, 216, 10, 2),
(219, 219, 25, 3),
(222, 222, 40, 2),
(226, 226, 30, 1),
(227, 227, 47, 2),
(228, 228, 76, 1),
(231, 231, 53, 1),
(239, 239, 49, 2),
(240, 240, 24, 3),
(242, 242, 72, 1),
(246, 246, 62, 2),
(247, 247, 5, 4),
(248, 248, 67, 2),
(250, 250, 14, 1),
(251, 251, 7, 4),
(252, 252, 53, 4),
(254, 254, 39, 4),
(255, 255, 72, 4),
(257, 257, 46, 3),
(258, 258, 58, 4),
(262, 262, 50, 3),
(264, 264, 60, 1),
(265, 265, 43, 3),
(266, 266, 58, 4),
(267, 267, 44, 3),
(268, 268, 16, 1),
(269, 269, 5, 3),
(270, 270, 72, 3),
(272, 272, 55, 4),
(274, 274, 42, 2),
(276, 276, 49, 1),
(278, 278, 58, 2),
(290, 290, 18, 4),
(291, 291, 74, 2),
(292, 292, 17, 4),
(294, 294, 40, 4),
(296, 296, 45, 3),
(298, 298, 19, 1),
(299, 299, 65, 3),
(301, 1, 41, 3),
(302, 11, 15, 3),
(303, 20, 56, 2),
(304, 32, 61, 3),
(307, 93, 11, 3),
(309, 131, 29, 3),
(310, 132, 45, 1),
(311, 133, 49, 1),
(313, 156, 16, 1),
(315, 182, 37, 1),
(317, 206, 6, 2),
(318, 216, 5, 2),
(322, 269, 24, 1),
(323, 270, 74, 1),
(337, 324, 67, 2),
(338, 324, 72, 5),
(340, 326, 67, 2),
(341, 326, 72, 5),
(342, 327, 67, 2),
(343, 327, 72, 5),
(344, 328, 67, 2),
(345, 328, 72, 5),
(346, 329, 67, 2),
(347, 329, 72, 5),
(348, 330, 67, 2),
(349, 330, 72, 5),
(350, 331, 43, 5),
(351, 331, 29, 3),
(352, 332, 43, 5),
(353, 332, 29, 3),
(354, 333, 43, 5),
(355, 333, 29, 3),
(356, 334, 39, 2),
(357, 334, 43, 5),
(358, 334, 29, 3),
(359, 335, 39, 2),
(360, 335, 43, 5),
(361, 335, 29, 3),
(362, 336, 39, 2),
(363, 336, 74, 3),
(364, 336, 43, 5),
(365, 336, 29, 3),
(366, 336, 63, 4),
(367, 337, 39, 2),
(368, 337, 74, 3),
(369, 337, 43, 5),
(370, 337, 29, 3),
(371, 337, 63, 4),
(372, 338, 43, 6),
(373, 339, 43, 5),
(374, 339, 29, 7),
(375, 340, 43, 5),
(376, 340, 29, 7),
(377, 341, 67, 2),
(378, 341, 72, 5),
(379, 344, 10, 11),
(380, 345, 20, 20),
(382, 347, 1, 1),
(390, 355, 10, 10),
(391, 356, 10, 20),
(394, 1, 10, 10),
(395, 1, 10, 10),
(396, 1, 10, 10),
(397, 1, 10, 10),
(398, 1, 10, 10),
(399, 1, 10, 10),
(420, 1, 2, 0),
(421, 1, 2, 1),
(422, 1, 2, 2),
(423, 1, 2, 3),
(424, 1, 2, 4),
(425, 1, 2, 5),
(426, 1, 2, 10),
(427, 1, 2, 10),
(428, 1, 2, 10),
(429, 1, 2, 10),
(430, 1, 2, 10),
(431, 1, 2, 10),
(433, 360, 8, 8),
(434, 1, 1, 10),
(435, 1, 1, 20),
(436, 1, 1, 30),
(437, 1, 1, 40),
(438, 1, 1, 50),
(439, 1, 1, 60),
(452, 9, 9, 0),
(453, 9, 9, 10),
(454, 9, 9, 20),
(455, 9, 9, 30),
(456, 9, 9, 40),
(457, 9, 9, 50),
(464, 6, 6, 0),
(465, 6, 6, 10),
(466, 6, 6, 20),
(467, 6, 6, 30),
(468, 6, 6, 40),
(469, 6, 6, 50),
(470, 9, 9, 10),
(471, 9, 9, 20),
(472, 9, 9, 30),
(473, 9, 9, 40),
(474, 9, 9, 50),
(475, 9, 9, 60),
(483, 1, 1, 9),
(484, 1, 1, 19),
(485, 1, 1, 29),
(498, 9, 15, 19),
(499, 9, 25, 29),
(500, 9, 35, 39),
(501, 9, 45, 49),
(502, 9, 55, 59),
(503, 9, 65, 69),
(511, 9, 8, 9),
(521, 1, 15, 15),
(522, 1, 25, 25),
(523, 1, 35, 35),
(524, 1, 45, 45),
(525, 1, 55, 55),
(526, 1, 65, 65),
(527, 176, 15, 15),
(528, 176, 25, 25),
(529, 176, 35, 35),
(532, 363, 47, 1),
(533, 363, 22, 1),
(534, 364, 47, 1),
(535, 364, 22, 1),
(536, 365, 47, 1),
(537, 365, 22, 1),
(538, 366, 47, 1),
(539, 366, 22, 1),
(540, 367, 47, 1),
(541, 367, 39, 1),
(542, 367, 44, 1),
(543, 367, 21, 2),
(544, 368, 47, 1),
(545, 368, 39, 1),
(546, 368, 44, 1),
(547, 368, 21, 2),
(552, 370, 26, 1),
(553, 370, 21, 1),
(554, 371, 26, 1),
(555, 371, 21, 1),
(556, 372, 75, 1),
(559, 374, 75, 1),
(560, 374, 3, 1),
(561, 375, 75, 1),
(562, 375, 3, 1),
(563, 376, 75, 1),
(564, 376, 3, 1),
(565, 376, 43, 1),
(574, 380, 55, 1),
(575, 380, 28, 10),
(576, 381, 55, 1),
(577, 381, 28, 10),
(581, 383, 10, 11),
(582, 383, 20, 22),
(583, 383, 30, 33),
(584, 384, 10, 11),
(585, 384, 20, 22),
(586, 384, 30, 33),
(592, 387, 43, 1),
(593, 387, 21, 1),
(594, 387, 3, 1),
(598, 389, 45, 1),
(599, 389, 34, 1),
(600, 390, 45, 1),
(601, 390, 34, 1),
(602, 391, 45, 1),
(603, 391, 34, 1),
(604, 392, 45, 1),
(605, 392, 34, 1),
(606, 393, 45, 1),
(607, 393, 34, 1),
(608, 394, 45, 1),
(609, 394, 34, 1),
(610, 395, 45, 1),
(611, 395, 34, 1),
(612, 396, 45, 1),
(613, 396, 34, 1),
(614, 397, 45, 1),
(615, 397, 34, 1),
(616, 398, 45, 1),
(617, 398, 34, 1),
(618, 399, 75, 1),
(619, 400, 75, 1),
(620, 401, 75, 1),
(621, 401, 69, 1),
(622, 401, 56, 5),
(623, 402, 75, 1),
(624, 402, 69, 1),
(625, 402, 56, 5),
(626, 402, 67, 1),
(627, 403, 75, 1),
(628, 403, 69, 1),
(629, 403, 56, 5),
(630, 403, 67, 1),
(631, 404, 50, 1),
(632, 404, 62, 1),
(633, 405, 67, 1),
(634, 405, 12, 1),
(641, 410, 12, 1),
(642, 410, 29, 1),
(643, 411, 72, 1),
(644, 411, 18, 1),
(645, 411, 25, 2),
(646, 412, 29, 1),
(648, 414, 43, 1),
(649, 415, 56, 1),
(650, 415, 33, 1),
(651, 416, 16, 1),
(655, 419, 56, 1),
(656, 420, 42, 1),
(657, 421, 14, 1),
(658, 421, 45, 1),
(659, 422, 42, 1),
(660, 422, 45, 1),
(661, 422, 19, 1),
(662, 423, 28, 1),
(663, 423, 75, 1),
(664, 424, 56, 1),
(665, 425, 45, 1),
(666, 425, 75, 2);

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `title` varchar(10) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `address1` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `postcode` varchar(10) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` tinyint(1) NOT NULL DEFAULT '0',
  `stored_card` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `title`, `lastname`, `firstname`, `email`, `address1`, `city`, `postcode`, `password`, `role`, `stored_card`) VALUES
(1, 'Ms.', 'Powell', 'Moana', 'PowellMoana@hotmail.com', '904-3577 Suspendisse St.', 'Kisi', '83686', '', 0, NULL),
(2, 'Ms.', 'Lee', 'Rowan', 'LeeRowan@hotmail.com', 'Ap #999-6197 Risus. St.', 'Cantalupo in Sabina', '606680', '', 0, NULL),
(3, 'Dr.', 'Mcdowell', 'Quinn', 'McdowellQuinn@hotmail.com', '1145 Gravida Av.', 'Florianópolis', '267979', '', 0, NULL),
(4, '', 'Banks', 'Hope', 'BanksHope@hotmail.com', '1082 Euismod St.', 'Ely', '817397', '', 0, NULL),
(5, 'Dr.', 'Holder', 'Scott', 'HolderScott@hotmail.com', 'Ap #868-1901 Ornare Avenue', 'Prestatyn', '1126', '', 0, NULL),
(6, 'Mrs.', 'Fox', 'Holmes', 'FoxHolmes@hotmail.com', 'Ap #769-3437 Tellus St.', 'Coinco', '887561', '', 0, NULL),
(7, 'Dr.', 'Leach', 'Tiger', 'LeachTiger@hotmail.com', 'Ap #376-8347 Sollicitudin Road', 'Palanzano', '27261-986', '', 0, NULL),
(8, '', 'Reid', 'Nina', 'ReidNina@hotmail.com', '902-7661 Pellentesque Road', 'Aisemont', '5506', '', 0, NULL),
(9, '', 'Campos', 'Aimee', 'CamposAimee@hotmail.com', '668-5748 Nisl St.', 'Vinci', '5088', '', 0, NULL),
(10, '', 'Pierce', 'Octavius', 'PierceOctavius@hotmail.com', '8909 Nec Road', 'Sint-Pieters-Kapelle', '36771', '', 0, NULL),
(11, 'Ms.', 'Decker', 'Yardley', 'DeckerYardley@hotmail.com', '371-3814 Elit. Av.', 'Castor', '72667', '', 0, NULL),
(12, '', 'Crawford', 'Abbot', 'CrawfordAbbot@hotmail.com', 'Ap #319-7124 Aliquam Ave', 'Coaldale', '00213-678', '', 0, NULL),
(13, 'Mr.', 'Huff', 'Jescie', 'HuffJescie@hotmail.com', '635-4184 Vel Road', 'Le Cannet', 'V37 3AY', '', 0, NULL),
(14, '', 'Lewis', 'Serena', 'LewisSerena@hotmail.com', 'Ap #400-9170 Enim St.', 'Habay-la-Neuve', '5160', '', 0, NULL),
(15, 'Ms.', 'White', 'Melanie', 'WhiteMelanie@hotmail.com', 'Ap #939-8090 Leo, Road', 'Kawartha Lakes', '9090', '', 0, NULL),
(16, 'Dr.', 'Mcintosh', 'Ashton', 'McintoshAshton@hotmail.com', 'P.O. Box 377, 8896 Nulla Street', 'Kansas City', '5744', '', 0, NULL),
(17, 'Dr.', 'Michael', 'Bruno', 'MichaelBruno@hotmail.com', 'P.O. Box 127, 9566 Aenean Av.', 'Griesheim', '946334', '', 0, NULL),
(18, 'Mrs.', 'Ashley', 'Hayley', 'AshleyHayley@hotmail.com', 'P.O. Box 482, 3427 Ut, Ave', 'Naumburg', '42-180', '', 0, NULL),
(19, 'Mrs.', 'Brady', 'Lars', 'BradyLars@hotmail.com', 'Ap #339-8617 Per Av.', 'Villers-Poterie', '97707-332', '', 0, NULL),
(20, 'Dr.', 'Warren', 'Adam', 'WarrenAdam@hotmail.com', '4301 Semper Avenue', 'Avise', '405076', '', 0, NULL),
(21, 'Dr.', 'Boyer', 'Xaviera', 'BoyerXaviera@hotmail.com', '8798 Ullamcorper Avenue', 'Port Coquitlam', '979758', '', 0, NULL),
(22, 'Mr.', 'Caldwell', 'Shelly', 'CaldwellShelly@hotmail.com', 'P.O. Box 434, 2275 Ante, St.', 'Paisley', '12117', '', 0, NULL),
(23, '', 'Maldonado', 'Zahir', 'MaldonadoZahir@hotmail.com', 'P.O. Box 545, 1629 Lectus Avenue', 'Turnhout', '59756', '', 0, NULL),
(24, 'Ms.', 'Finch', 'Burke', 'FinchBurke@hotmail.com', 'Ap #468-165 Orci. Rd.', 'Dorval', '86273-663', '', 0, NULL),
(25, 'Mrs.', 'Brennan', 'Bevis', 'BrennanBevis@hotmail.com', 'P.O. Box 372, 2667 Luctus Av.', 'Dundee', '2154 KL', '', 0, NULL),
(26, 'Ms.', 'Morse', 'Dante', 'MorseDante@hotmail.com', 'P.O. Box 543, 3474 Massa. Rd.', 'Navsari', '27856-335', '', 0, NULL),
(27, 'Mr.', 'Kane', 'Madeson', 'KaneMadeson@hotmail.com', 'Ap #563-5416 Aliquam Street', 'Graneros', '10134', '', 0, NULL),
(28, 'Mr.', 'Bentley', 'Bruce', 'BentleyBruce@hotmail.com', 'Ap #494-9012 Ante. Av.', 'Vernon', '37310', '', 0, NULL),
(29, '', 'Rice', 'Natalie', 'RiceNatalie@hotmail.com', 'P.O. Box 646, 4490 Luctus Ave', 'Baden', '359898', '', 0, NULL),
(30, 'Mr.', 'Camacho', 'Phillip', 'CamachoPhillip@hotmail.com', 'Ap #664-5023 Vivamus Ave', 'Patarrá', '43514', '', 0, NULL),
(31, '', 'Pruitt', 'Francesca', 'PruittFrancesca@hotmail.com', 'Ap #639-9303 Mi Ave', 'Cache Creek', '8634', '', 0, NULL),
(32, 'Dr.', 'Ferguson', 'Ryder', 'FergusonRyder@hotmail.com', 'P.O. Box 514, 9512 Cursus Avenue', 'Dreux', '21579', '', 0, NULL),
(33, '', 'English', 'Alea', 'EnglishAlea@hotmail.com', '286-1743 Pede. Av.', 'Bazel', '41157', '', 0, NULL),
(34, 'Mrs.', 'Key', 'Kieran', 'KeyKieran@hotmail.com', 'P.O. Box 293, 2736 Eu, Rd.', 'New Orleans', '8439', '', 0, NULL),
(35, 'Dr.', 'Mitchell', 'Otto', 'MitchellOtto@hotmail.com', 'P.O. Box 900, 9441 Consequat Ave', 'Quirihue', '6764 UO', '', 0, NULL),
(36, 'Ms.', 'Cardenas', 'Wayne', 'CardenasWayne@hotmail.com', '700-6560 Mi Ave', 'Ulloa (Barrial)', '1681 WR', '', 0, NULL),
(37, '', 'Hughes', 'Anthony', 'HughesAnthony@hotmail.com', '249-2884 A, Avenue', 'Uyo', '8210', '', 0, NULL),
(38, 'Mrs.', 'Davis', 'Xenos', 'DavisXenos@hotmail.com', 'P.O. Box 509, 7639 Nec Road', 'Navsari', '1604', '', 0, NULL),
(39, '', 'Delacruz', 'Zachery', 'DelacruzZachery@hotmail.com', '958-1728 Sit Ave', 'Kenosha', '5354', '', 0, NULL),
(40, 'Dr.', 'Mcconnell', 'Hanae', 'McconnellHanae@hotmail.com', 'Ap #579-4508 Feugiat Road', 'Bruck an der Mur', '9845', '', 0, NULL),
(41, 'Dr.', 'Barlow', 'Harper', 'BarlowHarper@hotmail.com', 'Ap #957-478 Aliquam Road', 'Beverley', '47779', '', 0, NULL),
(42, 'Mrs.', 'Conway', 'Lillith', 'ConwayLillith@hotmail.com', 'P.O. Box 582, 8814 Nulla Av.', 'Montereale', '896154', '', 0, NULL),
(43, 'Dr.', 'Mcguire', 'Halla', 'McguireHalla@hotmail.com', 'Ap #966-9742 Tristique St.', 'Francavilla in Sinni', '57373', '', 0, NULL),
(44, 'Ms.', 'Frank', 'Vivien', 'FrankVivien@hotmail.com', '123-1387 Vitae Street', 'Le Puy-en-Velay', '939526', '', 0, NULL),
(45, 'Dr.', 'Knight', 'Joelle', 'KnightJoelle@hotmail.com', 'P.O. Box 650, 7363 Sed Ave', 'Dworp', '43551-076', '', 0, NULL),
(46, 'Mrs.', 'Wooten', 'Solomon', 'WootenSolomon@hotmail.com', 'Ap #425-7179 Nullam Av.', 'Brighton', '81685', '', 0, NULL),
(47, '', 'Weeks', 'Theodore', 'WeeksTheodore@hotmail.com', '5586 Enim St.', 'Kapelle-op-den-Bos', '663046', '', 0, NULL),
(48, 'Ms.', 'Ellison', 'Martena', 'EllisonMartena@hotmail.com', 'P.O. Box 594, 2620 Montes, St.', 'Cannes', '10556', '', 0, NULL),
(49, '', 'Gentry', 'Kane', 'GentryKane@hotmail.com', '1922 Bibendum Avenue', 'Seloignes', '5220', '', 0, NULL),
(50, '', 'Hamilton', 'Dalton', 'HamiltonDalton@hotmail.com', '139-468 Arcu. Rd.', 'Pietrarubbia', '57074', '', 0, NULL),
(51, 'Mrs.', 'Best', 'Matthew', 'BestMatthew@hotmail.com', '580-4258 Habitant Ave', 'Heilbronn', 'C0V 5KK', '', 0, NULL),
(52, 'Dr.', 'Allison', 'Cameron', 'AllisonCameron@hotmail.com', 'Ap #464-4880 Mollis. Road', 'Auvelais', '986673', '', 0, NULL),
(53, 'Ms.', 'Huffman', 'Wayne', 'HuffmanWayne@hotmail.com', '7606 Vel St.', 'Perk', '74-300', '', 0, NULL),
(54, 'Dr.', 'Chaney', 'Dillon', 'ChaneyDillon@hotmail.com', 'P.O. Box 455, 4398 Eu, Road', 'Kuringen', '9996', '', 0, NULL),
(55, 'Ms.', 'Fitzgerald', 'Desiree', 'FitzgeraldDesiree@hotmail.com', '1280 Fusce St.', 'San Juan de Dios', '43937', '', 0, NULL),
(56, 'Mrs.', 'Chavez', 'Benjamin', 'ChavezBenjamin@hotmail.com', 'P.O. Box 359, 8705 Porttitor Rd.', 'Glenrothes', '80-038', '', 0, NULL),
(57, 'Mr.', 'Vega', 'Herman', 'VegaHerman@hotmail.com', 'P.O. Box 129, 6303 Porttitor Rd.', 'Rae Bareli', '479182', '', 0, NULL),
(58, 'Ms.', 'Pennington', 'Minerva', 'PenningtonMinerva@hotmail.com', 'P.O. Box 907, 8333 Ante. Av.', 'San Fernando', '60394', '', 0, NULL),
(59, 'Dr.', 'Hodges', 'Drew', 'HodgesDrew@hotmail.com', '140-9662 Phasellus Road', 'Navidad', '19279', '', 0, NULL),
(60, 'Ms.', 'Preston', 'Solomon', 'PrestonSolomon@hotmail.com', '111-4400 Rhoncus. St.', 'Terragnolo', '573941', '', 0, NULL),
(61, 'Mrs.', 'Cantrell', 'Dorian', 'CantrellDorian@hotmail.com', '739-2197 Proin Rd.', 'Ancud', '17113', '', 0, NULL),
(62, '', 'Hughes', 'Madeline', 'HughesMadeline@hotmail.com', '3697 Nunc Street', 'Kakinada', '64506', '', 0, NULL),
(63, 'Ms.', 'Henry', 'Hanae', 'HenryHanae@hotmail.com', 'P.O. Box 485, 8891 Mauris Road', 'Lakeland County', '89250', '', 0, NULL),
(64, '', 'Wade', 'Libby', 'WadeLibby@hotmail.com', '122-5264 Sit Rd.', 'Feltre', '66546', '', 0, NULL),
(65, 'Mr.', 'Trujillo', 'Candace', 'TrujilloCandace@hotmail.com', 'Ap #974-4882 Cras Street', 'Laon', '16549-193', '', 0, NULL),
(66, 'Mrs.', 'Cardenas', 'Yen', 'CardenasYen@hotmail.com', 'P.O. Box 709, 3993 Neque. Street', 'Romano d''Ezzelino', 'J97 1WE', '', 0, NULL),
(67, 'Dr.', 'Mclean', 'Cassandra', 'McleanCassandra@hotmail.com', 'P.O. Box 104, 7780 Id, Av.', 'Ajmer', '2695', '', 0, NULL),
(68, 'Mr.', 'Bruce', 'Craig', 'BruceCraig@hotmail.com', 'Ap #987-6918 Odio. St.', 'Guelph', '98594', '', 0, NULL),
(69, 'Mr.', 'Walker', 'Cyrus', 'WalkerCyrus@hotmail.com', '776-8798 Sed Road', 'Villa Alegre', 'A6P 5J8', '', 0, NULL),
(70, 'Dr.', 'Richards', 'Gray', 'RichardsGray@hotmail.com', 'P.O. Box 984, 1065 Ipsum. St.', 'Tarzo', 'JI3 5EL', '', 0, NULL),
(71, '', 'Witt', 'Hop', 'WittHop@hotmail.com', 'P.O. Box 909, 4327 Primis St.', 'Marystown', '52194', '', 0, NULL),
(72, '', 'Weber', 'Amos', 'WeberAmos@hotmail.com', 'P.O. Box 804, 4446 Aliquam St.', 'Blue Mountains', '03-579', '', 0, NULL),
(73, 'Ms.', 'Kline', 'Harding', 'KlineHarding@hotmail.com', '746-770 Feugiat Avenue', 'Wieze', '7949', '', 0, NULL),
(74, 'Mrs.', 'Chen', 'Brenna', 'ChenBrenna@hotmail.com', '5516 Lectus Street', 'Carluke', '3432', '', 0, NULL),
(75, 'Mr.', 'Young', 'Kelsey', 'YoungKelsey@hotmail.com', '162-9383 Vel Rd.', 'Burg', '3560', '', 0, NULL),
(76, 'Mr.', 'Sosa', 'Haviva', 'SosaHaviva@hotmail.com', '6202 Donec St.', 'Rhemes-Notre-Dame', '2392', '', 0, NULL),
(77, 'Mrs.', 'Hudson', 'Davis', 'HudsonDavis@hotmail.com', 'Ap #664-6710 Elementum Road', 'Cleveland', '58945', '', 0, NULL),
(78, 'Mrs.', 'Walker', 'Tasha', 'WalkerTasha@hotmail.com', 'P.O. Box 972, 1802 A Rd.', 'Contulmo', '51210', '', 0, NULL),
(79, 'Mr.', 'Gentry', 'Gray', 'GentryGray@hotmail.com', '2092 Erat. Avenue', 'Agen', '4936', '', 0, NULL),
(80, 'Mrs.', 'Case', 'Madeson', 'CaseMadeson@hotmail.com', '3518 Felis Rd.', 'Cheltenham', '27709', '', 0, NULL),
(81, 'Ms.', 'Cameron', 'Hayes', 'CameronHayes@hotmail.com', 'P.O. Box 222, 3705 Odio Ave', 'Alix', '717755', '', 0, NULL),
(82, 'Dr.', 'Combs', 'Palmer', 'CombsPalmer@hotmail.com', 'P.O. Box 169, 1481 At Ave', 'Vandoeuvre-lès-Nancy', '381841', '', 0, NULL),
(83, 'Ms.', 'Ramsey', 'Ahmed', 'RamseyAhmed@hotmail.com', 'P.O. Box 126, 6616 Suspendisse Street', 'Harrogate', '60830', '', 0, NULL),
(84, 'Ms.', 'Tate', 'Kenneth', 'TateKenneth@hotmail.com', '9642 Dui. Street', 'Weißenfels', '140901', '', 0, NULL),
(85, 'Dr.', 'Roth', 'Alfreda', 'RothAlfreda@hotmail.com', '1772 Risus, St.', 'Genk', '546300', '', 0, NULL),
(86, 'Ms.', 'Berry', 'Arsenio', 'BerryArsenio@hotmail.com', '795-5813 Lacus. Ave', 'Tubeke Tubize', '4816', '', 0, NULL),
(87, 'Mr.', 'Cherry', 'Cheryl', 'CherryCheryl@hotmail.com', '8130 Quam St.', 'Quemchi', '80-714', '', 0, NULL),
(88, 'Mrs.', 'Kirk', 'Emily', 'KirkEmily@hotmail.com', 'Ap #357-3556 Dui, Rd.', 'Coihaique', '41106', '', 0, NULL),
(89, 'Mrs.', 'Singleton', 'James', 'SingletonJames@hotmail.com', '8394 Volutpat. Ave', 'Baltimore', '645604', '', 0, NULL),
(90, 'Mr.', 'Aguirre', 'Declan', 'AguirreDeclan@hotmail.com', 'Ap #726-5415 Auctor, Avenue', 'Vlissegem', 'XE97 6YL', '', 0, NULL),
(91, 'Mr.', 'Shelton', 'Meredith', 'SheltonMeredith@hotmail.com', '574-7213 Urna St.', 'Mespelare', '6209 RU', '', 0, NULL),
(92, 'Ms.', 'Carson', 'Xavier', 'CarsonXavier@hotmail.com', 'Ap #949-7945 Magna. Avenue', 'Angoulême', '65119', '', 0, NULL),
(93, 'Dr.', 'Decker', 'Brendan', 'DeckerBrendan@hotmail.com', '7211 Suspendisse Rd.', 'Brahmapur', '5066 GE', '', 0, NULL),
(94, 'Mrs.', 'Hobbs', 'McKenzie', 'HobbsMcKenzie@hotmail.com', '575-524 Aliquet St.', 'Caplan', '651266', '', 0, NULL),
(95, '', 'Adams', 'Conan', 'AdamsConan@hotmail.com', 'Ap #517-6314 Dictum St.', 'Bo‘lhe', '67-500', '', 0, NULL),
(96, 'Dr.', 'Mccarty', 'Julian', 'MccartyJulian@hotmail.com', 'P.O. Box 304, 7414 Ipsum. Ave', 'Khanpur', '3976', '', 0, NULL),
(97, 'Dr.', 'Little', 'Jessamine', 'LittleJessamine@hotmail.com', '244-4650 Non Street', 'Montpellier', '1320', '', 0, NULL),
(98, 'Mr.', 'Black', 'Phyllis', 'BlackPhyllis@hotmail.com', 'P.O. Box 840, 1128 Non, Road', 'Tenby', '26684', '', 0, NULL),
(99, 'Mr.', 'Valdez', 'Kasimir', 'ValdezKasimir@hotmail.com', 'P.O. Box 876, 5597 Interdum. St.', 'Ravenstein', '49684', '', 0, NULL),
(100, 'Dr.', 'Burns', 'Asher', 'BurnsAsher@hotmail.com', 'P.O. Box 488, 2809 Ipsum Rd.', 'Buti', '909520', '', 0, NULL),
(101, 'Mr', 'Lancaster', 'Andy', 'a_m_lancaster@hotmail.com', 'Vine Cottage, Exminster Hill, Exminster', 'Exeter', 'EX6 8DW', 'password', 0, 33),
(104, 'Mr', 'Lancaster', 'Andy', 'a_m_lancaster@hotmail.com', 'Vine Cottage, Exminster Hill, Exminster', 'Exeter', 'EX6 8DW', 'password1', 0, NULL),
(108, 'Mr', 'Lancaster', 'Andy', 'BurnsAsher1@hotmail.com', 'Vine Cottage, Exminster Hill, Exminster', 'Exeter', 'EX6 8DW', '', 0, NULL),
(109, 'Mr', 'a', 'Andy', 'a@a.com', 'Vine Cottage, Exminster Hill, Exminster', 'Exeter', 'EX6 8DW', 'password', 0, NULL),
(110, 'Mr', 'Admin', 'TicketIT', 'admin@ticketit.com', '', '', '', 'password', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `customer_card_details`
--

CREATE TABLE `customer_card_details` (
  `cardid` int(11) NOT NULL,
  `cardtype` varchar(30) NOT NULL,
  `cardno` bigint(20) NOT NULL,
  `cardsecno` int(3) NOT NULL,
  `expiry_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer_card_details`
--

INSERT INTO `customer_card_details` (`cardid`, `cardtype`, `cardno`, `cardsecno`, `expiry_date`) VALUES
(1, 'visa', 1111222233334444, 123, '2017-11-01'),
(2, 'visa', 1111222233334444, 123, '2017-11-01'),
(3, 'Visa', 2222333355556666, 562, '2017-11-01'),
(4, 'Visa', 2222333355556666, 562, '2017-11-01'),
(5, 'Visa', 1234567812345678, 852, '2017-11-01'),
(6, 'Mastercard', 9999555511114444, 456, '2017-11-01'),
(7, 'Visa', 1234123412341234, 598, '2017-11-01'),
(8, 'Visa', 7777555533336666, 654, '2017-11-01'),
(9, 'Visa', 1234123412341234, 598, '2017-11-01'),
(10, 'Visa', 1234123412341234, 598, '2018-08-01'),
(11, 'Visa', 1234123412341234, 598, '2017-11-01'),
(12, 'Mastercard', 9999888877776666, 888, '2017-11-01'),
(13, 'Visa', 1234123412341234, 598, '2017-11-01'),
(14, 'Visa', 1234123412341234, 598, '2017-11-01'),
(15, 'Visa', 1234123412341234, 598, '2017-11-01'),
(16, 'Visa Debit', 2132132132132211, 321, '2017-11-01'),
(17, 'Visa Debit', 2132132132132211, 321, '2021-06-01'),
(18, 'Visa Debit', 1111222233334444, 555, '2020-11-01'),
(19, 'Visa', 1234123412341234, 598, '2018-08-01'),
(20, 'Visa', 1234123412341234, 598, '2018-08-01'),
(21, 'mastercard', 9999999999999999, 999, '2018-08-01'),
(22, 'mastercard', 8888888888888888, 888, '2018-08-01'),
(23, 'American Express', 7777777777777777, 555, '2017-01-01'),
(24, 'Visa Debit', 1122334455667788, 987, '2020-06-01'),
(25, 'American Express', 1234123412341234, 999, '2020-06-01'),
(26, 'American Express', 9633852274118522, 565, '2020-08-01'),
(27, 'Visa', 1111222233334444, 546, '2018-01-01'),
(28, 'Visa', 1111222233334444, 546, '2018-01-01'),
(29, 'Visa Debit', 7411852296339877, 888, '2019-05-01'),
(30, 'Visa', 1111222233334444, 456, '2017-01-01'),
(31, 'Visa Debit', 4444333322221111, 111, '2017-07-01'),
(32, 'American Express', 7777444488885555, 555, '2017-01-01'),
(33, 'American Express', 1111222233334444, 654, '2017-01-01'),
(34, 'American Express', 1111222244443333, 123, '2017-07-01');

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `venue_name_address` text NOT NULL,
  `datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`id`, `title`, `venue_name_address`, `datetime`) VALUES
(1, '42nd Street', 'Duchess Theatre', '2018-09-03 12:00:00'),
(2, 'Dreamgirls', 'Phoenix Theatre', '2018-08-16 11:00:00'),
(3, 'Engelbert Humperdinck', 'Adelphi Theatre', '2018-10-01 16:00:00'),
(4, 'The Book of Mormon', 'London County Hall', '2016-11-08 12:00:00'),
(5, 'The Great Gatsby', 'Lyceum Theatre', '2017-05-07 18:00:00'),
(6, 'Aladdin', 'Aldwych Theatre', '2018-08-29 14:30:00'),
(7, 'Fascinating Aida', 'Apollo Theatre', '2016-11-23 15:00:00'),
(8, 'Motown The Musical', 'Criterion Theatre', '2017-08-24 19:00:00'),
(9, 'The Lion King', 'Old Vic Theatre', '2018-07-29 18:00:00'),
(10, 'Five Guys Named Moe', 'Donmar Warehouse', '2017-10-10 09:00:00'),
(11, 'Nativity! The Musical', 'Savoy Theatre', '2016-11-09 12:00:00'),
(12, 'OVO - Cirque du Soleil', 'Shaftesbury Theatre', '2018-07-14 13:00:00'),
(13, 'The Snowman', 'Duke of York''s Theatre', '2017-07-18 09:00:00'),
(14, 'An American in Paris', 'Garrick Theatre', '2018-03-05 11:00:00'),
(15, 'Annie', 'Hampstead Theatre - Downstairs', '2018-08-28 15:00:00'),
(16, 'Phantom of the Opera', 'Jermyn Street Theatre', '2018-07-17 18:00:00'),
(17, 'Ralph McTell', 'London Coliseum', '2018-03-24 15:00:00'),
(18, 'Big Fish The Musical', 'Lyric Theatre', '2016-12-19 13:00:00'),
(19, 'Randy Newman In Concert', 'Marble Arch Theatre', '2018-09-06 21:00:00'),
(20, 'Thriller Live', 'New London Theatre', '2017-07-22 14:00:00'),
(21, 'Kinky Boots', 'Piccadilly Theatre', '2017-04-10 17:00:00'),
(22, 'School of Rock', 'Queen''s Theatre', '2017-08-20 09:00:00'),
(23, 'Wicked', 'Noel Coward Theatre', '2017-05-14 18:00:00'),
(24, 'Scouting For Girls', 'Playhouse Theatre', '2017-12-19 15:00:00'),
(25, 'Wilde Creatures', 'Prince of Wales Theatre', '2018-07-20 10:00:00'),
(26, 'Les Miserables', 'London Palladium', '2017-07-27 16:00:00'),
(27, 'Mamma Mia', 'Novello Theatre', '2018-04-11 14:00:00'),
(28, 'Young Frankenstein', 'Barbican', '2018-08-23 18:00:00'),
(29, 'Dick Whittington', 'Palace Theatre', '2017-11-06 06:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `id` int(11) NOT NULL,
  `datetime` datetime NOT NULL,
  `booking_id` int(11) NOT NULL,
  `amount_paid` double NOT NULL,
  `card_details_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`id`, `datetime`, `booking_id`, `amount_paid`, `card_details_id`) VALUES
(1, '2017-02-27 07:34:00', 1, 201.01, 0),
(2, '2018-03-10 04:26:00', 2, 181.01, 0),
(3, '2016-11-03 18:11:00', 3, 140.49, 0),
(4, '2017-04-29 10:18:00', 4, 218.17, 0),
(5, '2018-03-26 22:52:00', 5, 159.24, 0),
(6, '2016-12-05 04:55:00', 6, 27.69, 0),
(7, '2017-03-17 14:13:00', 7, 132.89, 0),
(8, '2018-05-12 18:32:00', 8, 151.56, 0),
(9, '2017-11-08 12:46:00', 9, 163, 0),
(10, '2018-07-27 18:10:00', 10, 109.12, 0),
(11, '2017-08-31 15:53:00', 11, 24.2, 0),
(12, '2017-10-08 08:11:00', 12, 137.76, 0),
(13, '2017-08-29 02:24:00', 13, 11.19, 0),
(14, '2017-09-17 22:32:00', 14, 100.51, 0),
(15, '2018-09-02 11:55:00', 15, 194.73, 0),
(16, '2018-03-14 10:17:00', 16, 252.02, 0),
(17, '2017-05-14 13:32:00', 17, 142.09, 0),
(18, '2017-03-17 12:48:00', 18, 94.77, 0),
(19, '2016-12-02 09:35:00', 19, 188.1, 0),
(20, '2017-05-17 01:33:00', 20, 151.79, 0),
(21, '2018-01-22 00:56:00', 21, 104.08, 0),
(22, '2017-02-01 23:16:00', 22, 177.51, 0),
(23, '2018-01-11 22:34:00', 23, 285.23, 0),
(24, '2017-05-07 23:10:00', 24, 208.56, 0),
(25, '2018-07-01 20:05:00', 25, 273.51, 0),
(26, '2017-07-05 17:55:00', 26, 53.07, 0),
(27, '2017-04-09 14:45:00', 27, 11.31, 0),
(28, '2017-04-29 13:01:00', 28, 116.66, 0),
(29, '2016-12-17 16:10:00', 29, 283.24, 0),
(30, '2018-05-18 14:52:00', 30, 125.57, 0),
(31, '2017-08-05 13:32:00', 31, 69.14, 0),
(32, '2017-07-24 16:18:00', 32, 272.59, 0),
(33, '2018-10-09 12:37:00', 33, 279.99, 0),
(34, '2018-03-28 06:23:00', 34, 5.56, 0),
(35, '2018-08-23 16:28:00', 35, 128.93, 0),
(36, '2017-09-23 21:05:00', 36, 188.05, 0),
(37, '2016-12-07 22:53:00', 37, 76.75, 0),
(38, '2018-07-26 19:48:00', 38, 192.09, 0),
(39, '2017-05-29 23:45:00', 39, 80.91, 0),
(40, '2017-10-12 07:06:00', 40, 126.3, 0),
(41, '2017-01-08 09:52:00', 41, 296.36, 0),
(42, '2017-05-20 05:26:00', 42, 224.1, 0),
(43, '2018-01-20 05:22:00', 43, 47.22, 0),
(44, '2016-10-23 09:07:00', 44, 37.36, 0),
(45, '2017-02-11 07:04:00', 45, 9.25, 0),
(46, '2017-08-27 01:54:00', 46, 24.01, 0),
(47, '2016-12-07 04:47:00', 47, 203.94, 0),
(48, '2017-08-03 19:57:00', 48, 118.51, 0),
(49, '2018-10-05 00:34:00', 49, 32.61, 0),
(50, '2018-07-19 15:45:00', 50, 61.3, 0),
(51, '2017-08-05 16:10:00', 51, 186.19, 0),
(52, '2018-10-22 05:34:00', 52, 201.06, 0),
(53, '2018-02-14 23:22:00', 53, 33.46, 0),
(54, '2016-10-31 11:45:00', 54, 184.01, 0),
(55, '2017-09-11 00:12:00', 55, 90.61, 0),
(56, '2018-02-26 08:23:00', 56, 153.51, 0),
(57, '2018-01-29 13:26:00', 57, 172.38, 0),
(58, '2018-06-22 09:40:00', 58, 16.85, 0),
(59, '2017-07-17 06:20:00', 59, 65.4, 0),
(60, '2017-12-27 09:40:00', 60, 117.39, 0),
(61, '2018-09-04 20:48:00', 61, 83.9, 0),
(62, '2017-11-25 14:00:00', 62, 289.7, 0),
(63, '2018-03-12 05:02:00', 63, 141.07, 0),
(64, '2017-11-11 14:07:00', 64, 101.9, 0),
(65, '2018-01-08 07:53:00', 65, 173.15, 0),
(66, '2016-11-26 10:03:00', 66, 192.17, 0),
(67, '2017-04-30 13:57:00', 67, 293.68, 0),
(68, '2017-04-11 07:14:00', 68, 94.72, 0),
(69, '2018-03-18 15:56:00', 69, 283.96, 0),
(70, '2018-05-28 02:22:00', 70, 46.6, 0),
(71, '2018-06-18 22:51:00', 71, 104.5, 0),
(72, '2017-11-21 04:49:00', 72, 69.48, 0),
(73, '2016-12-24 17:57:00', 73, 210.74, 0),
(74, '2018-03-16 11:54:00', 74, 263.01, 0),
(75, '2018-06-29 00:19:00', 75, 206.6, 0),
(76, '2017-11-17 12:58:00', 76, 108.2, 0),
(77, '2018-01-16 16:37:00', 77, 36.89, 0),
(78, '2017-05-05 06:03:00', 78, 57.34, 0),
(79, '2018-05-15 04:08:00', 79, 219.55, 0),
(80, '2016-11-30 18:12:00', 80, 11.74, 0),
(81, '2017-03-14 21:13:00', 81, 268.99, 0),
(82, '2017-08-10 08:24:00', 82, 89.66, 0),
(83, '2018-09-05 05:05:00', 83, 221.99, 0),
(84, '2017-02-25 14:25:00', 84, 1.73, 0),
(85, '2017-05-23 03:13:00', 85, 290.69, 0),
(86, '2017-05-13 20:45:00', 86, 187.47, 0),
(87, '2018-02-14 09:21:00', 87, 237.72, 0),
(88, '2018-01-28 20:36:00', 88, 104.6, 0),
(89, '2018-07-05 22:07:00', 89, 197, 0),
(90, '2017-01-19 21:56:00', 90, 272.77, 0),
(91, '2018-08-23 14:30:00', 91, 218.01, 0),
(92, '2017-01-03 04:19:00', 92, 17.47, 0),
(93, '2017-06-29 02:36:00', 93, 26.85, 0),
(94, '2018-04-23 01:10:00', 94, 210.15, 0),
(95, '2017-06-03 13:33:00', 95, 191.53, 0),
(96, '2018-02-28 22:29:00', 96, 112.31, 0),
(97, '2018-07-31 13:30:00', 97, 93.15, 0),
(98, '2018-09-02 19:32:00', 98, 160.3, 0),
(99, '2018-04-23 05:33:00', 99, 159.8, 0),
(100, '2017-04-22 13:54:00', 100, 286.21, 0),
(101, '2018-10-21 15:17:22', 101, 201.01, 0),
(102, '2018-06-23 23:40:05', 102, 181.01, 0),
(103, '2017-03-20 11:33:32', 103, 140.49, 0),
(104, '2017-03-06 22:26:56', 104, 218.17, 0),
(105, '2018-10-09 01:01:18', 105, 159.24, 0),
(106, '2017-07-11 09:57:46', 106, 27.69, 0),
(107, '2018-10-05 13:42:50', 107, 132.89, 0),
(108, '2017-08-10 08:45:16', 108, 151.56, 0),
(109, '2017-05-03 00:03:19', 109, 163, 0),
(110, '2017-06-13 09:00:02', 110, 109.12, 0),
(111, '2018-08-02 17:08:22', 111, 24.2, 0),
(112, '2017-07-28 00:10:43', 112, 137.76, 0),
(113, '2016-11-29 13:09:30', 113, 11.19, 0),
(114, '2017-10-17 17:48:48', 114, 100.51, 0),
(115, '2016-12-24 16:00:58', 115, 194.73, 0),
(116, '2018-07-07 03:16:40', 116, 252.02, 0),
(117, '2016-12-02 10:42:24', 117, 142.09, 0),
(118, '2018-10-02 10:13:14', 118, 94.77, 0),
(119, '2017-04-16 02:14:04', 119, 188.1, 0),
(120, '2018-05-30 18:29:04', 120, 151.79, 0),
(121, '2018-09-26 10:29:49', 121, 104.08, 0),
(122, '2018-07-12 23:38:49', 122, 177.51, 0),
(123, '2017-10-27 17:31:25', 123, 285.23, 0),
(124, '2017-06-09 05:34:06', 124, 208.56, 0),
(125, '2016-12-04 00:41:45', 125, 273.51, 0),
(126, '2017-11-01 02:06:12', 126, 53.07, 0),
(127, '2017-05-19 23:18:33', 127, 11.31, 0),
(128, '2016-11-05 23:54:38', 128, 116.66, 0),
(129, '2018-10-07 14:21:23', 129, 283.24, 0),
(130, '2017-03-13 04:06:33', 130, 125.57, 0),
(131, '2016-12-02 09:56:37', 131, 69.14, 0),
(132, '2017-05-24 20:51:51', 132, 272.59, 0),
(133, '2017-05-13 23:02:33', 133, 279.99, 0),
(134, '2017-11-08 00:31:13', 134, 5.56, 0),
(135, '2017-08-25 15:40:53', 135, 128.93, 0),
(136, '2018-05-25 07:16:09', 136, 188.05, 0),
(137, '2018-07-28 19:32:40', 137, 76.75, 0),
(138, '2018-05-28 10:39:26', 138, 192.09, 0),
(139, '2018-01-12 14:24:10', 139, 80.91, 0),
(140, '2018-09-02 06:04:10', 140, 126.3, 0),
(141, '2017-09-25 15:11:05', 141, 296.36, 0),
(142, '2018-01-29 09:34:06', 142, 224.1, 0),
(143, '2018-10-19 11:11:10', 143, 47.22, 0),
(144, '2017-09-17 00:00:51', 144, 37.36, 0),
(145, '2018-01-31 06:52:39', 145, 9.25, 0),
(146, '2017-06-03 18:41:20', 146, 24.01, 0),
(147, '2017-09-01 02:17:46', 147, 203.94, 0),
(148, '2017-01-29 06:44:27', 148, 118.51, 0),
(149, '2018-08-02 12:56:41', 149, 32.61, 0),
(150, '2017-10-08 05:57:01', 150, 61.3, 0),
(151, '2018-06-07 10:09:41', 151, 186.19, 0),
(152, '2018-03-12 01:05:02', 152, 201.06, 0),
(153, '2018-09-03 10:21:09', 153, 33.46, 0),
(154, '2017-07-24 19:52:44', 154, 184.01, 0),
(155, '2018-08-27 03:25:11', 155, 90.61, 0),
(156, '2016-12-22 13:38:07', 156, 153.51, 0),
(157, '2018-04-27 00:06:20', 157, 172.38, 0),
(158, '2017-09-27 00:32:38', 158, 16.85, 0),
(159, '2017-10-24 22:40:46', 159, 65.4, 0),
(160, '2017-08-06 23:18:37', 160, 117.39, 0),
(161, '2017-04-23 09:34:35', 161, 83.9, 0),
(162, '2018-04-22 08:21:31', 162, 289.7, 0),
(163, '2018-03-27 02:14:39', 163, 141.07, 0),
(164, '2017-01-26 08:24:20', 164, 101.9, 0),
(165, '2018-02-13 12:50:36', 165, 173.15, 0),
(166, '2017-03-08 09:28:12', 166, 192.17, 0),
(167, '2017-06-16 09:05:12', 167, 293.68, 0),
(168, '2017-12-04 23:01:32', 168, 94.72, 0),
(169, '2017-06-05 06:16:59', 169, 283.96, 0),
(170, '2018-09-02 03:20:20', 170, 46.6, 0),
(171, '2017-03-24 10:46:58', 171, 104.5, 0),
(172, '2018-03-13 15:17:09', 172, 69.48, 0),
(173, '2017-04-10 21:35:25', 173, 210.74, 0),
(174, '2018-05-23 05:13:36', 174, 263.01, 0),
(175, '2017-05-18 01:14:14', 175, 206.6, 0),
(176, '2017-06-12 02:14:23', 176, 108.2, 0),
(177, '2017-08-04 17:17:14', 177, 36.89, 0),
(178, '2017-01-20 19:59:01', 178, 57.34, 0),
(179, '2018-07-31 09:56:39', 179, 219.55, 0),
(180, '2017-03-18 00:04:08', 180, 11.74, 0),
(181, '2018-03-11 21:56:35', 181, 268.99, 0),
(182, '2017-08-05 08:05:51', 182, 89.66, 0),
(183, '2017-11-29 06:14:48', 183, 221.99, 0),
(184, '2018-01-18 03:07:58', 184, 1.73, 0),
(185, '2017-02-14 15:19:24', 185, 290.69, 0),
(186, '2017-12-24 08:32:40', 186, 187.47, 0),
(187, '2018-04-13 08:44:45', 187, 237.72, 0),
(188, '2017-04-21 02:40:00', 188, 104.6, 0),
(189, '2017-10-29 13:21:58', 189, 197, 0),
(190, '2018-06-12 06:13:10', 190, 272.77, 0),
(191, '2016-12-31 15:21:45', 191, 218.01, 0),
(192, '2017-04-28 03:05:43', 192, 17.47, 0),
(193, '2018-04-07 11:16:09', 193, 26.85, 0),
(194, '2018-02-10 10:55:20', 194, 210.15, 0),
(195, '2018-09-01 16:56:43', 195, 191.53, 0),
(196, '2016-12-14 23:46:06', 196, 112.31, 0),
(197, '2017-07-03 22:33:08', 197, 93.15, 0),
(198, '2018-06-28 07:18:11', 198, 160.3, 0),
(199, '2017-08-20 01:37:38', 199, 159.8, 0),
(200, '2018-06-16 06:35:17', 200, 286.21, 0),
(201, '2018-03-08 04:23:26', 201, 201.01, 0),
(202, '2018-05-12 14:41:37', 202, 181.01, 0),
(203, '2018-02-07 18:06:05', 203, 140.49, 0),
(204, '2018-07-24 18:20:51', 204, 218.17, 0),
(205, '2016-12-17 05:38:53', 205, 159.24, 0),
(206, '2017-11-13 02:19:27', 206, 27.69, 0),
(207, '2018-09-22 05:04:58', 207, 132.89, 0),
(208, '2018-06-14 21:38:16', 208, 151.56, 0),
(209, '2017-09-01 07:27:54', 209, 163, 0),
(210, '2017-02-14 19:24:42', 210, 109.12, 0),
(211, '2018-08-14 08:09:08', 211, 24.2, 0),
(212, '2017-11-05 19:29:17', 212, 137.76, 0),
(213, '2018-05-01 00:39:51', 213, 11.19, 0),
(214, '2018-10-16 21:02:54', 214, 100.51, 0),
(215, '2017-06-20 11:20:25', 215, 194.73, 0),
(216, '2017-10-25 07:07:23', 216, 252.02, 0),
(217, '2018-05-23 00:43:18', 217, 142.09, 0),
(218, '2018-02-09 01:39:14', 218, 94.77, 0),
(219, '2017-04-25 13:56:00', 219, 188.1, 0),
(220, '2018-01-20 02:24:43', 220, 151.79, 0),
(221, '2018-03-12 23:48:41', 221, 104.08, 0),
(222, '2016-12-28 08:58:03', 222, 177.51, 0),
(223, '2018-06-14 05:44:35', 223, 285.23, 0),
(224, '2018-06-19 21:57:54', 224, 208.56, 0),
(225, '2018-09-15 12:29:01', 225, 273.51, 0),
(226, '2017-05-18 12:56:16', 226, 53.07, 0),
(227, '2017-03-25 08:18:45', 227, 11.31, 0),
(228, '2016-11-24 05:02:16', 228, 116.66, 0),
(229, '2018-08-09 19:19:15', 229, 283.24, 0),
(230, '2018-05-14 19:29:31', 230, 125.57, 0),
(231, '2017-06-24 10:54:46', 231, 69.14, 0),
(232, '2018-09-06 00:36:22', 232, 272.59, 0),
(233, '2018-10-15 15:30:25', 233, 279.99, 0),
(234, '2018-05-27 01:21:21', 234, 5.56, 0),
(235, '2018-10-12 20:19:37', 235, 128.93, 0),
(236, '2018-05-01 02:47:35', 236, 188.05, 0),
(237, '2017-12-19 16:05:57', 237, 76.75, 0),
(238, '2018-10-14 04:58:22', 238, 192.09, 0),
(239, '2017-09-19 05:22:58', 239, 80.91, 0),
(240, '2017-06-28 14:16:11', 240, 126.3, 0),
(241, '2018-08-09 14:06:07', 241, 296.36, 0),
(242, '2017-07-29 14:33:34', 242, 224.1, 0),
(243, '2018-06-26 01:12:37', 243, 47.22, 0),
(244, '2017-12-21 18:22:21', 244, 37.36, 0),
(245, '2017-12-19 23:38:00', 245, 9.25, 0),
(246, '2017-02-05 22:35:54', 246, 24.01, 0),
(247, '2017-07-06 21:16:34', 247, 203.94, 0),
(248, '2017-10-15 10:43:26', 248, 118.51, 0),
(249, '2018-09-01 14:39:29', 249, 32.61, 0),
(250, '2016-11-06 21:43:28', 250, 61.3, 0),
(251, '2017-01-15 10:53:29', 251, 186.19, 0),
(252, '2016-10-23 08:44:58', 252, 201.06, 0),
(253, '2017-11-29 07:33:53', 253, 33.46, 0),
(254, '2017-04-12 22:27:31', 254, 184.01, 0),
(255, '2017-07-12 12:57:24', 255, 90.61, 0),
(256, '2018-05-15 16:09:04', 256, 153.51, 0),
(257, '2017-01-19 02:23:07', 257, 172.38, 0),
(258, '2017-09-20 13:35:49', 258, 16.85, 0),
(259, '2018-08-24 03:20:39', 259, 65.4, 0),
(260, '2017-12-04 01:39:05', 260, 117.39, 0),
(261, '2018-08-25 20:57:17', 261, 83.9, 0),
(262, '2017-02-23 13:46:55', 262, 289.7, 0),
(263, '2017-11-30 07:04:19', 263, 141.07, 0),
(264, '2017-08-18 17:10:42', 264, 101.9, 0),
(265, '2017-01-10 19:48:26', 265, 173.15, 0),
(266, '2017-08-12 08:37:30', 266, 192.17, 0),
(267, '2016-11-18 04:57:47', 267, 293.68, 0),
(268, '2017-09-06 01:21:43', 268, 94.72, 0),
(269, '2017-04-18 08:50:50', 269, 283.96, 0),
(270, '2017-04-02 00:06:07', 270, 46.6, 0),
(271, '2017-12-12 11:41:48', 271, 104.5, 0),
(272, '2017-05-13 16:01:12', 272, 69.48, 0),
(273, '2017-12-24 17:12:19', 273, 210.74, 0),
(274, '2017-06-07 20:14:21', 274, 263.01, 0),
(275, '2018-06-28 04:58:30', 275, 206.6, 0),
(276, '2017-09-20 14:57:17', 276, 108.2, 0),
(277, '2018-01-15 05:19:58', 277, 36.89, 0),
(278, '2017-08-16 17:28:59', 278, 57.34, 0),
(279, '2018-07-25 01:17:03', 279, 219.55, 0),
(280, '2018-04-05 20:29:34', 280, 11.74, 0),
(281, '2018-04-25 21:17:38', 281, 268.99, 0),
(282, '2018-10-21 01:02:13', 282, 89.66, 0),
(283, '2018-10-14 21:03:22', 283, 221.99, 0),
(284, '2018-06-08 18:50:16', 284, 1.73, 0),
(285, '2018-07-27 12:31:15', 285, 290.69, 0),
(286, '2018-10-08 03:52:02', 286, 187.47, 0),
(287, '2018-05-11 22:25:34', 287, 237.72, 0),
(288, '2018-08-17 10:38:02', 288, 104.6, 0),
(289, '2018-08-21 08:31:19', 289, 197, 0),
(290, '2017-01-25 16:49:30', 290, 272.77, 0),
(291, '2017-02-11 16:13:05', 291, 218.01, 0),
(292, '2016-12-20 13:42:12', 292, 17.47, 0),
(293, '2018-07-12 17:10:36', 293, 26.85, 0),
(294, '2017-08-24 15:24:31', 294, 210.15, 0),
(295, '2018-09-05 05:25:03', 295, 191.53, 0),
(296, '2017-04-01 15:11:58', 296, 112.31, 0),
(297, '2018-10-12 18:20:03', 297, 93.15, 0),
(298, '2017-08-21 20:13:35', 298, 160.3, 0),
(299, '2017-04-12 04:45:46', 299, 159.8, 0),
(300, '2018-07-15 03:37:24', 300, 286.21, 0),
(301, '2017-11-03 00:00:00', 326, 10.99, 0),
(302, '2017-11-03 14:15:30', 324, 10.99, 0),
(303, '2017-11-03 14:24:02', 0, 10.99, 0),
(304, '2017-11-03 14:28:21', 326, 464.81, 0),
(305, '2017-11-03 14:30:09', 327, 464.81, 0),
(306, '2017-11-03 14:30:54', 328, 464.81, 0),
(307, '2017-11-03 14:33:49', 330, 464.81, 0),
(308, '2017-11-03 15:07:56', 331, 683.5, 0),
(309, '2017-11-03 15:10:02', 332, 683.5, 0),
(310, '2017-11-03 15:11:59', 333, 683.5, 0),
(311, '2017-11-03 15:13:41', 334, 863.56, 0),
(312, '2017-11-03 15:14:14', 335, 863.56, 0),
(313, '2017-11-03 15:16:59', 337, 1290.77, 0),
(314, '2017-11-03 16:13:00', 338, 434.82, 0),
(315, '2017-11-03 16:13:42', 339, 1111.7, 0),
(316, '2017-11-03 16:14:40', 340, 1111.7, 0),
(317, '2017-11-06 10:12:14', 341, 464.81, 0),
(318, '2017-11-13 11:05:59', 362, 111.99, 0),
(319, '2017-11-13 12:31:44', 363, 111.99, 0),
(320, '2017-11-13 12:33:04', 364, 111.99, 0),
(321, '2017-11-13 12:35:16', 365, 111.99, 0),
(322, '2017-11-13 12:41:40', 366, 111.99, 5),
(323, '2017-11-13 13:32:10', 367, 111.99, 6),
(324, '2017-11-13 13:42:29', 368, 448.47, 7),
(325, '2017-11-13 16:40:46', 369, 309.26, 8),
(326, '2017-11-16 10:54:27', 370, 153.07, 9),
(327, '2017-11-16 10:55:53', 371, 153.07, 10),
(328, '2017-11-16 11:40:09', 372, 114.03, 11),
(329, '2017-11-16 11:55:41', 373, 202.99, 12),
(330, '2017-11-16 14:40:52', 374, 226.96, 13),
(331, '2017-11-16 14:41:54', 375, 226.96, 14),
(332, '2017-11-16 14:52:04', 376, 299.43, 15),
(333, '2017-11-16 14:53:14', 377, 299.43, 16),
(334, '2017-11-16 14:57:07', 378, 299.43, 17),
(335, '2017-11-16 15:29:52', 379, 129.74, 18),
(336, '2017-11-16 15:45:02', 380, 1452.78, 19),
(337, '2017-11-16 15:45:58', 381, 1452.78, 20),
(338, '2017-11-17 09:43:52', 383, 1.99, 10),
(339, '2017-11-17 09:47:05', 384, 8.99, 21),
(340, '2017-11-17 09:50:22', 385, 1.99, 22),
(341, '2017-11-17 10:40:22', 386, 149.81, 23),
(342, '2017-11-17 10:41:13', 387, 262.74, 10),
(343, '2017-11-17 10:42:53', 388, 1113.52, 24),
(344, '2017-11-17 12:00:58', 389, 205.17, 10),
(345, '2017-11-17 12:01:47', 390, 205.17, 10),
(346, '2017-11-17 12:06:11', 391, 205.17, 10),
(347, '2017-11-17 12:11:16', 392, 205.17, 10),
(348, '2017-11-17 12:11:40', 393, 205.17, 10),
(349, '2017-11-17 12:17:21', 394, 205.17, 10),
(350, '2017-11-17 12:17:33', 395, 205.17, 10),
(351, '2017-11-17 12:22:43', 396, 205.17, 10),
(352, '2017-11-17 12:23:53', 397, 205.17, 10),
(353, '2017-11-17 12:24:10', 398, 205.17, 10),
(354, '2017-11-17 14:31:55', 399, 114.03, 10),
(355, '2017-11-17 14:39:15', 400, 114.03, 10),
(356, '2017-11-17 14:42:42', 401, 854.8499999999999, 10),
(357, '2017-11-17 14:48:51', 402, 869.9799999999999, 10),
(359, '2017-11-17 14:55:30', 404, 232.32, 10),
(361, '2017-11-17 14:59:22', 406, 25.32, 25),
(362, '2017-11-17 15:07:50', 407, 957.46, 26),
(363, '2017-11-17 15:21:06', 408, 107.05, 27),
(364, '2017-11-17 15:56:09', 409, 43.64, 28),
(365, '2017-11-17 16:05:23', 410, 123.86, 10),
(366, '2017-11-17 16:08:51', 411, 340.65, 10),
(367, '2017-11-17 16:28:19', 412, 107.05, 10),
(368, '2017-11-17 16:40:45', 413, 18407.38, 29),
(369, '2017-11-17 16:42:21', 414, 72.47, 10),
(370, '2017-11-20 10:07:56', 415, 152.25, 10),
(371, '2017-11-20 10:08:35', 416, 95.45, 10),
(372, '2017-11-20 13:37:30', 417, 114.03, 30),
(373, '2017-11-20 13:38:12', 418, 654.28, 31),
(374, '2017-11-20 14:06:49', 419, 131.78, 32),
(375, '2017-11-20 14:11:25', 420, 18.62, 33),
(376, '2017-11-20 14:36:12', 421, 133.21, 34),
(377, '2017-11-20 15:29:37', 422, 213.76000000000002, 33),
(378, '2017-11-23 14:14:31', 423, 257.9, 33),
(379, '2017-11-29 09:59:46', 424, 131.78, 33),
(380, '2017-11-29 10:03:50', 425, 290.09000000000003, 33);

-- --------------------------------------------------------

--
-- Table structure for table `ticket_type`
--

CREATE TABLE `ticket_type` (
  `id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `capacity` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `price` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ticket_type`
--

INSERT INTO `ticket_type` (`id`, `event_id`, `capacity`, `description`, `price`) VALUES
(1, 27, 416, 'Stalls', 40.08),
(2, 10, 400, 'Standing', 60.13),
(3, 2, 154, 'Upper Circle', 112.93),
(4, 24, 63, 'Rear Stalls', 78.64),
(5, 17, 223, 'Box', 125.89),
(6, 10, 183, 'Stalls', 36.45),
(7, 29, 254, 'Stalls', 124.15),
(8, 22, 171, 'Box', 12.83),
(9, 20, 335, 'Standing', 140.11),
(10, 20, 140, 'Upper Circle', 130.31),
(11, 23, 385, 'Upper Circle', 132.87),
(12, 3, 200, 'Standing', 16.81),
(13, 18, 65, 'Stalls', 38.98),
(14, 11, 318, 'Circle', 71.18),
(15, 24, 371, 'Standing', 127.61),
(16, 7, 124, 'Circle', 95.45),
(17, 17, 316, 'Circle', 149.33),
(18, 18, 290, 'Standing', 125.36),
(19, 21, 82, 'Rear Stalls', 133.11),
(20, 6, 154, 'Box', 148.17),
(21, 9, 101, 'Upper Circle', 77.34),
(22, 12, 255, 'Rear Stalls', 147.86),
(23, 25, 261, 'Upper Circle', 32.42),
(24, 23, 357, 'Box', 125.57),
(25, 11, 244, 'Upper Circle', 64.19),
(26, 6, 270, 'Upper Circle', 75.73),
(27, 19, 407, 'Standing', 74.38),
(28, 11, 150, 'Box', 143.87),
(29, 1, 0, 'Stalls', 107.05),
(30, 17, 173, 'Stalls', 74.72),
(31, 7, 386, 'Upper Circle', 25.32),
(32, 27, 131, 'Box', 128.66),
(33, 8, 194, 'Upper Circle', 20.47),
(34, 10, 268, 'Circle', 143.14),
(35, 15, 447, 'Stalls', 112.8),
(36, 11, 280, 'Rear Stalls', 43.64),
(37, 29, 236, 'Upper Circle', 59.53),
(38, 15, 294, 'Box', 131.26),
(39, 14, 299, 'Upper Circle', 90.03),
(40, 13, 337, 'Upper Circle', 133.34),
(41, 11, 60, 'Stalls', 128.47),
(42, 23, 448, 'Circle', 18.62),
(43, 1, 0, 'Circle', 72.47),
(44, 20, 417, 'Rear Stalls', 100.75),
(45, 4, 280, 'Standing', 62.03),
(46, 16, 180, 'Upper Circle', 67.5),
(47, 3, 188, 'Upper Circle', 103.01),
(48, 22, 440, 'Rear Stalls', 26.33),
(49, 10, 67, 'Box', 55.11),
(50, 4, 283, 'Box', 88.96),
(51, 3, 400, 'Circle', 91.69),
(52, 27, 67, 'Upper Circle', 74.55),
(53, 24, 381, 'Stalls', 25.74),
(54, 26, 76, 'Standing', 105.41),
(55, 7, 58, 'Stalls', 14.08),
(56, 21, 296, 'Stalls', 131.78),
(57, 22, 168, 'Upper Circle', 104.02),
(58, 26, 247, 'Rear Stalls', 132.23),
(59, 15, 60, 'Rear Stalls', 59.97),
(60, 25, 102, 'Circle', 125.75),
(61, 29, 288, 'Rear Stalls', 80.22),
(62, 13, 298, 'Standing', 143.36),
(63, 22, 363, 'Circle', 69.94),
(64, 19, 161, 'Circle', 111.66),
(65, 4, 100, 'Rear Stalls', 129.61),
(66, 18, 380, 'Box', 26.73),
(67, 6, 334, 'Rear Stalls', 15.13),
(68, 28, 378, 'Box', 97.65),
(69, 16, 416, 'Stalls', 81.92),
(70, 24, 193, 'Upper Circle', 43.93),
(71, 29, 164, 'Standing', 67.5),
(72, 8, 338, 'Standing', 86.91),
(73, 1, 345, 'Box', 87.79),
(74, 22, 124, 'Stalls', 49.15),
(75, 5, 67, 'Standing', 114.03),
(76, 18, 446, 'Rear Stalls', 113.49);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `booking_line`
--
ALTER TABLE `booking_line`
  ADD PRIMARY KEY (`id`),
  ADD KEY `booking_id` (`booking_id`),
  ADD KEY `ticket_type_id` (`ticket_type_id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `customer_card_details`
--
ALTER TABLE `customer_card_details`
  ADD PRIMARY KEY (`cardid`),
  ADD KEY `cardid` (`cardid`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `booking_id` (`booking_id`),
  ADD KEY `id` (`id`),
  ADD KEY `card_details_id` (`card_details_id`);

--
-- Indexes for table `ticket_type`
--
ALTER TABLE `ticket_type`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `event_id` (`event_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `booking`
--
ALTER TABLE `booking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=426;
--
-- AUTO_INCREMENT for table `booking_line`
--
ALTER TABLE `booking_line`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=667;
--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;
--
-- AUTO_INCREMENT for table `customer_card_details`
--
ALTER TABLE `customer_card_details`
  MODIFY `cardid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=381;
--
-- AUTO_INCREMENT for table `ticket_type`
--
ALTER TABLE `ticket_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `booking`
--
ALTER TABLE `booking`
  ADD CONSTRAINT `booking_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `booking_line`
--
ALTER TABLE `booking_line`
  ADD CONSTRAINT `booking_line_ibfk_2` FOREIGN KEY (`ticket_type_id`) REFERENCES `ticket_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `booking_line_ibfk_3` FOREIGN KEY (`booking_id`) REFERENCES `booking` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ticket_type`
--
ALTER TABLE `ticket_type`
  ADD CONSTRAINT `ticket_type_ibfk_1` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
