# README #

### Purpose of the repository ###

This repository is to demostrate my coding experience and ability.

### Browse or download code ###

Examples of my code can be explored via Source or downloading the repository via Download.

Code is seperated into three folders. Acorn Aid, JAVA and Python.

Acorn Aid is a help desk solution developed in PHP and MySQL. Full details can be found at [http://acornaid.com/]

JAVA and Python folders include snippets of code from various projects.