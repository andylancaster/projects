<?php 
// acorn aid key
$aakey = "FREE";

// database settings
$pdo['host'] = "localhost";
$pdo['user'] =  "root";
$pdo['pass'] = "";
$pdo['db'] = "aa_210";

// table settings
$pdo_t['t_ticket'] = "aa_ticket";
$pdo_t['t_ticket_updates'] = "aa_ticket_updates";
$pdo_t['t_groups'] = "aa_groups";
$pdo_t['t_priorities'] = "aa_priorities";
$pdo_t['t_settings'] = "aa_settings";
$pdo_t['t_users'] = "aa_users";
$pdo_t['t_users_skills'] = "aa_users_skill";
$pdo_t['t_canned_msg'] = "aa_canned_messages";
$pdo_t['t_custom_fields'] = "aa_custom_fields";
$pdo_t['t_kb'] = "aa_kb";
$pdo_t['t_kb_groups'] = "aa_kb_groups";
$pdo_t['t_kb_comments'] = "aa_kb_comments";
$pdo_t['t_slas'] = "aa_slas";
$pdo_t['t_iemail'] = "aa_imail";
$pdo_t['t_formwiz_forms'] = "aa_forms";
$pdo_t['t_formwiz_form_fields'] = "aa_forms_fields";
$pdo_t['t_formwiz_form_values'] = "aa_forms_values";
?>