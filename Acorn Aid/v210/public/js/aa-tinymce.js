// JavaScript Document

// convert textareas into tinymce
tinymce.init({
	selector:'textarea',
	selector : "textarea:not(.notinymce)",
	content_style: ".mce-content-body {font-size:13px;font-family:Segoe UI Semilight, Segoe UI,Segoe UI Web Regular,Segoe UI Symbol,Helvetica Neue,BBAlpha Sans,S60 Sans,Arial,sans-serif;}",
	toolbar_items_size : 'small',
	plugins: "link, textcolor, image",
	toolbar: "bold italic underline | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent |  cut copy paste | undo redo | link image",
 	menubar : false,
	statusbar : false,
	browser_spellcheck : true,
	image_advtab: true,
	skin: "custom"
});
