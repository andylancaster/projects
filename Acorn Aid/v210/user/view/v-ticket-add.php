<?php
// ADD IF STATEMENT TO CHECK IF GUEST ACCESS OR REG IS ALLOWED!!!!
// if guest access not allowed then check for user sign in

if (!isset($_GET["guest"])) {
	if ($db_user_reg  == 1) {
		if (!isset($_SESSION['aaname'])) {
			@header ("Location: index.php?p=user-access&r=ticket-add&formid=".$_GET["formid"]);
		}
	}
}

$choose_priority = get_settings("Ticket_Priority");
$show_sla = get_settings("Ticket_SLA");
$file_attachment = get_settings("File_Enabled");

// get groups
$groups = aaModelGetGroups();
$array_of_groups = $groups->fetchAll();
$no_of_groups = $groups->rowCount();

// get priorities
$priorities = aaModelGetPriorities();
$array_of_priorities = $priorities->fetchAll();
$no_of_priorities = $priorities->rowCount();

// get form details e.g. group and priority
$form = aaGetFormsDetails($_GET["formid"]);
$form_array = $form->fetch();

if (isset($_POST["Add"])) {

	//print_r($_POST["custom"]);
	aaModelInsertRequest($_POST["user"],
						$_POST["user_email"],
						$_POST["category"],
						$_POST["priority"],
						$_POST["subject"],
						$_POST["notes"],
						@$_FILES["file"],
						@$_POST["custom"],
						$_GET["formid"],
						@$_POST["code"]);

}
?>
<div class="margin-body form" style="clear:both">
<h2><?php echo $lang['ticket-add-title']; ?></h2>
<hr />
	<?php echo read_session('aaerror-add-null'); ?>
	<?php echo read_session('aaerror-add-email'); ?>
	<?php echo read_session('aaerror-file'); ?>
	<?php echo read_session('aaerror-add-code'); ?>
    <form name="form1" method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>" enctype="multipart/form-data">
	<?php echo decode_entities($form_array["FormDesc"]); ?>

    <?php
	if (isset($_SESSION['aaname'])) {
   		echo '<input name="user" type="text" id="user" value="'.$_SESSION['aaname'].'" hidden>';
	} else {
		echo '<div class="form-field">';
		echo '<label for="user">'.$lang['ticket-add-name'].' *</label>';
    	echo '<input required pattern=".*\S+.*" name="user" type="text" id="user" value="'; cached_fields(@$_POST['user']);
		echo '" placeholder="'.$lang['ticket-add-name'].'">';
		echo '</div>';
	}
	?>

    <?php
	if (isset($_SESSION['aaemail'])) {
    	echo '<input name="user_email" type="email" id="user_email" value="'.$_SESSION['aaemail'].'" hidden>';
	} else {
		echo '<div class="form-field">';
		echo '<label for="user_email">'.$lang['ticket-add-email'].' *</label>';
    	echo '<input required pattern=".*\S+.*" name="user_email" type="email" id="user_email" value="'; cached_fields(@$_POST['user_email']);
		echo '" placeholder="'.$lang['ticket-add-email'].'">';
		echo '</div>';
	}
	?>

	<?php
	// if there's only one group then don't show select option
	if ($no_of_groups == 1 || isset($form_array["FormGroup"])) {
		$display_cat_opt = "style=\"display:none\"";
	}
	?>
	<div class="form-field" <?php echo @$display_cat_opt; ?>>
	<label for="category"><?php echo $lang['ticket-add-group']; ?> *</label>
	  <?php
	  if (isset($form_array["FormGroup"])) {
		echo '<input type="hidden" name="category" id="category" value="'.$form_array["FormGroup"].'"">';
	  } else {
		echo '<select class="NoSearchSelect2 ticket-add-select" name="category" id="category" >';
		foreach ($array_of_groups as $group) {

			if ($group["Def"] == 1) {

				echo "<option value=\"".$group["Cat_ID"]."\" selected=\"selected\">".decode_entities($group["Category"])."</option>";

			} else {

				echo "<option value=\"".$group["Cat_ID"]."\">".decode_entities($group["Category"])."</option>";

			}

		}
		echo '</select>';
	  }
	  ?>
	</div>

	<?php
	// if there's only one group then don't show select option
	if ($no_of_priorities == 1 || @$choose_priority == 0 || isset($form_array["FormPriority"])) {
		$display_priority_opt = "style=\"display:none\"";
	}
	?>
	<div class="form-field" <?php echo @$display_priority_opt; ?>>
	<label for="priority"><?php echo $lang['ticket-add-priority']; ?> *</label>
	<?php
	if (isset($form_array["FormPriority"])) {
	  echo '<input type="hidden" name="priority" id="priority" value="'.$form_array["FormPriority"].'"">';
	} else {
	  echo '<select class="NoSearchSelect2 ticket-add-select" name="priority" id="priority">';
		foreach ($array_of_priorities as $priority) {

		  if ($priority["Def"] == 1) {

			echo "<option value=\"".$priority["Level_ID"]."\" selected=\"selected\">".decode_entities($priority["Level"])."</option>";

		  } else {

			echo "<option value=\"".$priority["Level_ID"]."\">".decode_entities($priority["Level"])."</option>";

		  }

	  }
	  echo '</select>';
	}
	?>
	</div>

	<?php
	if ($show_sla == 1) {
		echo '<div id="sla" class="form-field"></div>';
    }

	// custom fields go here
	$customfields =	aaGetFormsFields($_GET["formid"]);
	$array_of_customfields = $customfields->fetchAll();
	//print_r($array_of_customfields);

	unset($_SESSION["custom_fields"]);

	foreach ($array_of_customfields as $cf) {

		$fieldname = ucfirst(str_replace("_", " ", $cf["Field_Name"]));
		// add html5 required
		$fieldreq = ($cf["Field_Required"] == 1) ? 'required pattern=".*\S+.*"' : '';
		$option = explode(",",$cf["Field_Options"]);
		switch ($cf["Field_Type"]) {
			case "Text":
			echo "<div class=\"form-field\"><label>".$fieldname."</label>";
			echo "<input ".$fieldreq." name=\"custom[".$cf["FID"]."]\" type=\"text\" maxlength=\"".$cf["Field_MaxLen"]."\" value=\"".@$_POST["custom"][$cf['FID']]."\" placeholder=\"".$fieldname."\" /></div>";
			?>
			<!-- <p class="error"><?php read_session('aaerror-custom['.$cf["Field_Name"].']'); ?></p> -->
			<?php
			break;

			case "Textbox":
			echo "<div class=\"form-field\"><label>".$fieldname."</label>";
			echo "<textarea ".$fieldreq." name=\"custom[".$cf["FID"]."]\" maxlength=\"".$cf["Field_MaxLen"]."\" placeholder=\"".$fieldname."\">".@$_POST["custom"][$cf['FID']]."</textarea>";
			?>
			<p class="error"><?php read_session('aaerror-custom['.$cf["Field_Name"].']'); ?></p>
			<?php
			break;

			case "Select":
				echo "<div class=\"form-field\">";
				echo "<label>".$fieldname."</label>";
				echo "<select class=\"NoSearchSelect2\" name=\"custom[".$cf["FID"]."]\">";
				foreach($option as $sel_opt) {

					// if already selected
					if ($sel_opt == $_POST["custom"][$cf['FID']]) {

						echo "<option value=\"".$sel_opt."\" selected=\"selected\">".$sel_opt."</option>";

					} else {

						echo "<option value=\"".$sel_opt."\">".$sel_opt."</option>";

					}
				}
				echo "</select>";
				echo "</div>";
			break;

			case "Checkbox":
			echo "<div class=\"form-field\"><label>".$fieldname."</label>";

				// default checkbox value to check if null
				echo "<input style=\"display:none\" hidden name=\"custom[".$cf["FID"]."][]\" type=\"checkbox\" value=\"0\" checked />";
				foreach($option as $checkbox_opt) {

					if (isset($_POST["custom"][$cf['FID']])) {
						// checek if value is in field array
						if (in_array($checkbox_opt,$_POST["custom"][$cf['FID']])) {

							$checked = "checked";

						} else {

							$checked = "";

						}

					}

					echo "<div class=\"form-field\"><label class=\"form-field-inline-label\">".$checkbox_opt."</label><input ".$fieldreq." name=\"custom[".$cf["FID"]."][]\" type=\"checkbox\" value=\"".$checkbox_opt."\" ".@$checked."/></div>";

				}
				echo "</div>";
			break;

			case "Radio":
				echo "<p><label><strong>".$fieldname."</strong></label>";
				foreach($option as $radio_opt) {

					if ($radio_opt == $_POST["custom"][$cf['FID']]) {

						echo "<div class=\"form-field\"><label class=\"form-field-inline-label\">".$radio_opt."</label><input name=\"custom[".$cf["FID"]."]\" type=\"radio\" value=\"".$radio_opt."\" checked /></div>";

					} else if (!($_POST["custom"][$cf['Field_Name']])) {

						echo "<div class=\"form-field\"><label class=\"form-field-inline-label\">".$radio_opt."</label><input name=\"custom[".$cf["FID"]."]\" type=\"radio\" value=\"".$radio_opt."\" checked /></div>";

					} else {

						echo "<div class=\"form-field\"><label class=\"form-field-inline-label\">".$radio_opt."</label><input name=\"custom[".$cf["FID"]."]\" type=\"radio\" value=\"".$radio_opt."\" /></div>";

					}
				}
				echo "</p>";
			  break;

			}

	}

	//print_r($custom_set_val);

	?>
	<?php
	// hide default message if removed in form wizard
	if ($form_array["FormSubject"] != "") {
		echo '<input name="subject" type="hidden" id="subject" value="'.$form_array["FormSubject"].'">';
	} else {
	?>
		<div class="form-field">
		<label for="subject"><?php echo $lang['ticket-add-subject']; ?> *</label>
		<input required pattern=".*\S+.*" name="subject" type="text" id="subject" value="<?php cached_fields(@$_POST["subject"]); ?>" placeholder="Subject">
		</div>
	<?php
	}
	?>

	<?php
	// hide default message if removed in form wizard
	if ($form_array["RemoveFormMessage"] == 0) {
	?>
	<div class="form-field">
	<label><?php echo $lang['ticket-add-msg']; ?> *</label>
	<textarea name="notes" id="notes" rows="5" placeholder="Message"><?php if (isset($_POST["notes"])) { echo $_POST["notes"]; } ?></textarea>
	</div>
	<?php
	}
	?>

	<?php
	if ($file_attachment == 1) {
		if ($form_array["RemoveFormFile"] == 0) {
	?>
	<div id="fileuploads" class="form-field">
	<input type="hidden" id="aafilelimit" value="<?php echo get_settings("File_Limit"); ?>" />
	<label for="file"><?php echo $lang['ticket-add-files-add']; ?></label>
	<input class="aafile" name="file[]" type="file" multiple="multiple" />
	</div>
	<?php
	echo @$form_error['FILE'];
		}
	}
	?>

	<?php
	$db_antispam = get_settings("Ticket_Antispam");
    if (@$db_antispam == 1) {
    ?>

	<div class="form-field">
    <label for="secim"><?php echo $lang['ticket-add-sec-code']; ?> <strong><?php security_code (); ?></strong></label>
	<br>
    </div>

	<div class="form-field">
    <label for="code"><?php echo $lang['ticket-add-sec-code-ent']; ?></label>
    <input name="code" type="text" id="code" value="<?php if (isset($code)) { echo $code; } ?>" placeholder="<?php echo $lang['ticket-add-sec-code-ent']; ?>">
	</div>
    <?php
    }
    ?>

    <p><input name="Add" type="submit" class="btn" id="Add" value="<?php echo $lang['ticket-add-submit']; ?>" /></p>

    </form>
</div>
