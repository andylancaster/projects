<?php
$db_kb_enable = get_settings('KB_Enable');
$forms = aaGetForms("NULL");
$forms_array = $forms->fetchAll();
$forms_no = $forms->rowCount();
// is default form enabled
$formdefault = get_settings("FormWiz_DefaultFormDisable");
// default form name selected
$formdefaultname = get_settings("FormWiz_DefaultFormName");

// build navigation of forms
if (!is_numeric($formdefaultname)) {
    $addticketurl = '<li><a href="index.php?p=ticket-add">'.$lang['u-nav-s-request'].'</a>';
} else {
    $addticketurl = '<li><a href="index.php?p=ticket-add&formid='.$formdefaultname.'">'.$lang['u-nav-s-request'].'</a>';
}
// build navigation of forms if more than one form built
if ($forms_no > 0) {
    if ($formdefault == '0') {
    	@$submenu .= '<li><a href="index.php?p=ticket-add">'.$lang["set-formwiz-dform"].'</a></li>';
    }

    foreach ($forms_array as $ff) {

        @$submenu .= '<li><a href="index.php?p=ticket-add&formid='.$ff["FormID"].'">'.$ff["FormName"].'</a></li>';

    }
}
?>
<div class="margin-body">
    <div class="header">
    <h1><a id="title" href="index.php"><?php echo get_settings('Company_Name'); ?></a></h1>
    </div>
    <div id="langauge" class="header">
    		<?php
		select_langauge();
		?>
    </div>
    <div id="nav" style="clear:both">
        <nav id="primary_nav_wrap">
            <ul>
    <?php
    if ($db_kb_enable == 1) {
    ?>
    <li><a href="index.php"><?php echo $lang['u-nav-kb']; ?></a></li>
    <?php
    }
    ?>

	<?php
	//
	if (isset($_SESSION['aaname'])) {

    echo $addticketurl;
    ?>
    <ul>
        <?php echo $submenu; ?>
    </ul>
</li>
    <li><a href="index.php?p=dashboard"><?php echo $lang['u-nav-dashboard']; ?></a></li>
	<li><a href="index.php?p=profile"><?php echo $lang['u-nav-profile']; ?></a></li>
    <li><a href="logout.php"><?php echo $lang['u-nav-logout']; ?>  (<?php echo ucfirst($_SESSION['aaname']); ?>)</a></li>
    <?php
	} else {

		if ($db_user_reg == 1 || $db_guest_access == 1) {
		?>
		<li><a href="index.php?p=ticket-add"><?php echo $lang['u-nav-s-request']; ?></a>
        <ul>
            <?php echo $submenu; ?>
        </ul>
    </li>
		<?php
		}

		if ($db_guest_access == 1) {
		?>
		<li><a href="index.php?p=ticket-track"><?php echo $lang['u-nav-t-request']; ?></a></li>
		<?php
		}

	}
	?>
            </ul>
        </nav>
    </div>
    <br>
</div>
