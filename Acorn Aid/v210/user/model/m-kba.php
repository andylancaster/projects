<?php
// get kb article
function aaModelGetKBArticle ($kbaid) {

	global $pdo_conn, $pdo_t;

	$date_format = get_settings('Date_Format');

	$sel_article = "SELECT *,DATE_FORMAT(KB_Date_Added, '$date_format') AS KB_Date_Added FROM ".$pdo_t['t_kb']." AS k
								LEFT JOIN ".$pdo_t['t_users']." AS u ON u.UID = k.KB_Author
								LEFT JOIN ".$pdo_t['t_kb_groups']." AS kg ON kg.KBGROUPID = k.KB_Group
								WHERE k.KBID = :kbaid";

	$q = $pdo_conn->prepare($sel_article);
	$q->execute(array("kbaid" => $kbaid));

	return $q;

}

function aaModelGetKBComments($kbaid) {

	global $pdo_conn, $pdo_t;

	$date_format = get_settings('Date_Format');
	$kb_approval = get_settings('KB_Comments_Approval');
	$sql_kb_appv = ($kb_approval == 1) ? 'AND KBComApproved = 1' : '';

	$sel_kb_coms = "SELECT *,DATE_FORMAT(KBComDT, '$date_format') AS KB_Com_DT FROM ".$pdo_t['t_kb_comments']." WHERE KBID = :kbaid $sql_kb_appv ORDER BY KBComDT DESC";

	$q = $pdo_conn->prepare($sel_kb_coms);
	$q->execute(array("kbaid" => $kbaid));

	return $q;

}

// count kb article
function aaModelUpdateKBCount($kbaid) {

	global $pdo_conn, $pdo_t;

	$kba_u_ip = $_SERVER['REMOTE_ADDR'];

	$update_kba_count = "UPDATE ".$pdo_t['t_kb']." SET KB_Count = KB_Count + 1, KB_Count_IP=:kbip WHERE KBID = :kbaid AND (KB_Count_IP != :kbip)";

	$q = $pdo_conn->prepare($update_kba_count);
	$q->execute(array("kbaid" => $kbaid, "kbip" => $kba_u_ip));

}

// like kb article
function aaModelLikeKBArticle ($kbaid) {

	global $pdo_conn, $pdo_t;

	$kba_u_ip = $_SERVER['REMOTE_ADDR'];

	$kb_like = "UPDATE ".$pdo_t['t_kb'] ." SET KB_Like = KB_Like + 1, KB_Rating_IP=:kbip WHERE KBID = :kbaid AND (KB_Rating_IP != :kbip)";

	$q = $pdo_conn->prepare($kb_like);
	$q->execute(array("kbaid" => $kbaid, "kbip" => $kba_u_ip));
	header('Location: kba.php?kbid='.$kbaid);
}

// dislike kb article
function aaModelDislikeKBArticle ($kbaid) {

	global $pdo_conn, $pdo_t;

	$kba_u_ip = $_SERVER['REMOTE_ADDR'];

	$kb_dislike = "UPDATE ".$pdo_t['t_kb'] ." SET KB_Dislike = KB_Dislike + 1, KB_Rating_IP=:kbip WHERE KBID = :kbaid AND (KB_Rating_IP != :kbip)";

	$q = $pdo_conn->prepare($kb_dislike);
	$q->execute(array(":kbaid" => $kbaid, "kbip" => $kba_u_ip));

	header('Location: kba.php?kbid='.$kbaid);
}

function aaModelPostKBComment ($kbid, $com_name, $com_email, $kb_comment) {

	global $pdo_conn, $pdo_t, $lang;
	$now = timezone_time();
	$com_name = clean($com_name, TRUE);
	$com_email = clean($com_email, TRUE);
	$kb_comment = clean($kb_comment, FALSE);
	$kb_needs_appv = get_settings('KB_Comments_Approval');

	// insert each custom field
	$sql_i_kbc = "INSERT INTO ".$pdo_t['t_kb_comments']." (KBID, KBComDT, KBComName, KBComEmail, KBComment, KBComApproved)
	VALUES (:kbid, :com_dt, :com_name, :com_email, :kb_comment, :kb_approved)";
	$q_i_kbc = $pdo_conn->prepare($sql_i_kbc);
	if(!$q_i_kbc->execute(array( 'kbid' => $kbid, 'com_dt' => $now,'com_name' => $com_name, 'com_email' => $com_email, 'kb_comment' => $kb_comment, 'kb_approved' => 0 ))) {
		print_r($q_i_kbc->errorInfo());
	} else {
		$kbcommentsuccess = '<div class="success-msg"><b>'.$lang['kb-comments-post-ok'].'</b>';
		$kbcommentsuccess .= ($kb_needs_appv == 1) ? '<br />'.$lang['kb-comments-post-approval'] : '';
		$kbcommentsuccess .= '</div>';
		set_session("kbcommentsuccess", $kbcommentsuccess);
		header("Location:".$_SERVER["REQUEST_URI"].'#kbcomments');
		exit();
	}

}

?>
