-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 08, 2017 at 08:13 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aa_210_dev`
--

-- --------------------------------------------------------

--
-- Table structure for table `aa_calendar`
--

CREATE TABLE `aa_calendar` (
  `datefield` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `aa_calendar`
--

INSERT INTO `aa_calendar` (`datefield`) VALUES
('2017-01-01'),
('2017-01-02'),
('2017-01-03'),
('2017-01-04'),
('2017-01-05'),
('2017-01-06'),
('2017-01-07'),
('2017-01-08'),
('2017-01-09'),
('2017-01-10'),
('2017-01-11'),
('2017-01-12'),
('2017-01-13'),
('2017-01-14'),
('2017-01-15'),
('2017-01-16'),
('2017-01-17'),
('2017-01-18'),
('2017-01-19'),
('2017-01-20'),
('2017-01-21'),
('2017-01-22'),
('2017-01-23'),
('2017-01-24'),
('2017-01-25'),
('2017-01-26'),
('2017-01-27'),
('2017-01-28'),
('2017-01-29'),
('2017-01-30'),
('2017-01-31'),
('2017-02-01'),
('2017-02-02'),
('2017-02-03'),
('2017-02-04'),
('2017-02-05'),
('2017-02-06'),
('2017-02-07'),
('2017-02-08'),
('2017-02-09'),
('2017-02-10'),
('2017-02-11'),
('2017-02-12'),
('2017-02-13'),
('2017-02-14'),
('2017-02-15'),
('2017-02-16'),
('2017-02-17'),
('2017-02-18'),
('2017-02-19'),
('2017-02-20'),
('2017-02-21'),
('2017-02-22'),
('2017-02-23'),
('2017-02-24'),
('2017-02-25'),
('2017-02-26'),
('2017-02-27'),
('2017-02-28'),
('2017-03-01'),
('2017-03-02'),
('2017-03-03'),
('2017-03-04'),
('2017-03-05'),
('2017-03-06'),
('2017-03-07'),
('2017-03-08'),
('2017-03-09'),
('2017-03-10'),
('2017-03-11'),
('2017-03-12'),
('2017-03-13'),
('2017-03-14'),
('2017-03-15'),
('2017-03-16'),
('2017-03-17'),
('2017-03-18'),
('2017-03-19'),
('2017-03-20'),
('2017-03-21'),
('2017-03-22'),
('2017-03-23'),
('2017-03-24'),
('2017-03-25'),
('2017-03-26'),
('2017-03-27'),
('2017-03-28'),
('2017-03-29'),
('2017-03-30'),
('2017-03-31'),
('2017-04-01'),
('2017-04-02'),
('2017-04-03'),
('2017-04-04'),
('2017-04-05'),
('2017-04-06'),
('2017-04-07'),
('2017-04-08'),
('2017-04-09'),
('2017-04-10'),
('2017-04-11'),
('2017-04-12'),
('2017-04-13'),
('2017-04-14'),
('2017-04-15'),
('2017-04-16'),
('2017-04-17'),
('2017-04-18'),
('2017-04-19'),
('2017-04-20'),
('2017-04-21'),
('2017-04-22'),
('2017-04-23'),
('2017-04-24'),
('2017-04-25'),
('2017-04-26'),
('2017-04-27'),
('2017-04-28'),
('2017-04-29'),
('2017-04-30'),
('2017-05-01'),
('2017-05-02'),
('2017-05-03'),
('2017-05-04'),
('2017-05-05'),
('2017-05-06'),
('2017-05-07'),
('2017-05-08'),
('2017-05-09'),
('2017-05-10'),
('2017-05-11'),
('2017-05-12'),
('2017-05-13'),
('2017-05-14'),
('2017-05-15'),
('2017-05-16'),
('2017-05-17'),
('2017-05-18'),
('2017-05-19'),
('2017-05-20'),
('2017-05-21'),
('2017-05-22'),
('2017-05-23'),
('2017-05-24'),
('2017-05-25'),
('2017-05-26'),
('2017-05-27'),
('2017-05-28'),
('2017-05-29'),
('2017-05-30'),
('2017-05-31'),
('2017-06-01'),
('2017-06-02'),
('2017-06-03'),
('2017-06-04'),
('2017-06-05'),
('2017-06-06'),
('2017-06-07'),
('2017-06-08'),
('2017-06-09'),
('2017-06-10'),
('2017-06-11'),
('2017-06-12'),
('2017-06-13'),
('2017-06-14'),
('2017-06-15'),
('2017-06-16'),
('2017-06-17'),
('2017-06-18'),
('2017-06-19'),
('2017-06-20'),
('2017-06-21'),
('2017-06-22'),
('2017-06-23'),
('2017-06-24'),
('2017-06-25'),
('2017-06-26'),
('2017-06-27'),
('2017-06-28'),
('2017-06-29'),
('2017-06-30'),
('2017-07-01'),
('2017-07-02'),
('2017-07-03'),
('2017-07-04'),
('2017-07-05'),
('2017-07-06'),
('2017-07-07'),
('2017-07-08'),
('2017-07-09'),
('2017-07-10'),
('2017-07-11'),
('2017-07-12'),
('2017-07-13'),
('2017-07-14'),
('2017-07-15'),
('2017-07-16'),
('2017-07-17'),
('2017-07-18'),
('2017-07-19'),
('2017-07-20'),
('2017-07-21'),
('2017-07-22'),
('2017-07-23'),
('2017-07-24'),
('2017-07-25'),
('2017-07-26'),
('2017-07-27'),
('2017-07-28'),
('2017-07-29'),
('2017-07-30'),
('2017-07-31'),
('2017-08-01'),
('2017-08-02'),
('2017-08-03'),
('2017-08-04'),
('2017-08-05'),
('2017-08-06'),
('2017-08-07'),
('2017-08-08'),
('2017-08-09'),
('2017-08-10'),
('2017-08-11'),
('2017-08-12'),
('2017-08-13'),
('2017-08-14'),
('2017-08-15'),
('2017-08-16'),
('2017-08-17'),
('2017-08-18'),
('2017-08-19'),
('2017-08-20'),
('2017-08-21'),
('2017-08-22'),
('2017-08-23'),
('2017-08-24'),
('2017-08-25'),
('2017-08-26'),
('2017-08-27'),
('2017-08-28'),
('2017-08-29'),
('2017-08-30'),
('2017-08-31'),
('2017-09-01'),
('2017-09-02'),
('2017-09-03'),
('2017-09-04'),
('2017-09-05'),
('2017-09-06'),
('2017-09-07'),
('2017-09-08'),
('2017-09-09'),
('2017-09-10'),
('2017-09-11'),
('2017-09-12'),
('2017-09-13'),
('2017-09-14'),
('2017-09-15'),
('2017-09-16'),
('2017-09-17'),
('2017-09-18'),
('2017-09-19'),
('2017-09-20'),
('2017-09-21'),
('2017-09-22'),
('2017-09-23'),
('2017-09-24'),
('2017-09-25'),
('2017-09-26'),
('2017-09-27'),
('2017-09-28'),
('2017-09-29'),
('2017-09-30'),
('2017-10-01'),
('2017-10-02'),
('2017-10-03'),
('2017-10-04'),
('2017-10-05'),
('2017-10-06'),
('2017-10-07'),
('2017-10-08'),
('2017-10-09'),
('2017-10-10'),
('2017-10-11'),
('2017-10-12'),
('2017-10-13'),
('2017-10-14'),
('2017-10-15'),
('2017-10-16'),
('2017-10-17'),
('2017-10-18'),
('2017-10-19'),
('2017-10-20'),
('2017-10-21'),
('2017-10-22'),
('2017-10-23'),
('2017-10-24'),
('2017-10-25'),
('2017-10-26'),
('2017-10-27'),
('2017-10-28'),
('2017-10-29'),
('2017-10-30'),
('2017-10-31'),
('2017-11-01'),
('2017-11-02'),
('2017-11-03'),
('2017-11-04'),
('2017-11-05'),
('2017-11-06'),
('2017-11-07'),
('2017-11-08'),
('2017-11-09'),
('2017-11-10'),
('2017-11-11'),
('2017-11-12'),
('2017-11-13'),
('2017-11-14'),
('2017-11-15'),
('2017-11-16'),
('2017-11-17'),
('2017-11-18'),
('2017-11-19'),
('2017-11-20'),
('2017-11-21'),
('2017-11-22'),
('2017-11-23'),
('2017-11-24'),
('2017-11-25'),
('2017-11-26'),
('2017-11-27'),
('2017-11-28'),
('2017-11-29'),
('2017-11-30'),
('2017-12-01'),
('2017-12-02'),
('2017-12-03'),
('2017-12-04'),
('2017-12-05'),
('2017-12-06'),
('2017-12-07'),
('2017-12-08'),
('2017-12-09'),
('2017-12-10'),
('2017-12-11'),
('2017-12-12'),
('2017-12-13'),
('2017-12-14'),
('2017-12-15'),
('2017-12-16'),
('2017-12-17'),
('2017-12-18'),
('2017-12-19'),
('2017-12-20'),
('2017-12-21'),
('2017-12-22'),
('2017-12-23'),
('2017-12-24'),
('2017-12-25'),
('2017-12-26'),
('2017-12-27'),
('2017-12-28'),
('2017-12-29'),
('2017-12-30'),
('2017-12-31'),
('2018-01-01'),
('2018-01-02'),
('2018-01-03'),
('2018-01-04'),
('2018-01-05'),
('2018-01-06'),
('2018-01-07'),
('2018-01-08'),
('2018-01-09'),
('2018-01-10'),
('2018-01-11'),
('2018-01-12'),
('2018-01-13'),
('2018-01-14'),
('2018-01-15'),
('2018-01-16'),
('2018-01-17'),
('2018-01-18'),
('2018-01-19'),
('2018-01-20'),
('2018-01-21'),
('2018-01-22'),
('2018-01-23'),
('2018-01-24'),
('2018-01-25'),
('2018-01-26'),
('2018-01-27'),
('2018-01-28'),
('2018-01-29'),
('2018-01-30'),
('2018-01-31'),
('2018-02-01'),
('2018-02-02'),
('2018-02-03'),
('2018-02-04'),
('2018-02-05'),
('2018-02-06'),
('2018-02-07'),
('2018-02-08'),
('2018-02-09'),
('2018-02-10'),
('2018-02-11'),
('2018-02-12'),
('2018-02-13'),
('2018-02-14'),
('2018-02-15'),
('2018-02-16'),
('2018-02-17'),
('2018-02-18'),
('2018-02-19'),
('2018-02-20'),
('2018-02-21'),
('2018-02-22'),
('2018-02-23'),
('2018-02-24'),
('2018-02-25'),
('2018-02-26'),
('2018-02-27'),
('2018-02-28'),
('2018-03-01'),
('2018-03-02'),
('2018-03-03'),
('2018-03-04'),
('2018-03-05'),
('2018-03-06'),
('2018-03-07'),
('2018-03-08'),
('2018-03-09'),
('2018-03-10'),
('2018-03-11'),
('2018-03-12'),
('2018-03-13'),
('2018-03-14'),
('2018-03-15'),
('2018-03-16'),
('2018-03-17'),
('2018-03-18'),
('2018-03-19'),
('2018-03-20'),
('2018-03-21'),
('2018-03-22'),
('2018-03-23'),
('2018-03-24'),
('2018-03-25'),
('2018-03-26'),
('2018-03-27'),
('2018-03-28'),
('2018-03-29'),
('2018-03-30'),
('2018-03-31'),
('2018-04-01'),
('2018-04-02'),
('2018-04-03'),
('2018-04-04'),
('2018-04-05'),
('2018-04-06'),
('2018-04-07'),
('2018-04-08'),
('2018-04-09'),
('2018-04-10'),
('2018-04-11'),
('2018-04-12'),
('2018-04-13'),
('2018-04-14'),
('2018-04-15'),
('2018-04-16'),
('2018-04-17'),
('2018-04-18'),
('2018-04-19'),
('2018-04-20'),
('2018-04-21'),
('2018-04-22'),
('2018-04-23'),
('2018-04-24'),
('2018-04-25'),
('2018-04-26'),
('2018-04-27'),
('2018-04-28'),
('2018-04-29'),
('2018-04-30'),
('2018-05-01'),
('2018-05-02'),
('2018-05-03'),
('2018-05-04'),
('2018-05-05'),
('2018-05-06'),
('2018-05-07'),
('2018-05-08'),
('2018-05-09'),
('2018-05-10'),
('2018-05-11'),
('2018-05-12'),
('2018-05-13'),
('2018-05-14'),
('2018-05-15'),
('2018-05-16'),
('2018-05-17'),
('2018-05-18'),
('2018-05-19'),
('2018-05-20'),
('2018-05-21'),
('2018-05-22'),
('2018-05-23'),
('2018-05-24'),
('2018-05-25'),
('2018-05-26'),
('2018-05-27'),
('2018-05-28'),
('2018-05-29'),
('2018-05-30'),
('2018-05-31'),
('2018-06-01'),
('2018-06-02'),
('2018-06-03'),
('2018-06-04'),
('2018-06-05'),
('2018-06-06'),
('2018-06-07'),
('2018-06-08'),
('2018-06-09'),
('2018-06-10'),
('2018-06-11'),
('2018-06-12'),
('2018-06-13'),
('2018-06-14'),
('2018-06-15'),
('2018-06-16'),
('2018-06-17'),
('2018-06-18'),
('2018-06-19'),
('2018-06-20'),
('2018-06-21'),
('2018-06-22'),
('2018-06-23'),
('2018-06-24'),
('2018-06-25'),
('2018-06-26'),
('2018-06-27'),
('2018-06-28'),
('2018-06-29'),
('2018-06-30'),
('2018-07-01'),
('2018-07-02'),
('2018-07-03'),
('2018-07-04'),
('2018-07-05'),
('2018-07-06'),
('2018-07-07'),
('2018-07-08'),
('2018-07-09'),
('2018-07-10'),
('2018-07-11'),
('2018-07-12'),
('2018-07-13'),
('2018-07-14'),
('2018-07-15'),
('2018-07-16'),
('2018-07-17'),
('2018-07-18'),
('2018-07-19'),
('2018-07-20'),
('2018-07-21'),
('2018-07-22'),
('2018-07-23'),
('2018-07-24'),
('2018-07-25'),
('2018-07-26'),
('2018-07-27'),
('2018-07-28'),
('2018-07-29'),
('2018-07-30'),
('2018-07-31'),
('2018-08-01'),
('2018-08-02'),
('2018-08-03'),
('2018-08-04'),
('2018-08-05'),
('2018-08-06'),
('2018-08-07'),
('2018-08-08'),
('2018-08-09'),
('2018-08-10'),
('2018-08-11'),
('2018-08-12'),
('2018-08-13'),
('2018-08-14'),
('2018-08-15'),
('2018-08-16'),
('2018-08-17'),
('2018-08-18'),
('2018-08-19'),
('2018-08-20'),
('2018-08-21'),
('2018-08-22'),
('2018-08-23'),
('2018-08-24'),
('2018-08-25'),
('2018-08-26'),
('2018-08-27'),
('2018-08-28'),
('2018-08-29'),
('2018-08-30'),
('2018-08-31'),
('2018-09-01'),
('2018-09-02'),
('2018-09-03'),
('2018-09-04'),
('2018-09-05'),
('2018-09-06'),
('2018-09-07'),
('2018-09-08'),
('2018-09-09'),
('2018-09-10'),
('2018-09-11'),
('2018-09-12'),
('2018-09-13'),
('2018-09-14'),
('2018-09-15'),
('2018-09-16'),
('2018-09-17'),
('2018-09-18'),
('2018-09-19'),
('2018-09-20'),
('2018-09-21'),
('2018-09-22'),
('2018-09-23'),
('2018-09-24'),
('2018-09-25'),
('2018-09-26'),
('2018-09-27'),
('2018-09-28'),
('2018-09-29'),
('2018-09-30'),
('2018-10-01'),
('2018-10-02'),
('2018-10-03'),
('2018-10-04'),
('2018-10-05'),
('2018-10-06'),
('2018-10-07'),
('2018-10-08'),
('2018-10-09'),
('2018-10-10'),
('2018-10-11'),
('2018-10-12'),
('2018-10-13'),
('2018-10-14'),
('2018-10-15'),
('2018-10-16'),
('2018-10-17'),
('2018-10-18'),
('2018-10-19'),
('2018-10-20'),
('2018-10-21'),
('2018-10-22'),
('2018-10-23'),
('2018-10-24'),
('2018-10-25'),
('2018-10-26'),
('2018-10-27'),
('2018-10-28'),
('2018-10-29'),
('2018-10-30'),
('2018-10-31'),
('2018-11-01'),
('2018-11-02'),
('2018-11-03'),
('2018-11-04'),
('2018-11-05'),
('2018-11-06'),
('2018-11-07'),
('2018-11-08'),
('2018-11-09'),
('2018-11-10'),
('2018-11-11'),
('2018-11-12'),
('2018-11-13'),
('2018-11-14'),
('2018-11-15'),
('2018-11-16'),
('2018-11-17'),
('2018-11-18'),
('2018-11-19'),
('2018-11-20'),
('2018-11-21'),
('2018-11-22'),
('2018-11-23'),
('2018-11-24'),
('2018-11-25'),
('2018-11-26'),
('2018-11-27'),
('2018-11-28'),
('2018-11-29'),
('2018-11-30'),
('2018-12-01'),
('2018-12-02'),
('2018-12-03'),
('2018-12-04'),
('2018-12-05'),
('2018-12-06'),
('2018-12-07'),
('2018-12-08'),
('2018-12-09'),
('2018-12-10'),
('2018-12-11'),
('2018-12-12'),
('2018-12-13'),
('2018-12-14'),
('2018-12-15'),
('2018-12-16'),
('2018-12-17'),
('2018-12-18'),
('2018-12-19'),
('2018-12-20'),
('2018-12-21'),
('2018-12-22'),
('2018-12-23'),
('2018-12-24'),
('2018-12-25'),
('2018-12-26'),
('2018-12-27'),
('2018-12-28'),
('2018-12-29'),
('2018-12-30'),
('2018-12-31'),
('2019-01-01'),
('2019-01-02'),
('2019-01-03'),
('2019-01-04'),
('2019-01-05'),
('2019-01-06'),
('2019-01-07'),
('2019-01-08'),
('2019-01-09'),
('2019-01-10'),
('2019-01-11'),
('2019-01-12'),
('2019-01-13'),
('2019-01-14'),
('2019-01-15'),
('2019-01-16'),
('2019-01-17'),
('2019-01-18'),
('2019-01-19'),
('2019-01-20'),
('2019-01-21'),
('2019-01-22'),
('2019-01-23'),
('2019-01-24'),
('2019-01-25'),
('2019-01-26'),
('2019-01-27'),
('2019-01-28'),
('2019-01-29'),
('2019-01-30'),
('2019-01-31'),
('2019-02-01'),
('2019-02-02'),
('2019-02-03'),
('2019-02-04'),
('2019-02-05'),
('2019-02-06'),
('2019-02-07'),
('2019-02-08'),
('2019-02-09'),
('2019-02-10'),
('2019-02-11'),
('2019-02-12'),
('2019-02-13'),
('2019-02-14'),
('2019-02-15'),
('2019-02-16'),
('2019-02-17'),
('2019-02-18'),
('2019-02-19'),
('2019-02-20'),
('2019-02-21'),
('2019-02-22'),
('2019-02-23'),
('2019-02-24'),
('2019-02-25'),
('2019-02-26'),
('2019-02-27'),
('2019-02-28'),
('2019-03-01'),
('2019-03-02'),
('2019-03-03'),
('2019-03-04'),
('2019-03-05'),
('2019-03-06'),
('2019-03-07'),
('2019-03-08'),
('2019-03-09'),
('2019-03-10'),
('2019-03-11'),
('2019-03-12'),
('2019-03-13'),
('2019-03-14'),
('2019-03-15'),
('2019-03-16'),
('2019-03-17'),
('2019-03-18'),
('2019-03-19'),
('2019-03-20'),
('2019-03-21'),
('2019-03-22'),
('2019-03-23'),
('2019-03-24'),
('2019-03-25'),
('2019-03-26'),
('2019-03-27'),
('2019-03-28'),
('2019-03-29'),
('2019-03-30'),
('2019-03-31'),
('2019-04-01'),
('2019-04-02'),
('2019-04-03'),
('2019-04-04'),
('2019-04-05'),
('2019-04-06'),
('2019-04-07'),
('2019-04-08'),
('2019-04-09'),
('2019-04-10'),
('2019-04-11'),
('2019-04-12'),
('2019-04-13'),
('2019-04-14'),
('2019-04-15'),
('2019-04-16'),
('2019-04-17'),
('2019-04-18'),
('2019-04-19'),
('2019-04-20'),
('2019-04-21'),
('2019-04-22'),
('2019-04-23'),
('2019-04-24'),
('2019-04-25'),
('2019-04-26'),
('2019-04-27'),
('2019-04-28'),
('2019-04-29'),
('2019-04-30'),
('2019-05-01'),
('2019-05-02'),
('2019-05-03'),
('2019-05-04'),
('2019-05-05'),
('2019-05-06'),
('2019-05-07'),
('2019-05-08'),
('2019-05-09'),
('2019-05-10'),
('2019-05-11'),
('2019-05-12'),
('2019-05-13'),
('2019-05-14'),
('2019-05-15'),
('2019-05-16'),
('2019-05-17'),
('2019-05-18'),
('2019-05-19'),
('2019-05-20'),
('2019-05-21'),
('2019-05-22'),
('2019-05-23'),
('2019-05-24'),
('2019-05-25'),
('2019-05-26'),
('2019-05-27'),
('2019-05-28'),
('2019-05-29'),
('2019-05-30'),
('2019-05-31'),
('2019-06-01'),
('2019-06-02'),
('2019-06-03'),
('2019-06-04'),
('2019-06-05'),
('2019-06-06'),
('2019-06-07'),
('2019-06-08'),
('2019-06-09'),
('2019-06-10'),
('2019-06-11'),
('2019-06-12'),
('2019-06-13'),
('2019-06-14'),
('2019-06-15'),
('2019-06-16'),
('2019-06-17'),
('2019-06-18'),
('2019-06-19'),
('2019-06-20'),
('2019-06-21'),
('2019-06-22'),
('2019-06-23'),
('2019-06-24'),
('2019-06-25'),
('2019-06-26'),
('2019-06-27'),
('2019-06-28'),
('2019-06-29'),
('2019-06-30'),
('2019-07-01'),
('2019-07-02'),
('2019-07-03'),
('2019-07-04'),
('2019-07-05'),
('2019-07-06'),
('2019-07-07'),
('2019-07-08'),
('2019-07-09'),
('2019-07-10'),
('2019-07-11'),
('2019-07-12'),
('2019-07-13'),
('2019-07-14'),
('2019-07-15'),
('2019-07-16'),
('2019-07-17'),
('2019-07-18'),
('2019-07-19'),
('2019-07-20'),
('2019-07-21'),
('2019-07-22'),
('2019-07-23'),
('2019-07-24'),
('2019-07-25'),
('2019-07-26'),
('2019-07-27'),
('2019-07-28'),
('2019-07-29'),
('2019-07-30'),
('2019-07-31'),
('2019-08-01'),
('2019-08-02'),
('2019-08-03'),
('2019-08-04'),
('2019-08-05'),
('2019-08-06'),
('2019-08-07'),
('2019-08-08'),
('2019-08-09'),
('2019-08-10'),
('2019-08-11'),
('2019-08-12'),
('2019-08-13'),
('2019-08-14'),
('2019-08-15'),
('2019-08-16'),
('2019-08-17'),
('2019-08-18'),
('2019-08-19'),
('2019-08-20'),
('2019-08-21'),
('2019-08-22'),
('2019-08-23'),
('2019-08-24'),
('2019-08-25'),
('2019-08-26'),
('2019-08-27'),
('2019-08-28'),
('2019-08-29'),
('2019-08-30'),
('2019-08-31'),
('2019-09-01'),
('2019-09-02'),
('2019-09-03'),
('2019-09-04'),
('2019-09-05'),
('2019-09-06'),
('2019-09-07'),
('2019-09-08'),
('2019-09-09'),
('2019-09-10'),
('2019-09-11'),
('2019-09-12'),
('2019-09-13'),
('2019-09-14'),
('2019-09-15'),
('2019-09-16'),
('2019-09-17'),
('2019-09-18'),
('2019-09-19'),
('2019-09-20'),
('2019-09-21'),
('2019-09-22'),
('2019-09-23'),
('2019-09-24'),
('2019-09-25'),
('2019-09-26'),
('2019-09-27'),
('2019-09-28'),
('2019-09-29'),
('2019-09-30'),
('2019-10-01'),
('2019-10-02'),
('2019-10-03'),
('2019-10-04'),
('2019-10-05'),
('2019-10-06'),
('2019-10-07'),
('2019-10-08'),
('2019-10-09'),
('2019-10-10'),
('2019-10-11'),
('2019-10-12'),
('2019-10-13'),
('2019-10-14'),
('2019-10-15'),
('2019-10-16'),
('2019-10-17'),
('2019-10-18'),
('2019-10-19'),
('2019-10-20'),
('2019-10-21'),
('2019-10-22'),
('2019-10-23'),
('2019-10-24'),
('2019-10-25'),
('2019-10-26'),
('2019-10-27'),
('2019-10-28'),
('2019-10-29'),
('2019-10-30'),
('2019-10-31'),
('2019-11-01'),
('2019-11-02'),
('2019-11-03'),
('2019-11-04'),
('2019-11-05'),
('2019-11-06'),
('2019-11-07'),
('2019-11-08'),
('2019-11-09'),
('2019-11-10'),
('2019-11-11'),
('2019-11-12'),
('2019-11-13'),
('2019-11-14'),
('2019-11-15'),
('2019-11-16'),
('2019-11-17'),
('2019-11-18'),
('2019-11-19'),
('2019-11-20'),
('2019-11-21'),
('2019-11-22'),
('2019-11-23'),
('2019-11-24'),
('2019-11-25'),
('2019-11-26'),
('2019-11-27'),
('2019-11-28'),
('2019-11-29'),
('2019-11-30'),
('2019-12-01'),
('2019-12-02'),
('2019-12-03'),
('2019-12-04'),
('2019-12-05'),
('2019-12-06'),
('2019-12-07'),
('2019-12-08'),
('2019-12-09'),
('2019-12-10'),
('2019-12-11'),
('2019-12-12'),
('2019-12-13'),
('2019-12-14'),
('2019-12-15'),
('2019-12-16'),
('2019-12-17'),
('2019-12-18'),
('2019-12-19'),
('2019-12-20'),
('2019-12-21'),
('2019-12-22'),
('2019-12-23'),
('2019-12-24'),
('2019-12-25'),
('2019-12-26'),
('2019-12-27'),
('2019-12-28'),
('2019-12-29'),
('2019-12-30'),
('2019-12-31');

-- --------------------------------------------------------

--
-- Table structure for table `aa_canned_messages`
--

CREATE TABLE `aa_canned_messages` (
  `CANID` int(11) NOT NULL,
  `Can_Title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Can_Message` mediumtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `aa_custom_fields`
--

CREATE TABLE `aa_custom_fields` (
  `FID` int(11) NOT NULL,
  `Field_Name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Field_Type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Field_Required` int(11) NOT NULL,
  `Field_MaxLen` int(11) NOT NULL,
  `Field_Options` mediumtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `aa_forms`
--

CREATE TABLE `aa_forms` (
  `FormID` int(11) NOT NULL,
  `FormName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `FormDesc` text COLLATE utf8_unicode_ci NOT NULL,
  `FormGroup` int(11) DEFAULT NULL,
  `FormPriority` int(11) DEFAULT NULL,
  `FormSubject` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RemoveFormMessage` int(11) NOT NULL DEFAULT '0',
  `RemoveFormFile` int(11) NOT NULL DEFAULT '0',
  `Disabled` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `aa_forms_fields`
--

CREATE TABLE `aa_forms_fields` (
  `FormFieldsID` int(11) NOT NULL,
  `FormID` int(11) NOT NULL,
  `FID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `aa_forms_values`
--

CREATE TABLE `aa_forms_values` (
  `FVID` int(11) NOT NULL,
  `TicketIDAlias` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `FormID` int(11) NOT NULL,
  `FID` int(11) NOT NULL,
  `FieldValue` varchar(1000) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `aa_groups`
--

CREATE TABLE `aa_groups` (
  `Cat_ID` int(11) NOT NULL,
  `Parent_ID` int(11) DEFAULT NULL,
  `Category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Def` int(11) DEFAULT NULL,
  `Ticket_Assignment` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `aa_groups`
--

INSERT INTO `aa_groups` (`Cat_ID`, `Parent_ID`, `Category`, `Def`, `Ticket_Assignment`) VALUES
(1, NULL, 'Default', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `aa_imail`
--

CREATE TABLE `aa_imail` (
  `IMAILID` int(11) NOT NULL,
  `Email_Addr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Email_Host` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Port_No` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `Protocol` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Email_SSL` int(1) NOT NULL,
  `Val_Cert` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Email_TLS` int(1) NOT NULL,
  `Email_Folder` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Email_User` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Email_Pass` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Email_Group` int(11) NOT NULL,
  `Priority` int(11) NOT NULL,
  `Disabled` tinyint(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `aa_kb`
--

CREATE TABLE `aa_kb` (
  `KBID` tinyint(11) NOT NULL,
  `KB_Date_Added` datetime NOT NULL,
  `KB_Title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `KB_Author` int(11) NOT NULL,
  `KB_Group` int(11) NOT NULL,
  `KB_Article` text COLLATE utf8_unicode_ci NOT NULL,
  `KB_Sticky` int(1) NOT NULL,
  `KB_Position` int(3) NOT NULL,
  `KB_Hidden` int(1) NOT NULL,
  `KB_Meta_Tags` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `KB_Count` int(11) DEFAULT '0',
  `KB_Count_IP` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `KB_Like` int(11) NOT NULL DEFAULT '0',
  `KB_Dislike` int(11) NOT NULL DEFAULT '0',
  `KB_Rating_IP` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `aa_kb_comments`
--

CREATE TABLE `aa_kb_comments` (
  `KBCID` int(11) NOT NULL,
  `KBID` int(11) NOT NULL,
  `KBComDT` datetime NOT NULL,
  `KBComName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `KBComEmail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `KBComment` text COLLATE utf8_unicode_ci NOT NULL,
  `KBComApproved` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `aa_kb_groups`
--

CREATE TABLE `aa_kb_groups` (
  `KBGROUPID` int(11) NOT NULL,
  `KB_Group` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `aa_priorities`
--

CREATE TABLE `aa_priorities` (
  `Level_ID` int(11) NOT NULL,
  `Level` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `OrderID` int(11) NOT NULL,
  `Def` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `aa_priorities`
--

INSERT INTO `aa_priorities` (`Level_ID`, `Level`, `OrderID`, `Def`) VALUES
(1, 'Normal', 2, 1),
(2, 'Low', 1, 0),
(3, 'High', 3, 0),
(4, 'Urgent', 4, 0);

-- --------------------------------------------------------

--
-- Table structure for table `aa_settings`
--

CREATE TABLE `aa_settings` (
  `ID` int(11) NOT NULL,
  `Company_Name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Full_URL_Path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Langauge` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Timezone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Date_Format` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Redirect_Page` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Ticket_Dir` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `Ticket_Reply_Position` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `Ticket_Priority` int(1) NOT NULL,
  `Ticket_SLA` int(1) NOT NULL,
  `Ticket_Antispam` int(1) NOT NULL,
  `Ticket_Email` int(1) NOT NULL,
  `Ticket_Reopen` int(1) NOT NULL,
  `Ticket_Feedback` int(1) NOT NULL,
  `Ticket_Time` int(1) NOT NULL,
  `LiveChat_Enable` int(1) NOT NULL,
  `LiveChat_Idle_Time` int(11) NOT NULL,
  `KB_Enable` int(1) NOT NULL,
  `KB_Author_Allow` int(1) NOT NULL,
  `KB_Count` int(1) NOT NULL,
  `KB_Rating` int(1) NOT NULL,
  `KB_Comments` int(1) NOT NULL,
  `KB_Comments_Guests` int(1) NOT NULL,
  `KB_Comments_Approval` int(1) NOT NULL,
  `KB_Share` int(1) NOT NULL,
  `KB_Showx` int(1) NOT NULL,
  `File_Enabled` int(1) NOT NULL,
  `File_Limit` int(11) NOT NULL,
  `File_Path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `File_Size` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `File_Type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Enable_Guest_Access` int(1) NOT NULL,
  `Enable_User_Reg` int(1) NOT NULL,
  `Imail_Manual` int(1) NOT NULL,
  `FormWiz_DefaultFormDisable` int(1) NOT NULL,
  `FormWiz_DefaultFormName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Email_Enabled` int(1) NOT NULL,
  `Email_Method` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `Email_SMTP_Host` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Email_SMTP_Auth` int(1) NOT NULL,
  `Email_SMTP_User` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Email_SMTP_Pass` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Email_SMTP_Encr` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `Email_SMTP_Port` int(5) NOT NULL,
  `Email_Display` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Email_Addr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Email_Re_Addr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Email_New_Subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Email_New_Body` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `Email_Update_Subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Email_Update_Body` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `Email_Paused_Subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Email_Paused_Body` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `Email_Closed_Subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Email_Closed_Body` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `Email_Forward_Subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Email_Forward_Body` mediumtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `aa_settings`
--

INSERT INTO `aa_settings` (`ID`, `Company_Name`, `Full_URL_Path`, `Langauge`, `Timezone`, `Date_Format`, `Redirect_Page`, `Ticket_Dir`, `Ticket_Reply_Position`, `Ticket_Priority`, `Ticket_SLA`, `Ticket_Antispam`, `Ticket_Email`, `Ticket_Reopen`, `Ticket_Feedback`, `Ticket_Time`, `LiveChat_Enable`, `LiveChat_Idle_Time`, `KB_Enable`, `KB_Author_Allow`, `KB_Count`, `KB_Rating`, `KB_Comments`, `KB_Comments_Guests`, `KB_Comments_Approval`, `KB_Share`, `KB_Showx`, `File_Enabled`, `File_Limit`, `File_Path`, `File_Size`, `File_Type`, `Enable_Guest_Access`, `Enable_User_Reg`, `Imail_Manual`, `FormWiz_DefaultFormDisable`, `FormWiz_DefaultFormName`, `Email_Enabled`, `Email_Method`, `Email_SMTP_Host`, `Email_SMTP_Auth`, `Email_SMTP_User`, `Email_SMTP_Pass`, `Email_SMTP_Encr`, `Email_SMTP_Port`, `Email_Display`, `Email_Addr`, `Email_Re_Addr`, `Email_New_Subject`, `Email_New_Body`, `Email_Update_Subject`, `Email_Update_Body`, `Email_Paused_Subject`, `Email_Paused_Body`, `Email_Closed_Subject`, `Email_Closed_Body`, `Email_Forward_Subject`, `Email_Forward_Body`) VALUES
(0, 'Acorn Aid', 'http://localhost/acornaid/', 'english', 'Europe/London', '%D %b %y %H:%i:%s', 'p.php?p=tickets', 'ASC', 'BOT', 0, 1, 1, 1, 0, 1, 1, 1, 10, 0, 0, 1, 1, 1, 1, 1, 1, 5, 1, 5, '../uploads/', '1048576', 'txt, doc, docx, xls, xlsx, jpeg, jpg, gif, png', 1, 1, 0, 1, '21', 1, 'PHPMAIL', '', 1, '', '', '', 0, 'Company Name', 'support@yourdomain.com', 'support@yourdomain.com', 'New Service Request - [TICKET_NO] ([TICKET_SUBJECT])', '<p>Dear [TICKET_USER]<strong>,</strong></p>\r\n<p>Thank you for contacting us.</p>\r\n<p>This is confirmation of your request taken at [TICKET_DATE_UPDATED].</p>\r\n<p>We aim to reply to your request by [TICKET_SLAR] and fix by [TICKET_SLAF].</p>\r\n<p>The description of the request is:</p>\r\n<p>---</p>\r\n<p>[TICKET_ENQUIRY]</p>\r\n<p>---</p>\r\n<p>You can track and update your service request using your ticket number and email address at&nbsp;<span style="color: #444444; font-family: ''Segoe UI Semilight'', ''Segoe UI'', ''Segoe UI Web Regular'', ''Segoe UI Symbol'', ''Helvetica Neue'', ''BBAlpha Sans'', ''S60 Sans'', Arial, sans-serif; font-size: 13px;">[TICKET_URL]</span><span style="color: #444444; font-family: ''Segoe UI Semilight'', ''Segoe UI'', ''Segoe UI Web Regular'', ''Segoe UI Symbol'', ''Helvetica Neue'', ''BBAlpha Sans'', ''S60 Sans'', Arial, sans-serif; font-size: 13px;">&nbsp;</span></p>', 'Update regarding Service Request - [TICKET_NO]', '<p>Dear [TICKET_USER],</p>\r\n<p>Your call [TICKET_NO] has been updated.</p>\r\n<p>The update is shown below.</p>\r\n<p>---</p>\r\n<p>[TICKET_UPDATE]</p>\r\n<p>---</p>\r\n<p>Service Request Information:</p>\r\n<p>---</p>\r\n<p>Reference Number: [TICKET_NO]</p>\r\n<p>Reported by: [TICKET_USER]</p>\r\n<p>Reporting Date: [TICKET_DATE_ADDED]</p>\r\n<p>---</p>\r\n<p>You can track and update your service request using your ticket number and email address at&nbsp;<span style="color: #444444; font-family: ''Segoe UI Semilight'', ''Segoe UI'', ''Segoe UI Web Regular'', ''Segoe UI Symbol'', ''Helvetica Neue'', ''BBAlpha Sans'', ''S60 Sans'', Arial, sans-serif; font-size: 13px;">[TICKET_URL]</span><span style="color: #444444; font-family: ''Segoe UI Semilight'', ''Segoe UI'', ''Segoe UI Web Regular'', ''Segoe UI Symbol'', ''Helvetica Neue'', ''BBAlpha Sans'', ''S60 Sans'', Arial, sans-serif; font-size: 13px;">&nbsp;</span></p>', 'Paused regarding Service Request - [TICKET_NO]', '<p>Dear [TICKET_USER],</p>\r\n<p>Your call&nbsp;[TICKET_NO]&nbsp;with the us has been been paused.</p>\r\n<p>The update is shown below.</p>\r\n<p>---</p>\r\n<p>[TICKET_UPDATE]</p>\r\n<p>---</p>\r\n<p>The original Service Request Description is.</p>\r\n<p>---</p>\r\n<p>[TICKET_ENQUIRY]</p>\r\n<p>---</p>\r\n<p>Service Request Information:</p>\r\n<p>---</p>\r\n<p>Reference Number: [TICKET_NO]</p>\r\n<p>Reported by: [TICKET_USER]</p>\r\n<p>Reporting Date: [TICKET_DATE_ADDED]</p>\r\n<p>---</p>\r\n<p>You can track and update your service request using your ticket number and email address at&nbsp;<span style="color: #444444; font-family: ''Segoe UI Semilight'', ''Segoe UI'', ''Segoe UI Web Regular'', ''Segoe UI Symbol'', ''Helvetica Neue'', ''BBAlpha Sans'', ''S60 Sans'', Arial, sans-serif; font-size: 13px;">[TICKET_URL]</span><span style="color: #444444; font-family: ''Segoe UI Semilight'', ''Segoe UI'', ''Segoe UI Web Regular'', ''Segoe UI Symbol'', ''Helvetica Neue'', ''BBAlpha Sans'', ''S60 Sans'', Arial, sans-serif; font-size: 13px;">&nbsp;</span></p>', 'Closed regarding Service Request - [TICKET_NO]', '<p>Dear [TICKET_USER],</p>\r\n<p>Your call [TICKET_NO] with the us has been closed.</p>\r\n<p>The final update is shown below.</p>\r\n<p>---</p>\r\n<p>[TICKET_UPDATE]</p>\r\n<p>---</p>\r\n<p>The original Service Request Description is.</p>\r\n<p>--- [TICKET_ENQUIRY] ---</p>\r\n<p>Service Request Information:</p>\r\n<p>---</p>\r\n<p>Reference Number: [TICKET_NO]</p>\r\n<p>Reported by: [TICKET_USER]</p>\r\n<p>Reporting Date: [TICKET_DATE_ADDED]</p>\r\n<p>---</p>\r\n<p>You can track and update your service request using your ticket number and email address at&nbsp;<span style="color: #444444; font-family: ''Segoe UI Semilight'', ''Segoe UI'', ''Segoe UI Web Regular'', ''Segoe UI Symbol'', ''Helvetica Neue'', ''BBAlpha Sans'', ''S60 Sans'', Arial, sans-serif; font-size: 13px;">[TICKET_URL]</span><span style="color: #444444; font-family: ''Segoe UI Semilight'', ''Segoe UI'', ''Segoe UI Web Regular'', ''Segoe UI Symbol'', ''Helvetica Neue'', ''BBAlpha Sans'', ''S60 Sans'', Arial, sans-serif; font-size: 13px;">&nbsp;</span></p>', 'FW: FYI - [TICKET_NO]', '<p>Please can you assist with request below?</p>\r\n<p>Thanks</p>\r\n<p>[TICKET_UPDATE]</p>');

-- --------------------------------------------------------

--
-- Table structure for table `aa_slas`
--

CREATE TABLE `aa_slas` (
  `SLA_ID` int(11) NOT NULL,
  `GID` int(11) NOT NULL,
  `PID` int(11) NOT NULL,
  `SLA_Reply_Days` int(11) NOT NULL,
  `SLA_Reply_Hours` int(11) NOT NULL,
  `SLA_Fix_Days` int(11) NOT NULL,
  `SLA_Fix_Hours` int(11) NOT NULL,
  `Reply_Escalation_Group` int(11) NOT NULL,
  `Fix_Escalation_Group` int(11) NOT NULL,
  `Escalation_Email` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `aa_ticket`
--

CREATE TABLE `aa_ticket` (
  `ID` int(7) UNSIGNED ZEROFILL NOT NULL,
  `TicketIDAlias` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `User` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `User_Email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Cat_ID` int(11) NOT NULL,
  `t_GID_Origin` int(1) NOT NULL,
  `Level_ID` int(11) NOT NULL,
  `Type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `Subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Message` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `Files` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `IP_Address` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Page_Referer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Browser` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Status` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `Owner` int(11) DEFAULT NULL,
  `Collision` int(11) DEFAULT NULL,
  `Collision_Time` datetime DEFAULT NULL,
  `Feedback` int(11) DEFAULT NULL,
  `SLA_Reply` datetime DEFAULT NULL,
  `SLA_Reply_Alert` int(1) DEFAULT NULL,
  `SLA_Reply_Escalated` int(1) DEFAULT NULL,
  `SLA_Fix` datetime DEFAULT NULL,
  `SLA_Fix_Alert` int(1) DEFAULT NULL,
  `SLA_Fix_Escalated` int(1) DEFAULT NULL,
  `Date_Replied` datetime DEFAULT NULL,
  `Date_Added` datetime NOT NULL,
  `Date_Updated` datetime NOT NULL,
  `Date_Closed` datetime DEFAULT NULL,
  `FormID` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `aa_ticket`
--

INSERT INTO `aa_ticket` (`ID`, `TicketIDAlias`, `User`, `User_Email`, `Cat_ID`, `t_GID_Origin`, `Level_ID`, `Type`, `Subject`, `Message`, `Files`, `IP_Address`, `Page_Referer`, `Browser`, `Status`, `Owner`, `Collision`, `Collision_Time`, `Feedback`, `SLA_Reply`, `SLA_Reply_Alert`, `SLA_Reply_Escalated`, `SLA_Fix`, `SLA_Fix_Alert`, `SLA_Fix_Escalated`, `Date_Replied`, `Date_Added`, `Date_Updated`, `Date_Closed`, `FormID`) VALUES
(0000001, '59E1CDE406991AA1', 'Acorn Aid', 'general@acornaid.com', 1, 1, 1, 'Web', 'Welcome to Acorn Aid', '&lt;p&gt;Congratulations on completing the installation of Acorn Aid, The free, simple, modern day help desk solution.&lt;/p&gt;\r\n&lt;p&gt;Begin using Acorn Aid by going to Settings and configure each option to your desired requirements.&lt;/p&gt;\r\n&lt;p&gt;Support can be contacted from &lt;a title="Acorn Aid" href="http://www.acornaid.com" target="_blank"&gt;Acorn Aid&lt;/a&gt;&lt;/p&gt;\r\n&lt;p&gt;Regards&lt;/p&gt;\r\n&lt;p&gt;Acorn Aid&lt;/p&gt;', '', '::1', 'http://localhost:1023/acornaid/2/admin/p.php?p=ticket-add', 'Mozilla Firefox 47.0 [windows]', 'Open', NULL, 0, '2017-10-14 14:30:44', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-14 14:03:39', '2017-10-14 14:03:39', NULL, NULL),
(0000002, '59E1CDF72D104AA2', 'Acorn Aid', 'general@acornaid.com', 1, 1, 1, 'Web', 'Please Rate Acorn Aid', '&lt;p&gt;Please rate or leave a review for Acorn Aid&lt;/p&gt;\r\n&lt;p&gt;&lt;a href="http://www.hotscripts.com/listing/easy-ticket-143667/" target="_blank"&gt;http://www.hotscripts.com/listing/easy-ticket-143667/&lt;/a&gt;&lt;/p&gt;\r\n&lt;p&gt;Regards&lt;/p&gt;\r\n&lt;p&gt;Acorn Aid&lt;/p&gt;', '', '::1', 'http://localhost:1023/acornaid/2/admin/p.php?p=ticket-add', 'Mozilla Firefox 47.0 [windows]', 'Open', NULL, 0, '2016-10-02 20:51:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-14 14:19:27', '2017-10-14 14:19:27', NULL, NULL),
(0000003, '59E1CE0E89659AA3', 'Acorn Aid', 'general@acornaid.com', 1, 1, 1, 'Web', 'Follow Acorn Aid on Facebook and Twitter', '&lt;p&gt;Please follow Acorn Aid for news, updates and all other social media goodness.&lt;/p&gt;\r\n&lt;p&gt;&lt;a title="Acorn Aid Facebook" href="https://www.facebook.com/acornaid" target="_blank"&gt;Facebook&lt;/a&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;a title="Acorn Aid Twitter" href="https://twitter.com/acornaid" target="_blank"&gt;Twitter&lt;/a&gt;&lt;/p&gt;\r\n&lt;p&gt;Regards&lt;/p&gt;\r\n&lt;p&gt;Acorn Aid&lt;/p&gt;', '', '::1', 'http://localhost:1023/acornaid/2/admin/p.php?p=ticket-add', 'Mozilla Firefox 47.0 [windows]', 'Open', NULL, 0, '2017-10-14 14:31:03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-14 14:26:38', '2017-10-14 14:26:38', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `aa_ticket_updates`
--

CREATE TABLE `aa_ticket_updates` (
  `ID` int(11) NOT NULL,
  `TicketIDAlias` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `Ticket_ID` int(7) UNSIGNED ZEROFILL NOT NULL,
  `Update_By` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Updated_At` datetime NOT NULL,
  `Update_Type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `Update_Time` int(11) NOT NULL,
  `Forward_To` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Notes` text COLLATE utf8_unicode_ci NOT NULL,
  `Update_Files` text COLLATE utf8_unicode_ci NOT NULL,
  `Update_Emailed` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `aa_users`
--

CREATE TABLE `aa_users` (
  `UID` int(11) NOT NULL,
  `Pwd` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `Fname` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `TeleNo` varchar(22) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Signature` text COLLATE utf8_unicode_ci NOT NULL,
  `Role` int(1) NOT NULL,
  `Preferred_View` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `Table_Layout` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Layout_Style` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Notify_TN` int(11) NOT NULL,
  `Notify_TU` int(11) NOT NULL,
  `Notify_PM` int(11) NOT NULL,
  `Session_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Date_Created` datetime NOT NULL,
  `Last_Logon` datetime DEFAULT NULL,
  `Chat_Status` int(1) DEFAULT NULL,
  `Chat_Time` datetime DEFAULT NULL,
  `Disabled` int(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `aa_users`
--

INSERT INTO `aa_users` (`UID`, `Pwd`, `Fname`, `Email`, `TeleNo`, `Signature`, `Role`, `Preferred_View`, `Table_Layout`, `Layout_Style`, `Notify_TN`, `Notify_TU`, `Notify_PM`, `Session_ID`, `Date_Created`, `Last_Logon`, `Chat_Status`, `Chat_Time`, `Disabled`) VALUES
(1, '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8', 'Joe Bloggs', 'admin@yourdomain.com', '01234', '', 3, 'List', 'ti,tsub,tg,tp,tdu', 'white', 1, 1, 1, '778srkj72nic2i8891ctvtd4v1', '2016-04-18 00:00:00', '2017-10-08 19:08:32', 0, '2017-10-08 19:13:32', 0);

-- --------------------------------------------------------

--
-- Table structure for table `aa_users_skill`
--

CREATE TABLE `aa_users_skill` (
  `UCID` int(11) NOT NULL,
  `UID` int(11) NOT NULL,
  `CID` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `aa_users_skill`
--

INSERT INTO `aa_users_skill` (`UCID`, `UID`, `CID`) VALUES
(1, 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aa_canned_messages`
--
ALTER TABLE `aa_canned_messages`
  ADD PRIMARY KEY (`CANID`);

--
-- Indexes for table `aa_custom_fields`
--
ALTER TABLE `aa_custom_fields`
  ADD PRIMARY KEY (`FID`),
  ADD KEY `FID` (`FID`);

--
-- Indexes for table `aa_forms`
--
ALTER TABLE `aa_forms`
  ADD PRIMARY KEY (`FormID`),
  ADD KEY `FormID` (`FormID`);

--
-- Indexes for table `aa_forms_fields`
--
ALTER TABLE `aa_forms_fields`
  ADD PRIMARY KEY (`FormFieldsID`),
  ADD KEY `FormFieldsID` (`FormFieldsID`,`FormID`,`FID`);

--
-- Indexes for table `aa_forms_values`
--
ALTER TABLE `aa_forms_values`
  ADD PRIMARY KEY (`FVID`),
  ADD KEY `FVID` (`FVID`,`TicketIDAlias`,`FormID`,`FID`);

--
-- Indexes for table `aa_groups`
--
ALTER TABLE `aa_groups`
  ADD PRIMARY KEY (`Cat_ID`),
  ADD KEY `Cat_ID` (`Cat_ID`);

--
-- Indexes for table `aa_imail`
--
ALTER TABLE `aa_imail`
  ADD PRIMARY KEY (`IMAILID`),
  ADD KEY `IMAILID` (`IMAILID`);

--
-- Indexes for table `aa_kb`
--
ALTER TABLE `aa_kb`
  ADD PRIMARY KEY (`KBID`),
  ADD KEY `KBID` (`KBID`);

--
-- Indexes for table `aa_kb_comments`
--
ALTER TABLE `aa_kb_comments`
  ADD PRIMARY KEY (`KBCID`),
  ADD KEY `KBCID` (`KBCID`,`KBID`);

--
-- Indexes for table `aa_kb_groups`
--
ALTER TABLE `aa_kb_groups`
  ADD PRIMARY KEY (`KBGROUPID`),
  ADD KEY `KBGROUPID` (`KBGROUPID`);

--
-- Indexes for table `aa_priorities`
--
ALTER TABLE `aa_priorities`
  ADD PRIMARY KEY (`Level_ID`),
  ADD KEY `Level_ID` (`Level_ID`);

--
-- Indexes for table `aa_settings`
--
ALTER TABLE `aa_settings`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `aa_slas`
--
ALTER TABLE `aa_slas`
  ADD PRIMARY KEY (`SLA_ID`);

--
-- Indexes for table `aa_ticket`
--
ALTER TABLE `aa_ticket`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID` (`ID`,`TicketIDAlias`,`Cat_ID`,`Level_ID`,`Owner`,`FormID`);

--
-- Indexes for table `aa_ticket_updates`
--
ALTER TABLE `aa_ticket_updates`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK_TID` (`Ticket_ID`),
  ADD KEY `ID` (`ID`,`TicketIDAlias`);

--
-- Indexes for table `aa_users`
--
ALTER TABLE `aa_users`
  ADD PRIMARY KEY (`UID`),
  ADD KEY `UID` (`UID`),
  ADD KEY `Email` (`Email`);

--
-- Indexes for table `aa_users_skill`
--
ALTER TABLE `aa_users_skill`
  ADD PRIMARY KEY (`UCID`),
  ADD UNIQUE KEY `UID` (`UID`,`CID`),
  ADD KEY `UCID` (`UCID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aa_canned_messages`
--
ALTER TABLE `aa_canned_messages`
  MODIFY `CANID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `aa_custom_fields`
--
ALTER TABLE `aa_custom_fields`
  MODIFY `FID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `aa_forms`
--
ALTER TABLE `aa_forms`
  MODIFY `FormID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `aa_forms_fields`
--
ALTER TABLE `aa_forms_fields`
  MODIFY `FormFieldsID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `aa_forms_values`
--
ALTER TABLE `aa_forms_values`
  MODIFY `FVID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `aa_groups`
--
ALTER TABLE `aa_groups`
  MODIFY `Cat_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `aa_imail`
--
ALTER TABLE `aa_imail`
  MODIFY `IMAILID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `aa_kb`
--
ALTER TABLE `aa_kb`
  MODIFY `KBID` tinyint(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `aa_kb_comments`
--
ALTER TABLE `aa_kb_comments`
  MODIFY `KBCID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `aa_kb_groups`
--
ALTER TABLE `aa_kb_groups`
  MODIFY `KBGROUPID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `aa_priorities`
--
ALTER TABLE `aa_priorities`
  MODIFY `Level_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `aa_slas`
--
ALTER TABLE `aa_slas`
  MODIFY `SLA_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `aa_ticket`
--
ALTER TABLE `aa_ticket`
  MODIFY `ID` int(7) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `aa_ticket_updates`
--
ALTER TABLE `aa_ticket_updates`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `aa_users`
--
ALTER TABLE `aa_users`
  MODIFY `UID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `aa_users_skill`
--
ALTER TABLE `aa_users_skill`
  MODIFY `UCID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
