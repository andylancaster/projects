
-- Acorn Aid Upgrade DB Changes from 1.2 - 2.0.x
-- DB Struture Changes

--
-- Groups
--

ALTER TABLE `groups` ADD `Ticket_Assignment` INT(11) NOT NULL AFTER `Def`;


--
-- KB table
--
ALTER TABLE `kb` ADD `KB_Sticky` INT(1) NOT NULL AFTER `KB_Article`, ADD `KB_Position` INT(3) NOT NULL AFTER `KB_Sticky`, ADD `KB_Hidden` INT(1) NOT NULL AFTER `KB_Position`, ADD `KB_Meta_Tags` VARCHAR(255) NOT NULL AFTER `KB_Hidden`;


--
-- Settings table
--
ALTER TABLE `settings` ADD `Ticket_SLA` INT(1) NOT NULL AFTER `Ticket_Priority`;
ALTER TABLE `settings` ADD `KB_Share` INT(1) NOT NULL AFTER `KB_Rating`;

ALTER TABLE `settings` ADD `Enable_Guest_Access` INT(1) NOT NULL AFTER `File_Type`, ADD `Enable_User_Reg` INT(1) NOT NULL AFTER `Enable_Guest_Access`;

-- Set guest access and forwarding values
UPDATE `settings` SET `Enable_Guest_Access` = '1' WHERE `settings`.`ID` = 0;

-- Set redirect page to new page
UPDATE `settings` SET `Redirect_Page` = 'p.php?p=tickets' WHERE `settings`.`ID` = 0;

-- Alter settings fields to new types
ALTER TABLE `settings` CHANGE `File_Enabled` `File_Enabled` INT(1) NOT NULL, CHANGE `File_Type` `File_Type` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;

-- Delete Ticket_Assignment. Moved to groups table

ALTER TABLE `settings` DROP COLUMN `Ticket_Assignment`;


--
-- Ticket table
--
ALTER TABLE `ticket` ADD `t_GID_Origin` INT(1) NOT NULL AFTER `Cat_ID`;
ALTER TABLE `ticket` ADD `SLA_Reply` DATETIME NULL AFTER `Feedback`, ADD `SLA_Reply_Alert` INT(1) NULL AFTER `SLA_Reply`, ADD `SLA_Reply_Escalated` INT(1) NULL AFTER `SLA_Reply_Alert`, ADD `SLA_Fix` DATETIME NULL AFTER `SLA_Reply_Escalated`, ADD `SLA_Fix_Alert` INT(1) NULL AFTER `SLA_Fix`, ADD `SLA_Fix_Escalated` INT(1) NULL AFTER `SLA_Fix_Alert`;


--
-- Ticket_updates table
--
ALTER TABLE `ticket_updates` CHANGE `Update_Time` `Update_Time` INT(11) NOT NULL;


--
-- Users table
--
ALTER TABLE `users` ADD `TeleNo` VARCHAR(22) NULL AFTER `Email`;

ALTER TABLE `users` ADD `Preferred_View` VARCHAR(20) NOT NULL AFTER `Role`, ADD `Layout_Style` VARCHAR(255) NOT NULL AFTER `Preferred_View`;

ALTER TABLE `users` ADD `Date_Created` DATETIME NOT NULL AFTER `Session_ID`;

ALTER TABLE `users` ADD `Disabled` INT(1) NOT NULL AFTER `Chat_Time`;

-- Move data
UPDATE users SET User_ID = Email;
UPDATE users SET Fname = concat(Fname,' ',Lname);
UPDATE `users` SET Role = 0 WHERE Role = 'User';
UPDATE `users` SET Role = 1 WHERE Role = 'Agent'; 
UPDATE `users` SET Role = 2 WHERE Role = 'Supervisor'; 
UPDATE `users` SET Role = 3 WHERE Role = 'Admin' ;
UPDATE `users` SET `Preferred_View` = 'Expanded', `Layout_Style` = 'white';

ALTER TABLE `users` CHANGE `Role` `Role` INT(1) NOT NULL;

-- Delete discontinued columnns
ALTER TABLE `users`
  DROP COLUMN `User_ID`,
  DROP COLUMN `Lname`;
  
  
 
--
-- SLAs table
--
CREATE TABLE `slas` (
  `SLA_ID` int(11) NOT NULL,
  `GID` int(11) NOT NULL,
  `PID` int(11) NOT NULL,
  `SLA_Reply_Days` int(11) NOT NULL,
  `SLA_Reply_Hours` int(11) NOT NULL,
  `SLA_Fix_Days` int(11) NOT NULL,
  `SLA_Fix_Hours` int(11) NOT NULL,
  `Reply_Escalation_Group` int(11) NOT NULL,
  `Fix_Escalation_Group` int(11) NOT NULL,
  `Escalation_Email` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `slas`
	  ADD PRIMARY KEY (`SLA_ID`);

ALTER TABLE `slas`
	  MODIFY `SLA_ID` int(11) NOT NULL AUTO_INCREMENT;

 