<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Acorn Aid 1.2 - 2.0.x Upgrade</title>
<style>
html,body {
	margin: 10px;
    -webkit-font-smoothing: antialiased;
    text-shadow: 1px 1px 1px rgba(0,0,0,0.004);
	font-family:"Segoe UI Semilight", "Segoe UI","Segoe UI Web Regular","Segoe UI Symbol","Helvetica Neue","BBAlpha Sans","S60 Sans",Arial,sans-serif;
	font-size:13px;
	line-height: 1.8em;
}

a {
	color:#0066CC;
	text-decoration:none;
}

a:hover {
	text-decoration:underline;
}

.success {
	color:#009900;
}

.warning {
	color:#FF9900;
}

.error {
	color:#CC3300;
}


.large {
	font-size:large;
}
	
#title {
	margin:0 auto;
	font-size: x-large;
	font-weight: bold;
	width: 650px;
}

.desc {
	color:#090;
}
	
#outer {
	margin: 0 auto;
	background-color:#FFFFFF;
	width: 650px;
	border: 1px #DDD solid;
	-webkit-box-shadow: 2px 2px 2px 2px #EEE;
	box-shadow: 2px 2px 2px 2px #EEE;	
	-moz-border-radius:3px;
	-webkit-border-radius:3px;
	border-radius:3px;	
	padding:20px;
}

input[type='text'] {
	border: 1px solid #DDD;
	padding: 10px;
	width: 90%;
	border-radius:5px;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
}
input[type='submit'], input[type='button']  {
	-moz-box-shadow:inset 0px 1px 0px 0px #ffffff;
	-webkit-box-shadow:inset 0px 1px 0px 0px #ffffff;
	box-shadow:inset 0px 1px 0px 0px #ffffff;
	
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #ffffff), color-stop(1, #f6f6f6));
	background:-moz-linear-gradient(top, #ffffff 5%, #f6f6f6 100%);
	background:-webkit-linear-gradient(top, #ffffff 5%, #f6f6f6 100%);
	background:-o-linear-gradient(top, #ffffff 5%, #f6f6f6 100%);
	background:-ms-linear-gradient(top, #ffffff 5%, #f6f6f6 100%);
	background:linear-gradient(to bottom, #ffffff 5%, #f6f6f6 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffff', endColorstr='#f6f6f6',GradientType=0);
	
	background-color:#ffffff;
	
	-moz-border-radius:6px;
	-webkit-border-radius:6px;
	border-radius:6px;
	
	border:1px solid #dcdcdc;
	
	display:inline-block;
	font-weight:bold;
	color:#666666;
	padding:10px;
	text-decoration:none;
	
	text-shadow:0px 1px 0px #ffffff;
	cursor:pointer;
}	
input[type='submit']:hover, input[type='button']:hover  {
 background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #f6f6f6), color-stop(1, #ffffff));
        background:-moz-linear-gradient(top, #f6f6f6 5%, #ffffff 100%);
        background:-webkit-linear-gradient(top, #f6f6f6 5%, #ffffff 100%);
        background:-o-linear-gradient(top, #f6f6f6 5%, #ffffff 100%);
        background:-ms-linear-gradient(top, #f6f6f6 5%, #ffffff 100%);
        background:linear-gradient(to bottom, #f6f6f6 5%, #ffffff 100%);
        filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#f6f6f6', endColorstr='#ffffff',GradientType=0);
        
        background-color:#f6f6f6;
}

input[type='submit']:active, input[type='button']:active {
        position:relative;
        top:1px;
}
</style>
</head>
<body>
<?php

// db values
if (isset($_POST["Upgrade"])) {
	
	$i_host = $_POST["i_host"];
	$i_u = $_POST["i_username"];
	$i_p = $_POST["i_pw"];
	$i_db = $_POST["i_db"];

	upgrade_tables($i_host,$i_u,$i_p,$i_db);


}

// backup and upgrade acorn aid tables
function upgrade_tables($host,$user,$pass,$name) {
  
	$link = mysqli_connect($host,$user,$pass);
	mysqli_select_db($link, $name);

	//get all of the tables
	$tables = array();
	$result = mysqli_query($link, 'SHOW TABLES');
	while($row = mysqli_fetch_row($result)) {
		$tables[] = $row[0];
	}

	//cycle through
	foreach($tables as $table)
	{
	$result = mysqli_query($link,'SELECT * FROM '.$table);
	$num_fields = mysqli_num_fields($result);

	@$return.= 'DROP TABLE IF EXISTS '.$table.';';
	$row2 = mysqli_fetch_row(mysqli_query($link,'SHOW CREATE TABLE '.$table));
	$return.= "\n\n".$row2[1].";\n\n";

	for ($i = 0; $i < $num_fields; $i++) 
	{
	while($row = mysqli_fetch_row($result))
	{
	$return.= 'INSERT INTO '.$table.' VALUES(';
	for($j=0; $j<$num_fields; $j++) 
	{


	if (isset($row[$j]) && ($row[$j] != NULL)) { 
 $row[$j] = addslashes($row[$j]);
 $row[$j] = ereg_replace("\n","\\n",$row[$j]);
		$return.= '"'.$row[$j].'"' ; 
	} else { 
		if(is_null($row[$j])) { 
			$return.= 'NULL'; 			
		} else { 
			$return.='""'; 
		}
	} 

	if ($j<($num_fields-1)) { $return.= ','; }
	
	}
	$return.= ");\n";
	}
	}
	$return.="\n\n\n";
	}
	//echo str_replace("\n", "<br>", $return);
	//save file
	$handle = fopen('backups\acornaid-12-db-backup-'.time().'-'.(md5(implode(',',$tables))).'.sql','w+');
	if(fwrite($handle,@$return)) {
		echo 'Backup file successfully created in backups folder.';
		fclose($handle);
	} else {
		echo 'Backup failed';
	}
	
	// upgrade database fields

	$query = file_get_contents("aa-12-2-changes.sql"); 
	if (mysqli_multi_query($link, $query)) {
		
		echo "<p class='success'>Changes to database changes successful.</p>";

		$settingsfile = "../lib/db.php";
		$i_ticket = "ticket";
		$i_ticket_update = "ticket_updates";
		$i_categories = "groups";
		$i_priorities = "priorities";
		$i_settings = "settings";
		$i_users = "users";
		$i_users_skill = "users_skill";
		$i_calendar = "calendar";
		$i_canned_replies = "canned_messages";
		$i_custom_fields = "custom_fields";
		$i_kb = "kb";
		$i_kb_groups = "kb_groups";
		$i_slas = "slas";
		$i_imail = "imail";	
		// file content for settings.php
		$file_content = "<?php \n".
						"// acorn aid key\n".
						"\$aakey = \"FREE\";\n".
						"\n".
						"// database settings\n".
						"\$pdo['host'] = \"".$host."\";\n".
						"\$pdo['user'] =  \"".$user."\";\n".
						"\$pdo['pass'] = \"".$pass."\";\n".
						"\$pdo['db'] = \"".$name."\";\n".
						"\n".
						"// table settings\n".
						"\$pdo_t['t_ticket'] = \"".$i_ticket."\";\n".
						"\$pdo_t['t_ticket_updates'] = \"".$i_ticket_update."\";\n".
						"\$pdo_t['t_groups'] = \"".$i_categories."\";\n".
						"\$pdo_t['t_priorities'] = \"".$i_priorities."\";\n".
						"\$pdo_t['t_settings'] = \"".$i_settings."\";\n".
						"\$pdo_t['t_users'] = \"".$i_users."\";\n".
						"\$pdo_t['t_users_skills'] = \"".$i_users_skill."\";\n".
						"\$pdo_t['t_canned_msg'] = \"".$i_canned_replies."\";\n".
						"\$pdo_t['t_custom_fields'] = \"".$i_custom_fields."\";\n".
						"\$pdo_t['t_kb'] = \"".$i_kb."\";\n".
						"\$pdo_t['t_kb_groups'] = \"".$i_kb_groups."\";\n".
						"\$pdo_t['t_slas'] = \"".$i_slas."\";\n".
						"\$pdo_t['t_iemail'] = \"".$i_imail."\";\n".
						"?>";
		
		$filehandle = fopen($settingsfile, 'w+') or die ("Error writing settings file");
		fwrite($filehandle, $file_content);
		fclose($filehandle);
		
		echo "<p class='success'>Db.php file created/updated.</p>";
		
	} else {
		
		echo "<p class='error'>Failed to update database.</p>";
	}	
	
	mysqli_close($link);		
	
}

// Store cached values
function cache_values ($field) {

	if (isset($field)) {
	
		echo $field;
		
	}
	
}
?>

<div id="title">Acorn Aid 1.2 to 2.0.x Upgrade</div>
<br>
<div id="outer">

<form name="form1" method="post" action="">
    <p>Fill out the form below with your MySQL details to automatically complete the upgrade from Acorn Aid 1.2 to 2.0.x. </p>
    <p>A backup will be completed as part of the upgrade and stored in the backups folder. Please also consider manually backing up your MySQL database and file uploads folder.</p>
	<p>Following the upgrade <b>please note</b> your log in username will change from your username to your email address. Passwords will remain the same.</p>
	<br>
	
	<p>Existing Database Name</p>
    <input type="text" name="i_db" value="<?php cache_values(@$i_db); ?>" required>
    
    <p>MySQL Host</p>
    <input type="text" name="i_host" value="<?php cache_values(@$i_host); ?>" required>
    
    <p>MySQL Username</p>
    <input type="text" name="i_username" value="<?php cache_values(@$i_u); ?>" required>
    
    <p>MySQL Password</p>
    <input type="text" name="i_pw" value="<?php cache_values(@$i_p); ?>">
    
    <p><input name="Upgrade" type="submit" id="Upgrade" value="Upgrade"></p>
</form>

</div>

</body>
</html>