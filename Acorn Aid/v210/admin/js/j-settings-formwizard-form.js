$(document).ready(function() {

	$( function() {
      $( "#sortable1, #sortable2" ).sortable({
				//connectWith: ".connectedSortable",

        connectWith: ".connectedSortable",
				//containment: "#sortable1",
					receive: function( event, ui ) {
						// get field id
						var fid = ui.item.attr("fid");
						// get field data from db
						$.ajax({
							async: false,
							url: "ajax/a-formwizard-form.php",
							type: "post",
							data: { p_fid : fid },
							global: false,
							success: function(data){
								//alert(data);
								mydata = data;
							},
							error:function(){
								alert("Failed to get custom field data");
							}
						});

						//var attr = ui.item.attr('fid');
						var val = ui.item.html();
						// filter out fields for moving back
						var span = $(val).filter("#formwiz-field-text").html();

						var sender = ui.sender.attr("id");
						if (sender == "sortable2") {
							$(ui.item).html(mydata + " " + val);
						} else {
							$(ui.item).html(span);
						}
					}
      }).disableSelection();
    } );

	$("#formwiz-save").click(function(e) {
		var myForm = $('#myForm');
		// if not valid show html5 errors
		if(! myForm[0].checkValidity()) {
			myForm.find(':submit').click();
		} else {
			var formid = $("#set_formid").val();
			var formname = $("#set_formname").val();
			var formdesc = tinyMCE.get('set_formdesc').getContent();
			var group = $("#set_group").val();
			var priority = $("#set_priority").val();
			var subject = $("#set_sub").val();
			var message = $("#set_msg").is(':checked') ? 1 : 0;
			var file = $("#set_file").is(':checked') ? 1 : 0;

			var fieldids = [];
			$("#sortable1 .f").each(function(){
				var eachfid = $(this).attr("fid");
				fieldids.push(eachfid);
			});

			$.ajax({
				async: true,
				url: "ajax/a-formwizard-save.php",
				type: "post",
				data: { p_formid: formid, p_formname: formname, p_formdesc: formdesc, p_group : group, p_priority : priority, p_fids : fieldids, p_subject : subject, p_message : message, p_file : file },
				success: function(data){
					//alert(data);
					window.location.replace("p.php?p=settings-formwizard");
				},
				error:function(){
					alert("Failed to save form wizard");
				}
			});

		}
		return false;
	});

});
