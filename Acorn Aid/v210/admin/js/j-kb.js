$(document).ready(function() {

	$('.kbcoms_review').click(function() {
		$('.popup, .overlay').fadeToggle();

		var kbaid = $(this).attr("kbaid");
		//alert(kbaid);

		$.ajax({
			url: "ajax/a-kb-review-comments.php",
			type: "post",
			data: { p_kbaid : kbaid },
			cache: false,
			success: function(data){
				//alert(data);
				var kbreviewdata = data;
				$('#kbcomments_results').html(data);

				// Approve or reject kb comment
				$(".kbcomchg").click(function() {
					var kbcid =  $(this).attr("id");
					var kbchgtostat = $(this).attr("stat");

					// change status of div before ajax call
					if (kbchgtostat == "del") {
						$(this).closest('.kbcomment').fadeOut();
					} else {
						$(this).closest('.kbcomment').addClass("kbcomment-approved");
						$(this).replaceWith("<span><b>Approved</b></span>");
					}
					// change in sql
					$.ajax({
						url: "ajax/a-kb-comment-approval.php",
						type: "post",
						data: { p_kbcid : kbcid, p_kbchgtostat : kbchgtostat },
						cache: false,
						async: true,
						success: function(data){
							console.log();
						},
						error:function(){
							alert("Failed to get knowledge base comments");
						}
					});


				});
			},
			error:function(){
				alert("Failed to get knowledge base comments");
			}
		});

	});

});
