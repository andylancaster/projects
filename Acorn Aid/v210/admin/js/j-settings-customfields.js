$(document).ready(function() {
	// custom fields page functions

	function show_options () {

		var type = $("#custom_type").val();

		if (type == "Text" || type == "Textbox") {

			// disable / enable other fields to allow form submit
			$("#custom_options").hide();
			$("#custom_options_input").prop('disabled', true);
			$("#custom_maxlen").show();
			$("#custom_maxlen_input").prop('disabled', false);

		} else if (type == "Select" || type == "Radio" || type == "Checkbox") {

			$("#custom_maxlen").hide();
			$("#custom_maxlen_input").prop('disabled', true);
			$("#custom_maxlen_input").val("1");
			$("#custom_options").show();
			$("#custom_options_input").prop('disabled', false);

		}

	}

	show_options();

	// on changing field type
	$("#custom_type").change(function(event) {

		event.preventDefault();
		show_options();
	});


	$(".delete_field").click(function() {
	var dialogue_text = $('#Delete_Text').val();
	var status = confirm(dialogue_text);
		if(status == false){
			return false;
		} else {
			location.reload();
		}
	});

	$("#add_field").click(function() {
		$('.popup, .overlay').fadeToggle();
	});

	$(".edit_field").click(function() {
		$('.popup, .overlay').fadeToggle();
		var fid = $(this).attr("fid");
		var ecn = $(this).attr("ecn");
		var ect = $(this).attr("ect");
		var ecr = $(this).attr("ecr");
		var ecl = $(this).attr("ecl");
		var eco = $(this).attr("eco");

		$('#custom_id').val(fid);
		$('#custom_name').val(ecn);
		$('#custom_type').val(ect).change();
		if(ecr == 1) {
			$('#custom_required').prop('checked', true);
		}
		$('#custom_maxlen_input').val(ecl);
		$('#custom_options_input').val(eco);

		return false;
	});

	$('#c-submit').click(function(e) {
		var myForm = $('#customform');
		if(! myForm[0].checkValidity()) {
			myForm.find(':submit').click();
		} else {
			var cfid = $('#custom_id').val();
			var cn = $('#custom_name').val();
			var ct = $('#custom_type').val();
			var cr = $('#custom_required').is(':checked') ? '1' : '0';
			var cm = $('#custom_maxlen_input').val();
			var co = $('#custom_options_input').val();
			//alert (cn + ct + "..." + cr + "..." + cm + co);
			$.ajax({
				url: "ajax/a-settings-customfields.php",
				type: "POST",
				data: { p_fid : cfid, p_cn : cn, p_ct : ct, p_cr : cr, p_cm : cm, p_co : co },
				async: false,
				success: function(data){

					location.reload();
				},
				error:function(){
					alert("Failed to add custom field");
				}
			});
		}
		e.preventDefault;
		return false;
	});


// end of custom fields page
});
