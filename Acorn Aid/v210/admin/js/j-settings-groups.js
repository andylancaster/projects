$(document).ready(function() {
	// custom fields page function

	$("#add_group").click(function() {
		$('.popup, .overlay').fadeToggle();
	});

	$(".g_edit").click(function() {
		var gid = $(this).attr("gid");
		$.ajax({
			url: "ajax/a-settings-edit-groups.php",
			type: "POST",
			data: { p_gid : gid },
			dataType: "json",
			async: false,
			success: function(data){
				//alert(data.c)
				$('#gid').val(gid);
				$("#gname").val(data.b);
				if(data.c == null) {
					$("#gassign").val("NULL").trigger('change');
				} else {
					$("#gassign").val(data.c).trigger('change');
				}
				$("#gadmins").val(data.a).trigger('change');

				$('.popup, .overlay').fadeToggle();
			},
			error:function(){
				alert("Failed to add/edit group");
			}
		});
		return false;

	});

	$('#g-submit').click(function(e) {
		var myForm = $('#groupform');
		if(! myForm[0].checkValidity()) {
			myForm.find(':submit').click();
		} else {
			var gid = $('#gid').val();
			var gn = $('#gname').val();
			var ga = $('#gassign').val();
			var gadmins = $('#gadmins').val();

			$.ajax({
				url: "ajax/a-settings-groups.php",
				type: "POST",
				data: { p_gid : gid, p_gn : gn, p_ga : ga, p_gadmins : gadmins },
				async: false,
				success: function(data){
					//alert(data);
					location.reload();
				},
				error:function(){
					alert("Failed to add/edit group");
				}
			});
		}
		e.preventDefault;
		return false;

	});

// end of custom fields page
});
