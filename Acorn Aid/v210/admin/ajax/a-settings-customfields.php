<?php
require "../../lib/db.php";
require "../../lib/global.php";
require "../model/m-settings-customfields.php";
$pdo_conn = pdo_conn();

//print_r($_POST);

$custom_name_denied = array(" ", "\\", "/", ",", "\"", "&quot;", "'", ".", ";", ":", "?");
$cn = str_replace($custom_name_denied, "_", $_POST["p_cn"]);
$cfid = $_POST["p_fid"];
$ct = $_POST["p_ct"];
$cr = ($_POST["p_cr"] == 1) ? 1 : 0;
$cm = $_POST["p_cm"];
$co = $_POST["p_co"];
if ($cfid == "") {

	$sql = "INSERT INTO ".$pdo_t['t_custom_fields']." (Field_Name, Field_Type, Field_MaxLen, Field_Required, Field_Options)
			VALUES (:custom_name, :custom_type, :custom_maxlen, :custom_required, :custom_options)";


	$q = $pdo_conn->prepare($sql);
	if (!($q->execute(array("custom_name" => $cn,
						"custom_type" => $ct,
						"custom_maxlen" => $cm,
						"custom_required" => $cr,
						"custom_options" => $co)))) {

		print_r($q->errorInfo());

	} else {
		echo "INSERT OK";
	}
} else {

	$sql = "UPDATE ".$pdo_t['t_custom_fields']." SET Field_Name = :cn, Field_Type = :ct, Field_Required = :cr, Field_MaxLen = :cm, Field_Options = :co
	WHERE FID = :cfid";
	$q = $pdo_conn->prepare($sql);
	if (!($q->execute(array("cn" => $cn,
						"ct" => $ct,
						"cr" => $cr,
						"cm" => $cm,
						"co" => $co,
						"cfid" => $cfid)))) {

		print_r($q->errorInfo());

	} else {
		echo "UPDATE OK!";
	}
}

?>
