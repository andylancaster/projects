<?php
require "../../lib/db.php";
require "../../lib/global.php";
$pdo_conn = pdo_conn();

$gid = $_POST["p_gid"];

$sql = "SELECT Category,Ticket_Assignment, UID FROM ".$pdo_t['t_groups']." AS g LEFT JOIN ".$pdo_t['t_users_skills']." AS us ON g.Cat_ID = us.CID WHERE Cat_ID = :gid";

$groupadmins = $pdo_conn->prepare($sql);
$groupadmins->execute(array("gid" => $gid));
$array_of_admins = $groupadmins->fetchAll();
foreach ($array_of_admins as $a) {
	$admins[] = $a["UID"];
	$groupname = $a["Category"];
	$groupassign = $a["Ticket_Assignment"];
}

$arr = array('a' => $admins, 'b' => $groupname, 'c' => $groupassign);
/* Send as JSON */
 header("Content-Type: application/json", true);

/* Return JSON */
echo json_encode($arr);

/* Stop Execution */
exit;
?>
