<?php
session_start();
require_once "../../lib/db.php";
require_once "../../lib/global.php";
$pdo_conn = pdo_conn();
@include '../../public/language/'.$_SESSION['aalang'].'.php';

$kbaid = $_POST["p_kbaid"];
// set date format from settings
$date_format = get_settings('Date_Format');

$sel_kb_coms = "SELECT *,DATE_FORMAT(KBComDT, '$date_format') AS KB_Com_DT FROM ".$pdo_t['t_kb_comments']." WHERE KBID = :kbaid ORDER BY KBComDT DESC";
$q = $pdo_conn->prepare($sel_kb_coms);
$q->execute(array("kbaid" => $kbaid));

$array_of_kbcomments = $q->fetchAll();
foreach($array_of_kbcomments as $kbc) {

	$approve_status = ($kbc["KBComApproved"] == 0) ? '<a id="'.$kbc["KBCID"].'" href="#" class="kbcomchg" stat="appv">'.$lang['set-kb-db-coms-app'].'</a>' : '<b>'.$lang['set-kb-db-comsa-app-ok'].'</b>';
	$approve_bg = ($kbc["KBComApproved"] == 0) ? '' : 'kbcomment-approved';

	echo '<div class="kbcomment layout-padding '.$approve_bg.'">';
	echo '<b>'.$kbc["KBComName"].'</b> - '.$kbc["KB_Com_DT"].'<p>'.decode_entities($kbc["KBComment"]).'</p>';
	echo '<p>'.$approve_status.' | <a id="'.$kbc["KBCID"].'" href="#" class="kbcomchg" stat="del">'.$lang['set-kb-db-coms-del'].'</a></p>';
	echo '</div>';

}
?>
