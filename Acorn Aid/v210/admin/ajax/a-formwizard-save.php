<?php
session_start();
require_once "../../lib/db.php";
require_once "../../lib/global.php";
@include '../../public/language/'.$_SESSION['aalang'].'.php';
$pdo_conn = pdo_conn();

$formid = $_POST["p_formid"];
$formname = $_POST["p_formname"];
$formdesc = $_POST["p_formdesc"];
$group = ($_POST["p_group"] == "NULL") ? null : $_POST["p_group"];
$priority = ($_POST["p_priority"]  == "NULL") ? null : $_POST["p_priority"];
$subject = $_POST["p_subject"];
$msg = $_POST["p_message"];
$file = $_POST["p_file"];
$fid = $_POST["p_fids"];
echo $formid.$formname.$group.$priority;
print_r($fid);

// update or save form wizard
if ($formid != "") {
	echo "edit form";
	// update form
	$update_form = "UPDATE ".$pdo_t['t_formwiz_forms']." SET FormName=:fn, FormDesc=:fd, FormGroup=:fg, FormPriority=:fp, FormSubject=:fs, RemoveFormMessage=:fm, RemoveFormFile=:ff  WHERE FormID = :formid";
	$q = $pdo_conn->prepare($update_form);
	$q->execute(array('fn' => $formname, 'fd' => $formdesc, 'fg' => $group, 'fp' => $priority, 'fs' => $subject, 'fm' => $msg, 'ff' => $file, 'formid' => $formid));
	$lastId = $formid;

	// delete all form fields, recreate new order at the end
	$sqlff = "DELETE FROM ".$pdo_t['t_formwiz_form_fields']." WHERE FormID = :formid";
	$qff = $pdo_conn->prepare($sqlff);
	$qff->execute(array('formid' => $formid));

} else {
	echo "new form";
	// insert form name and settings
	$insert_request = "INSERT INTO ".$pdo_t['t_formwiz_forms']." (FormName, FormDesc, FormGroup, FormPriority, FormSubject, RemoveFormMessage, RemoveFormFile) VALUES (:fn, :fd, :fg, :fp, :fs, :fm, :ff)";
	$q = $pdo_conn->prepare($insert_request);
	if ($q->execute(array('fn' => $formname,
						'fd' => $formdesc,
						'fg' => $group,
						'fp' => $priority,
						'fs' => $subject,
						'fm' => $msg,
						'ff' => $file))) {

		$lastId = $pdo_conn->lastInsertId();
		echo "<p>".$lastId."</p>";
	}
}

// insert each custom field from form wizard
foreach ($fid as $f) {
	$add_form_fields = "INSERT INTO ".$pdo_t['t_formwiz_form_fields']." (FormID,FID) VALUES (:formid, :fid)";
	$q = $pdo_conn->prepare($add_form_fields);
	if (!($q->execute(array('formid' => $lastId, 'fid' => $f)))) {
		print_r($q->errorInfo());
	} else {
		echo "Fields Inserted OK";
	}
}
?>
