<?php
require "../../lib/db.php";
require "../../lib/global.php";
$pdo_conn = pdo_conn();

//print_r($_POST);
$gid = $_POST["p_gid"];
$gname = $_POST["p_gn"];
$gassign = ($_POST["p_ga"] == "NULL") ? NULL : $_POST["p_ga"];
$gadmins = $_POST["p_gadmins"];

print_r($gadmins);

if ($gid == "") {

	$sql = "INSERT INTO ".$pdo_t['t_groups']." (Category, Ticket_Assignment) VALUES (:gname, :gassign)";
	$q = $pdo_conn->prepare($sql);
	if (!($q->execute(array("gname" => $gname, "gassign" => $gassign)))) {
		print_r($q->errorInfo());
		exit();
	} else {
		echo "INSERT OK";
		$gid = $pdo_conn->lastInsertId();
	}

} else {

	$sql = "UPDATE ".$pdo_t['t_groups']." SET Category = :gn, Ticket_Assignment = :ga WHERE Cat_ID = :gid";
	$q = $pdo_conn->prepare($sql);
	if (!($q->execute(array("gn" => $gname,
						"ga" => $gassign,
						"gid" => $gid)))) {

		print_r($q->errorInfo());
		exit();
	} else {
		echo "UPDATE OK!";
	}
}

// if skills are selected
if (isset($gadmins)) {
	// reinsert all ticked records
	$sql_drop_group_admins = "DELETE FROM ".$pdo_t['t_users_skills']." WHERE CID = :cid";
	$q_drop_group_admins = $pdo_conn->prepare($sql_drop_group_admins);

	if(!$q_drop_group_admins->execute(array('cid' => $gid))) {
		print_r($q_drop_group_admins->errorInfo());
	} else {
		echo "DROPPED OLD GROUP ADMINS. ADD ADMINS AGAIN.";
		foreach ($gadmins as $a) {

			// reinsert all ticked records
			$sql_u_s_i = "INSERT INTO ".$pdo_t['t_users_skills']." (UID, CID) VALUES (:uid, :skill)";
			$q_u_s_i = $pdo_conn->prepare($sql_u_s_i);

			if(!$q_u_s_i->execute(array('uid' => $a, 'skill' => $gid))) {
				print_r($q_u_s_i->errorInfo());
				exit;
			} else {
				echo "Agents added to group.";
			}

		}
	}
}
?>
