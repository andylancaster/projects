<?php
// get groups
$groups = aaModelGetGroups();
$array_of_groups = $groups->fetchAll();
$no_of_groups = $groups->rowCount();

$agents = aaModelGetAgents();
$array_of_agents = $agents->fetchAll();

// Set default category for drop down boxes
if (isset($_POST["group-default"])) {

	aaModelSetDefaultGroup($_POST["cid"]);

}

if (isset($_POST['group-del'])) {

	aaModelDeleteGroup($_POST["cid"]);

}
?>
<div id="layout-body-centre" class="layout-padding form">
    <?php include 'v-settings-menu.php'; ?>

    <h2><?php echo $lang["set-groups-title"]; ?></h2>

	<div class="layout-body-dialog">

		<p><?php echo $lang["set-groups-desc"]; ?></p>
		<p><a href="#" id="add_group"><?php echo $lang['set-groups-add']; ?></a></p>

	</div>

    <h2><?php echo $lang["set-groups-exist"]; ?></h2>

	<div class="layout-body-dialog">

    <table>
    <thead>
    <tr>
    <td width="80%"><?php echo $lang['set-groups-table-g']; ?></td>
    <td width="20%"><?php echo $lang['set-groups-table-ta']; ?></td>
    <td></td>
    <td></td>
    <td></td>
    </tr>
    </thead>
    <tbody>
    <?php

	foreach($array_of_groups as $group) {
    $tpg = aaModelGetTicketsPerGroup($group["Cat_ID"]);
    $tpg = $tpg->fetch();
    ?>
    <tr>
    <form name="group" method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
    <input name="cid" id="cid" value="<?php echo $group["Cat_ID"]; ?>" hidden style="display:none; ">
    <?php
    // if only one group left don't show delete option

    if ($group["Def"] > 0) {
	    $green = "style=\"color:#090\"";
    } else {
	    $green = "";
    }

    if ($tpg['tcount'] > 0) {
	    $disabled = 'disabled';
	    $disabled_title = $lang['generic-delete-warn'];
    } else {
	    $disabled = '';
	    $disabled_title = $lang["generic-delete"];
    }

    echo "<td><span $green>".decode_entities($group['Category'])." <strong title=\"Tickets per group\">[".$tpg['tcount']."]</strong></span></td>";

    ?>
	<td><?php echo $group['Fname']; ?></td>
    <td><button type="submit" name="group_edit" id="group_edit" class="g_edit" gid="<?php echo $group["Cat_ID"]; ?>"><i class="fa fa-pencil-square-o"></i></button></td>
    <?php
    if ($no_of_groups > 1) {
    ?>
    <td><button type="submit" name="group-del" <?php echo $disabled; ?> title="<?php echo $disabled_title; ?>"><i class="fa fa-trash-o"></i></button></td>
    <?php
    }
    ?>
    <td><button type="submit" name="group-default"><i class="fa fa-check" title="<?php echo $lang["generic-mark"]; ?>"></i></button></td>
    </form>
    </tr>
    <?php

    }

    ?>
    </tbody>
    </table>

	<div class="overlay"></div>
	<div class="popup layout-padding">

		<h3><?php echo $lang["set-groups-add"]; ?></h3>
			<form id="groupform" class="popupform" action="#" method="post">
			<input hidden="hidden" type="text" id="gid" name="gid"/>

		    <div class="form-field">
			<label for="newgroup"><?php echo $lang['set-groups-table-g']; ?> *</label>
		    <input required pattern=".*\S+.*" type="text" name="gname" id="gname" value="" placeholder="<?php echo $lang['set-groups-enter']; ?>" />
		    </div>

			<div class="form-field">
			<label for="gassign"><?php echo $lang['set-groups-table-ta']; ?></label>
			<?php
			echo "<select class=\"NoSearchSelect2\" name=\"gassign\" id=\"gassign\">";
			echo "<option value=\"NULL\">".$lang["generic-unassigned"]."</option>";
			echo "<option value=\"Owner\" disabled>".$lang['tickets-owner']."</option>";
			foreach ($array_of_agents as $agent) {
				if($group['Ticket_Assignment'] == $agent["UID"]) {
					echo "<option value=".$agent["UID"]." selected=\"selected\"> - ".$agent["Fname"]."</option>";
				} else {
					echo "<option value=".$agent["UID"]."> - ".$agent["Fname"]."</option>";
				}
			}
			echo "</select>";
			?>
			</div>

			<div class="form-field">
			<label for="newgroup"><?php echo $lang['set-groups-agents']; ?> *</label>
			<select name="gadmins" id="gadmins" multiple>
				<?php
				foreach($array_of_agents as $a) {
					echo '<option value="'.$a["UID"].'" selected>'.$a["Fname"].'</option>';
				}
				?>
			</select>
			</div>

			<p><input class="btn" type="submit" name="g-submit" id="g-submit" value="<?php echo $lang["generic-save"]; ?>" /></p>

		</form>
	</div>

	</div>
</div>
