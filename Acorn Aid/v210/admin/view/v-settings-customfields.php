<div id="layout-body-centre" class="layout-padding form">
    	<?php include 'v-settings-menu.php'; ?>

		<?php
		// delete custom field
		if (isset($_POST["custom_delete"])) {
			aaModelDeleteCustomField ($_POST["FID"], $_POST["Field_Name"]);
		}
		?>

        <h2><?php echo $lang['set-custom-db-af']; ?></h2>
        <div class="layout-body-dialog">
        <?php echo $lang['set-custom-title-desc']; ?>
        <p><a href="#" id="add_field"><?php echo $lang["set-custom-add"]; ?></a></p>
        </div>
        <?php
		$customfields = aaModelGetCustomFields();
		$customfields_array = $customfields->fetchAll();
		$custom_total = $customfields->rowCount();

        if ($custom_total > 0) {
        ?>
        <h2><?php echo $lang['set-custom-db-ef']; ?></h2>

        <div class="layout-body-dialog">

        <table>
        <colgroup>
        <col />
        <col />
        <col />
        <col />
        <col />
        <col />
        </colgroup>
        <thead>
        <tr>
        <td><?php echo ucwords($lang['set-custom-db-fn']); ?></td>
        <td><?php echo ucwords($lang['set-custom-db-ft']); ?></td>
        <td><?php echo ucwords($lang['set-custom-db-fr']); ?></td>
        <td><?php echo ucwords($lang['set-custom-db-fl']); ?></td>
        <td><?php echo ucwords($lang['set-custom-db-fo']); ?></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        </tr>
        </thead>
        <tbody>
        <?php
		foreach ($customfields_array as $cf) {

        if ($cf["Field_Required"] == 1) {
       		$required = $lang["generic-yes"];
        } else {
        	$required = $lang["generic-no"];
        }

        $fieldname = str_replace("_", " ", $cf["Field_Name"]);
        ?>
        <tr>
        <form method="post">
        <input style="display:none; visibility:hidden" type="text" id="FID" name="FID" value="<?php echo $cf["FID"]; ?>" />
        <input style="display:none; visibility:hidden" type="text" id="ecn" name="ecn" value="<?php echo $cf["Field_Name"]; ?>" />
        <input style="display:none; visibility:hidden" type="text" id="ect" name="ect" value="<?php echo $cf["Field_Type"]; ?>" />
        <input style="display:none; visibility:hidden" type="text" id="ecr" name="ecr" value="<?php echo $cf["Field_Required"]; ?>" />
        <input style="display:none; visibility:hidden" type="text" id="ecl" name="ecl" value="<?php echo $cf["Field_MaxLen"]; ?>" />
        <input style="display:none; visibility:hidden" type="text" id="eco" name="eco" value="<?php echo $cf["Field_Options"]; ?>" />

        <input style="display:none; visibility:hidden" type="text" id="Field_Name" name="Field_Name" value="<?php echo $cf["Field_Name"]; ?>" />
        <input style="display:none; visibility:hidden" type="text" id="Delete_Text" name="Delete_Text" value="<?php echo $lang["set-custom-delete-con"]; ?>" />
        <td data-title="<?php echo ucwords($lang["set-custom-db-fn"]); ?>"><?php echo decode_entities($fieldname); ?></td>
        <td data-title="<?php echo ucwords($lang["set-custom-db-ft"]); ?>"><?php echo $cf["Field_Type"];?></td>
        <td data-title="<?php echo ucwords($lang["set-custom-db-fr"]); ?>"><?php echo $required; ?></td>
        <td data-title="<?php echo ucwords($lang["set-custom-db-fl"]); ?>"><?php echo $cf["Field_MaxLen"];?></td>
        <td data-title="<?php echo ucwords($lang["set-custom-db-fo"]); ?>"><?php echo decode_entities($cf["Field_Options"]); ?>&nbsp;</td>
        <td data-title="Edit"><button type="submit" fid="<?php echo $cf["FID"]; ?>" ecn="<?php echo $cf["Field_Name"]; ?>" ect="<?php echo $cf["Field_Type"]; ?>" ecr="<?php echo $cf["Field_Required"]; ?>" ecl="<?php echo $cf["Field_MaxLen"]; ?>" eco="<?php echo $cf["Field_Options"]; ?>" class="edit_field" id="custom_edit" name="custom_edit" title="<?php echo $lang["generic-edit"]; ?>"><i class="fa fa-pencil-square-o"></i></button></td>
        <td data-title="Delete"><button type="submit" class="delete_field" id="custom_delete" name="custom_delete" title="<?php echo $lang["generic-delete"]; ?>"><i class="fa fa-trash-o"></i></button></td>
        </form>
        </tr>
        <?php
        }
        ?>
        </tbody>
        </table>
    	<?php
		}
		?>

        <div class="overlay"></div>
        <div class="popup layout-padding">

            <h3><?php echo $lang["set-custom-db-af"]; ?></h3>
                <form id="customform" class="popupform" action="#" method="post">
                <input hidden="hidden" type="text" id="custom_id" name="custom_id"/>

                <div class="form-field">
                <label for="custom_name"><?php echo $lang['set-custom-db-fn']; ?> *</label>
                <input required pattern=".*\S+.*" type="text" id="custom_name" name="custom_name" placeholder="<?php echo $lang['set-custom-db-fn']; ?>" value="<?php if (isset($custom_name)) { echo $custom_name; } ?>" />
                </div>

                <div class="form-field">
                <label for="custom_type"><?php echo $lang["set-custom-db-ft"]; ?></label>
                <?php
                $field_types = array("Text","Textbox","Select","Checkbox","Radio");
                ?>
                <select class="NoSearchSelect2" name="custom_type" id="custom_type">
                <?php
                foreach ($field_types as $type) {
                    if ($custom_type == $type) {
                        echo "<option value=\"".$type."\" selected=\"selected\">".$type."</option>";
                    } else {
                        echo "<option value=\"".$type."\">".$type."</option>";
                    }
                }
                ?>
                </select>
                </div>

                <div class="form-field">
                <input id="custom_required" name="custom_required" type="checkbox" value="1" />
                <label class="form-field-inline-label" for="custom_required"><?php echo $lang['set-custom-db-fr']; ?></label>
                </div>

                <div id="custom_maxlen" class="form-field">
                <label for="custom_maxlen"><?php echo $lang['set-custom-db-fl']; ?></label>
                <input type="number" min="1" name="custom_maxlen" id="custom_maxlen_input" value="255" placeholder="Maximum length" value="<?php if (isset($custom_maxlen)) { echo $custom_maxlen; } ?>" />
                </div>

                <div id="custom_options" class="form-field">
                <label for="custom_options"><?php echo $lang['set-custom-db-fo']; ?> <?php echo $lang['set-custom-db-fo-desc']; ?></label>
                <input required pattern=".*\S+.*" type="text" id="custom_options_input" name="custom_options_input" placeholder="<?php echo $lang['set-custom-db-fo']; ?>" value="<?php if (isset($custom_options)) { echo $custom_options; } ?>" />
                </div>

                <p><input class="btn" type="submit" name="c-submit" id="c-submit" value="<?php echo $lang['generic-save']; ?>"></p>
                </form>

            </form>
        </div>

        </div>
    </div>
