<?php
$kbgroups = aaModelGetKBGroups();
$array_of_kbgroups = $kbgroups->fetchAll();
$no_of_kbgroups = $kbgroups->rowCount();
$db_kb_enable = get_settings("KB_Enable");

if($db_kb_enable == 0) {
	header("Location: p.php?p=kb");
}

if (isset($_POST['group-del'])) {

aaModelDeleteKBGroup($_POST["kbgid"]);

}
?>
<div id="layout-body-centre" class="layout-padding form">

    <h2><?php echo $lang['kb-db-ag-title']; ?></h2>

	<div class="layout-body-dialog">
    <?php echo $lang['kb-db-ag-title-desc']; ?>
    <?php
	// add new group
	if (isset($_POST["group_save"])) {

		aaModelSaveKBGroup($_POST["newgroup"]);

	}
	?>
    <form name="addgroup" method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
	<div class="form-field">
	<label><?php echo $lang["set-groups-add"]; ?></label>
    <input required pattern=".*\S+.*" type="text" name="newgroup" id="newgroup" value="" placeholder="Enter new group" autofocus="autofocus" />
    <p><input class="btn" type="submit" name="group_save" id="group_save" value="<?php echo $lang["generic-save"]; ?>" /></p>
	</div>
    </form>

	</div>

    <?php
    if ($no_of_kbgroups > 0) {
    ?>
    <h2><?php echo $lang["kb-db-eg"]; ?></h2>

	<div class="layout-body-dialog">
	    <table>
		<?php
		foreach($array_of_kbgroups as $kb_group) {
		$apg = aaModelGetArticlesByKBGroup($kb_group["KBGROUPID"]);
		$apg = $apg->fetch();
	    ?>
	    <form name="group" method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
	    <?php
	    // if only one group left don't show delete option

	    if ($apg['tcount'] > 0) {
	    $disabled = 'disabled';
	    $disabled_title = $lang['generic-delete-warn-article'];
	    } else {
	    $disabled = '';
	    $disabled_title = $lang['generic-delete-warn-article'];
	    }

	    ?>
	    <tr>
	    <td width="100%">
	    <?php

	    echo decode_entities($kb_group["KB_Group"])." <strong title=\"Articles per group\">[".$apg['tcount']."]</strong>";

	    ?>
	    </td>
	    <td>
	    <input name="kbgid" id="kbgid" value="<?php echo $kb_group["KBGROUPID"]; ?>" hidden style="display:none; visibility:hidden; ">
	    <button class="btn" type="submit" name="group-del" <?php echo $disabled; ?> title="<?php echo $disabled_title; ?>"><i class="fa fa-trash-o"></i></button>
	    </td>
	    </form>
	    <?php

	    }
		?>
	    </table>
	</div>
    <?php
    }
    ?>

</div>
