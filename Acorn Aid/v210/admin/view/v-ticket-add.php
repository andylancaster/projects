<?php
$users = aaModelGetUsers();
$array_of_users = $users->fetchAll();
$no_of_users = $users->rowCount();

// get groups
$groups = aaModelGetGroups();
$array_of_groups = $groups->fetchAll();
$no_of_groups = $groups->rowCount();

// get agents
$agents = aaModelGetAgents();
$array_of_agents = $agents->fetchAll();

// get priorities
$priorities = aaModelGetPriorities();
$array_of_priorities = $priorities->fetchAll();
$no_of_priorities = $priorities->rowCount();

// get live custom forms
$forms = aaGetForms("NULL");
$forms_array = $forms->fetchAll();
// is default form enabled
$formdefault = get_settings("FormWiz_DefaultFormDisable");
// default form name selected
$formdefaultname = get_settings("FormWiz_DefaultFormName");

// build navigation of forms
if ($formdefault == '0') {
	$submenu .= '<li><a href="p.php?p=ticket-add">'.$lang["set-formwiz-dform"].'</a></li>';
}
foreach ($forms_array as $ff) {

    $submenu .= '<li><a href="p.php?p=ticket-add&formid='.$ff["FormID"].'">'.$ff["FormName"].'</a></li>';

}
// get form details e.g. group and priority
$form = aaGetFormsDetails($_GET["formid"]);
$form_array = $form->fetch();
?>
<div id="layout-body-centre" class="layout-padding form">
	<div id="navigation-settings-menu">
	<ul id="settings-menu">
	    <?php echo $submenu; ?>
	</ul>
	</div>
        <h2><?php echo $lang["ticket-add-title"]; ?></h2>
        <div class="layout-body-dialog">
        <?php
    	if (isset($_POST["Add"])) {

        aaModelInsertRequest($_POST["ticket-add-fname"],
						$_POST["ticket-add-email"],
						$_POST["category"],
						$_POST["priority"],
						$_POST["subject"],
						$_POST["notes"],
						$_FILES["file"],
						@$_POST["custom"],
                        $_GET["formid"]);

		}
        ?>
		<?php echo read_session('aaerror-add-null'); ?>
		<?php echo read_session('aaerror-add-email'); ?>
		<?php echo read_session('aaerror-file'); ?>
        <form name="form1" method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>" enctype="multipart/form-data">
		<?php echo decode_entities($form_array["FormDesc"]); ?>
        <p><a href="#" id="s_popup"><?php echo $lang['ticket-add-search-u']; ?></a> <?php echo $lang['ticket-add-guest']; ?></p>

        <div class="form-field">
        <label for="ticket-add-fname"><?php echo $lang['ticket-add-name']; ?> *</label>
        <input required pattern=".*\S+.*" id="ticket-add-fname" name="ticket-add-fname" type="text" value="<?php cached_fields(@$_POST["ticket-add-fname"]); ?>" placeholder="<?php echo $lang['ticket-add-name']; ?>">
		</div>

        <div class="form-field">
        <label for="ticket-add-email"><?php echo $lang['ticket-add-email']; ?> *</label>
        <input required pattern=".*\S+.*" id="ticket-add-email" name="ticket-add-email" type="email" value="<?php cached_fields(@$_POST["ticket-add-email"]); ?>" placeholder="<?php echo $lang['ticket-add-email']; ?>">
        </div>

        <?php
        // if there's only one group then don't show select option
        if ($no_of_groups == 1 || isset($form_array["FormGroup"])) {
            $display_cat_opt = "style=\"display:none\"";
        }
        ?>
        <div class="form-field" <?php echo @$display_cat_opt; ?>>
        <label for="category"><?php echo $lang['ticket-add-group']; ?> *</label>
          <?php
          if (isset($form_array["FormGroup"])) {
            echo '<input type="hidden" name="category" id="category" value="'.$form_array["FormGroup"].'"">';
          } else {
            echo '<select class="ticket-add-select" name="category" id="category" >';
            foreach ($array_of_groups as $group) {

                if ($group["Def"] == 1) {

                    echo "<option value=\"".$group["Cat_ID"]."\" selected=\"selected\">".decode_entities($group["Category"])."</option>";

                } else {

                    echo "<option value=\"".$group["Cat_ID"]."\">".decode_entities($group["Category"])."</option>";

                }

            }
            echo '</select>';
          }
          ?>
        </div>

		<?php
        // if there's only one group then don't show select option
        if ($no_of_priorities == 1 || isset($form_array["FormPriority"])) {
            $display_priority_opt = "style=\"display:none\"";
        }
        ?>
        <div class="form-field" <?php echo @$display_priority_opt; ?>>
        <label for="priority"><?php echo $lang['ticket-add-priority']; ?> *</label>
        <?php
        if (isset($form_array["FormPriority"])) {
          echo '<input type="hidden" name="priority" id="priority" value="'.$form_array["FormPriority"].'"">';
        } else {
          echo '<select class="ticket-add-select" name="priority" id="priority">';
      		foreach ($array_of_priorities as $priority) {

              if ($priority["Def"] == 1) {

                echo "<option value=\"".$priority["Level_ID"]."\" selected=\"selected\">".decode_entities($priority["Level"])."</option>";

              } else {

                echo "<option value=\"".$priority["Level_ID"]."\">".decode_entities($priority["Level"])."</option>";

              }

          }
          echo '</select>';
        }
        ?>
        </div>

        <div style="display:none" id="sla" class="form-field"></div>

        <?php
        // custom fields go here
        $customfields =	aaGetFormsFields($_GET["formid"]);
    	$array_of_customfields = $customfields->fetchAll();
        unset($_SESSION["custom_fields"]);

		foreach ($array_of_customfields as $cf) {

            $fieldname = ucfirst(str_replace("_", " ", $cf["Field_Name"]));
            // add html5 required
            $fieldreq = ($cf["Field_Required"] == 1) ? 'required pattern=".*\S+.*"' : '';
            $option = explode(",",$cf["Field_Options"]);
            switch ($cf["Field_Type"]) {
                case "Text":
                echo "<div class=\"form-field\"><label>".$fieldname."</label>";
                echo "<input ".$fieldreq." name=\"custom[".$cf["FID"]."]\" type=\"text\" maxlength=\"".$cf["Field_MaxLen"]."\" value=\"".@$_POST["custom"][$cf['FID']]."\" placeholder=\"".$fieldname."\" /></div>";
                ?>
                <!-- <p class="error"><?php read_session('aaerror-custom['.$cf["Field_Name"].']'); ?></p> -->
                <?php
                break;

                case "Textbox":
                echo "<div class=\"form-field\"><label>".$fieldname."</label>";
                echo "<textarea ".$fieldreq." name=\"custom[".$cf["FID"]."]\" maxlength=\"".$cf["Field_MaxLen"]."\" placeholder=\"".$fieldname."\">".@$_POST["custom"][$cf['FID']]."</textarea>";
                ?>
                <p class="error"><?php read_session('aaerror-custom['.$cf["Field_Name"].']'); ?></p>
                <?php
                break;

                case "Select":
					echo "<div class=\"form-field\">";
                    echo "<label>".$fieldname."</label>";
                    echo "<select class=\"NoSearchSelect2\" name=\"custom[".$cf["FID"]."]\">";
                    foreach($option as $sel_opt) {

                        // if already selected
                        if ($sel_opt == $_POST["custom"][$cf['FID']]) {

                            echo "<option value=\"".$sel_opt."\" selected=\"selected\">".$sel_opt."</option>";

                        } else {

                            echo "<option value=\"".$sel_opt."\">".$sel_opt."</option>";

                        }
                    }
                    echo "</select>";
					echo "</div>";
                break;

                case "Checkbox":
                echo "<div class=\"form-field\"><label>".$fieldname."</label>";

                    // default checkbox value to check if null
                    echo "<input style=\"display:none\" hidden name=\"custom[".$cf["FID"]."][]\" type=\"checkbox\" value=\"0\" checked />";
                    foreach($option as $checkbox_opt) {

                        if (isset($_POST["custom"][$cf['FID']])) {
                            // checek if value is in field array
                            if (in_array($checkbox_opt,$_POST["custom"][$cf['FID']])) {

                                $checked = "checked";

                            } else {

                                $checked = "";

                            }

                        }

                        echo "<div class=\"form-field\"><label class=\"form-field-inline-label\">".$checkbox_opt."</label><input ".$fieldreq." name=\"custom[".$cf["FID"]."][]\" type=\"checkbox\" value=\"".$checkbox_opt."\" ".@$checked."/></div>";

                    }
                    echo "</div>";
                break;

                case "Radio":
                    echo "<p><label><strong>".$fieldname."</strong></label>";
                    foreach($option as $radio_opt) {

                        if ($radio_opt == $_POST["custom"][$cf['FID']]) {

                            echo "<div class=\"form-field\"><label class=\"form-field-inline-label\">".$radio_opt."</label><input name=\"custom[".$cf["FID"]."]\" type=\"radio\" value=\"".$radio_opt."\" checked /></div>";

                        } else if (!($_POST["custom"][$cf['Field_Name']])) {

                            echo "<div class=\"form-field\"><label class=\"form-field-inline-label\">".$radio_opt."</label><input name=\"custom[".$cf["FID"]."]\" type=\"radio\" value=\"".$radio_opt."\" checked /></div>";

                        } else {

                            echo "<div class=\"form-field\"><label class=\"form-field-inline-label\">".$radio_opt."</label><input name=\"custom[".$cf["FID"]."]\" type=\"radio\" value=\"".$radio_opt."\" /></div>";

                        }
                    }
                    echo "</p>";
                  break;

                }

        }

        //print_r($custom_set_val);

        ?>
        <?php
        // hide default message if removed in form wizard
        if ($form_array["FormSubject"] != "") {
            echo '<input name="subject" type="hidden" id="subject" value="'.$form_array["FormSubject"].'">';
        } else {
        ?>
            <div class="form-field">
            <label for="subject"><?php echo $lang['ticket-add-subject']; ?> *</label>
            <input required pattern=".*\S+.*" name="subject" type="text" id="subject" value="<?php cached_fields(@$_POST["subject"]); ?>" placeholder="Subject">
            </div>
        <?php
        }
        ?>

        <?php
        // hide default message if removed in form wizard
        if ($form_array["RemoveFormMessage"] == 0) {
        ?>
        <div class="form-field">
        <label><?php echo $lang['ticket-add-msg']; ?> *</label>
        <textarea name="notes" id="notes" rows="5" placeholder="Message"><?php if (isset($_POST["notes"])) { echo $_POST["notes"]; } ?></textarea>
        </div>
        <?php
        }
        ?>

		<?php
        $file_attachment = get_settings("File_Enabled");

        if ($file_attachment == 1) {
            if ($form_array["RemoveFormFile"] == 0) {
        ?>
        <div class="form-field">
        <label for="file"><?php echo $lang['ticket-add-files-add']; ?></label>
        <input class="aafile" name="file[]" type="file" multiple="multiple" />
        </div>
        <?php
            }
        }
        ?>

        <p><input class="btn" name="Add" type="submit" id="Add" value="<?php echo $lang["generic-save"]; ?>" /></p>

        </form>

        <div class="overlay"></div>
        <div class="popup layout-padding">

            <h3><?php echo $lang['ticket-add-search-u-sub']; ?></h3>
        	<form id="s-form">
            <div class="form-field">
            <label><?php echo $lang['ticket-add-search-u-name']; ?></label>
            <input id="s_name" name="s_name" type="text" />
            </div>

            <div class="form-field">
            <label><?php echo $lang['ticket-add-search-u-email']; ?></label>
            <input id="s_uname" name="s_uname" type="text" />
            </div>

            <div class="form-field">
            <label><?php echo $lang['ticket-add-search-u-tel']; ?></label>
            <input id="s_tel" name="s_tel" type="text" />
            </div>
            <div id="s_results"></div>
            <p>
            <input class="btn" id="s-submit" name="s-submit" type="submit" value="<?php echo $lang['search-button-submit']; ?>" />
            <input class="btn" id="s-reset" name="s-reset" type="submit" value="<?php echo $lang['ticket-add-search-u-reset']; ?>" />
            </p>

            </form>
        </div>
    </div>
</div>
