<?php
$forms = aaGetForms();
$forms_array = $forms->fetchAll();
$forms_total = $forms->rowCount();
// default form status
$formdefault = get_settings("FormWiz_DefaultFormDisable");
if ($formdefault == '1') {
	$checked_ffdisabled = 'checked';
}
// default form name selected
$formdefaultname = get_settings("FormWiz_DefaultFormName");
// save form wizard settings
if(isset($_POST['Save_FormWiz_Settings'])) {
	aaModelSaveFormWizardSettings($_POST["set_formwiz_disdefault"], $_POST["set_formwiz_default"]);
}

if(isset($_POST['form_edit'])) {

	// redirect to editting form
	header ('Location: p.php?p=settings-formwizard-form&formedit&formid='.$_POST['FID']);
	exit;

}

if(isset($_POST['form_delete'])) {

	// delete email account
	aaModelDeleteForm($_POST['FID']);

}

if(isset($_POST['form_disable'])) {

	// disable email account
	aaModelDisableForm($_POST['FID']);

}
?>
<div id="layout-body-centre" class="layout-padding form">
    <?php include 'v-settings-menu.php'; ?>
    <h2><?php echo $lang['set-formwiz-title']; ?></h2>

	<div class="layout-body-dialog">

	<p><?php echo $lang['set-formwiz-title-desc']; ?></p>
    <p><a href="p.php?p=settings-formwizard-form"><i class="fa fa-cubes" aria-hidden="true"></i> <?php echo $lang['set-formwiz-add-form']; ?></a></p>
	</div>
	<?php
	if ($forms_total > 0) {
	?>

	<h2><?php echo $lang['set-formwize-set-title']; ?></h2>
	<div class="layout-body-dialog">
		<?php
		read_session('ff_settings_saved');
		?>
		<form method="post">
			<div class="form-field">
				<label><?php echo $lang["set-formwiz-dformdis"]; ?>
					<input name="set_formwiz_disdefault" id="set_formwiz_disdefault" type="checkbox" value="1" <?php echo @$checked_ffdisabled; ?> />
				</label>
			</div>

			<div class="form-field">
				<label><?php echo $lang["set-formwiz-dform"]; ?></label>
				<select class="NoSearchSelect2" name="set_formwiz_default">
					<?php
					echo '<option value="'.$lang["set-formwiz-dform"].'">'.$lang["set-formwiz-dform"].'</option>';
					foreach ($forms_array as $ff) {
						if ($formdefaultname == $ff["FormID"]) {
							echo '<option value="'.$ff["FormID"].'" selected>'.$ff["FormName"].'</option>';
						} else {
							echo '<option value="'.$ff["FormID"].'">'.$ff["FormName"].'</option>';
						}
					}
					?>
				</select>
			</div>
			<p><input class="btn" name="Save_FormWiz_Settings" type="submit" value="<?php echo $lang["generic-save"]; ?>" /></p>
		</form>
	</div>

	<h2><?php echo $lang['set-formwize-cur-title']; ?></h2>
	<div class="layout-body-dialog">


        <table width="100%">
        <colgroup>
        <col />
        <col />
        <col />
        <col />
        </colgroup>
        <tbody>
        <?php
        foreach($forms_array as $form) {
        ?>
        <a name="<?php echo $form['FormID']; ?>"></a>
        <form method="post" action="<?php echo $_SERVER['REQUEST_URI'].'#'.$form['FormID']; ?>">
        <input style="display:none; visibility:hidden" type="text" id="FormStatus" name="FormStatus" value="<?php echo $form["Disabled"]; ?>" />
        <input style="display:none; visibility:hidden" type="text" id="FID" name="FID" value="<?php echo $form["FormID"]; ?>" />
				<input style="display:none; visibility:hidden" type="text" id="Delete_Text" name="Delete_Text" value="<?php echo $lang["set-formwiz-delete-con"]; ?>" />

				<tr class="nohover">
            <td width="100%" data-title="<?php echo $lang["set-iemail-db-ea"]; ?>"><b><?php echo $form['FormName']; ?></b></td>
            <td class="nohover" data-title="Status">
            <?php
            if ($form['Disabled'] == NULL) {
            ?>
            <button type="submit" id="form_disable" name="form_disable" title="<?php echo $lang["generic-disable"]; ?>"><i class="fa fa-lock"></i></button>
            <?php
            } else {
            ?>
            <button type="submit" id="form_disable" name="form_disable" title="<?php echo $lang["generic-enable"]; ?>"><i class="fa fa-unlock-alt"></i></button>
            <?php
            }
            ?>
            </td>
            <td data-title="Edit"><button type="submit" id="form_edit" name="form_edit" title="<?php echo $lang["generic-edit"]; ?>"><i class="fa fa-pencil-square-o"></i></button></td>
            <td data-title="Delete"><button type="submit" id="form_delete" class="form_delete" name="form_delete" title="<?php echo $lang["generic-delete"]; ?>"><i class="fa fa-trash-o"></i></button></td>
        </tr>
        <tr class="nohover">
            <td data-title="Status" colspan="4">
            <?php
            if ($form['Disabled'] == 1) {
                echo '<span class="error">'.$lang['set-formwiz-exist-dis'].'</span>';
            } else {
                echo '<span class="success">'.$lang['set-formwiz-exist-en'].'</span>';
            }
            ?>
            </td>
        </tr>
        <tr class="nohover"><td data-title="Details" colspan="4" class="detail">
        <?php
        $formfield = aaGetFormsFields($form["FormID"]);
        $formfield_array = $formfield->fetchAll();
        echo $lang['set-formwiz-fields'];
        foreach($formfield_array as $ff) {
            echo '<br>'.$ff["Field_Name"].'<span class="text-xsmall"> {'.$lang["set-custom-db-ft"].': '.$ff["Field_Type"].'; '.$lang["set-custom-db-fo"].': '.$ff["Field_Options"].'}</span>';
        }
        ?>
        <br>
        </td></tr>
        </tr>
        </form>
        <?php
        }
        ?>
        </tbody>
        </table>
	</div>
	<?php
}
 ?>
</div>
