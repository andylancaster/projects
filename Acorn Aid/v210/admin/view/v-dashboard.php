<div id="layout-body-dashboard" class="layout-padding">

    <div class="col1"><h2 class="h2-dash"><i class="fa fa-comments-o"></i> <?php echo $lang['dash-title-activity']; ?></h2></div>
    <div class="v-space">&nbsp;</div>
    <div class="col2"><h2 class="h2-dash"><?php echo $lang['dash-title-summary']; ?></h2></div>
    <div>&nbsp;</div>
    <div class="layout-body-dialog col1">

    <div id="aa-recent-activity" class="recent-activity">
    <div id="load-image"></i></div>
    </div>
    <div id="load-more"><?php echo $lang['dash-load-more']; ?></div>
    </div>

    <div class="v-space">&nbsp;</div>
    <!--
        <br />
        <div>
        <form>
        <label>View By:</label>
        <select id="dashboard-group-summary-date" name="dashboard-group-summary-date">
			<option value="today">Today</option>
			<option value="yesterday">Yesterday</option>
			<option value="this_week">This Week</option>
			<option value="last_week">Last Week</option>
			<option value="this_month">This Month</option>
        </select>
        </form>
        </div>
    -->
        <div id="dashboardsum"></div>
        <p>&nbsp;</p

</div>
</div>
