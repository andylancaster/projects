<?php
if (!isset($_GET['tid'])) {

	header('Location: p.php?p=ticket-add');

}
?>
<div id="layout-body-centre" class="layout-padding form">
	<h2><?php echo $lang["ticket-add-title"]; ?></h2>
	<div class="layout-body-dialog">
	<p><?php echo $lang['ticket-add-success-new']; ?> : <a href="p.php?p=ticket&tid=<?php echo urlencode($_GET['tid']); ?>"><strong><?php echo $_GET['tid']; ?></strong></a></p>
	<p><?php echo $lang['ticket-add-success-breif']; ?> : <strong><?php echo $_GET['ue']; ?></strong></p>
	</div>
</div>
