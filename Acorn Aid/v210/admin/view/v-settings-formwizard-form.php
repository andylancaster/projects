<div id="layout-body-centre" class="layout-padding form">
    <?php include 'v-settings-menu.php'; ?>
    <h2><?php echo $lang['set-formwiz-title']; ?></h2>
    <div class="layout-body-dialog">
    <p><?php echo $lang['set-formwiz-form-desc']; ?></p>
    <?php
    // get groups
    $groups = aaModelGetGroups();
    $array_of_groups = $groups->fetchAll();
    $no_of_groups = $groups->rowCount();

    // get priorities
    $priorities = aaModelGetPriorities();
    $array_of_priorities = $priorities->fetchAll();
    $no_of_priorities = $priorities->rowCount();

    // get custom fields
    $customfields = aaModelGetCustomFields();
    $customfields_array = $customfields->fetchAll();

    // set variable for new forms
    $editform_array = [];
    if (isset($_GET["formedit"])) {
        // get custom fields
        $editform = aaGetFormsFields($_GET["formid"]);
        $editform_array = $editform->fetchAll();
        $formname = $editform_array[0]["FormName"];
        $formdesc = $editform_array[0]["FormDesc"];
        $formgroup = $editform_array[0]["FormGroup"];
        $formpri = $editform_array[0]["FormPriority"];
        $formsub = $editform_array[0]["FormSubject"];
        $formmsg = $editform_array[0]["RemoveFormMessage"];
        $formfile = $editform_array[0]["RemoveFormFile"];


    }
    ?>

  <div id="formwiz-builder" class="column-two layout-padding">
    <form id="myForm" method="post">
        <input type="hidden" id="set_formid" name="set_formid" value="<?php echo $_GET["formid"];?>"/>
      <div class="form-field">
      <label for="set_formname"><?php echo $lang["set-formwiz-db-fn"]; ?></label>
      <input name="set_formname" type="text" id="set_formname" value="<?php cached_fields (@$_POST["set_formname"], @$formname); ?>" required pattern=".*\S+.*">
      </div>

      <div class="form-field">
      <label for="set_formdesc"><?php echo $lang['set-formwiz-db-fd']; ?></label>
      <textarea name="set_formdesc" id="set_formdesc"><?php cached_fields (@$_POST["set_formdesc"], @$formdesc); ?></textarea>
      </div>

      <div class="form-field">
      <label for="set_group"><?php echo $lang["set-formwiz-db-group"]; ?></label>
      <select class="NoSearchSelect2" name="set_group" id="set_group">
        <option value="NULL" selected>N/A</option>
        <?php
        foreach ($array_of_groups as $group) {
            if ($group["Cat_ID"] == $formgroup) {
                echo '<option value="'.$group["Cat_ID"].'" selected>'.decode_entities($group["Category"]).'</option>';
            } else {
                echo '<option value="'.$group["Cat_ID"].'">'.decode_entities($group["Category"]).'</option>';
            }
        }
        ?>
      </select>
      <i class="text-xsmall"><?php echo $lang["set-formwiz-blank"]; ?></i>
      </div>

      <div class="form-field">
      <label for="set_priority"><?php echo $lang["set-formwiz-db-pri"]; ?></label>
      <select class="NoSearchSelect2" name="set_priority" id="set_priority">
        <option value="NULL" selected>N/A</option>
        <?php
        foreach ($array_of_priorities as $priority) {
            if ($priority["Level_ID"] == $formpri) {
                echo '<option value='.$priority["Level_ID"].' selected>'.decode_entities($priority["Level"]).'</option>';
            } else {
              echo '<option value='.$priority["Level_ID"].'>'.decode_entities($priority["Level"]).'</option>';
            }
        }
        ?>
      </select>
      <i class="text-xsmall"><?php echo $lang["set-formwiz-blank"]; ?></i>
      </div>
      <div class="form-field">
      <label for="set_sub"><?php echo $lang["set-formwiz-db-sub"]; ?></label>
      <input name="set_sub" type="text" id="set_sub" value="<?php cached_fields (@$_POST["set_sub"], @$formsub); ?>">
      </div>

      <div class="form-field">
      <label for="set_msg" class="form-field-inline-label"><?php echo $lang["set-formwiz-db-msg"]; ?></label>
      <?php
      if ($formmsg == 1) {
          echo '<input type="checkbox" name="set_msg" id="set_msg" value="1" checked>';
      } else {
          echo '<input type="checkbox" name="set_msg" id="set_msg" value="1">';
      }
      ?>
      </div>

      <div class="form-field">
      <label for="set_file" class="form-field-inline-label"><?php echo $lang["set-formwiz-db-file"]; ?></label>
      <?php
      if ($formfile == 1) {
          echo '<input type="checkbox" name="set_file" id="set_file" value="1" checked>';
      } else {
          echo '<input type="checkbox" name="set_file" id="set_file" value="1">';
      }
      ?>
      </div>

      <div class="form-field">
        <label for="set_formname"><?php echo $lang["set-formwiz-db-sf"]; ?><br><i class="text-xsmall"><?php echo $lang['set-formwiz-instr']; ?></i></label>
      <ul id="sortable1" class="connectedSortable">
          <?php
          // fields in edit for comparing available fields
          $efid = [];
          foreach ($editform_array as $ecf) {
              if (isset($ecf["FID"])) {
              echo '<li class="f ui-state-highlight" fid="'.$ecf["FID"].'">'.aaChgFormFields ($ecf["FID"]).' <span id="formwiz-field-text"><b>'.$ecf["Field_Name"].'</b> <span class="text-xsmall">{'.$lang["set-custom-db-ft"].': '.$ecf["Field_Type"].';}</span></span></li>';
              $efid[] = $ecf["FID"];
            }
          }
          ?>
      </ul>
    </div>

  <div class="form-field">
      <p><input id="formwiz-save" class="btn" type="submit"/></p>
  </div>

  </form>
  </div>
  <div id="formwiz-fields" class="column-two layout-padding">
  <a href="p.php?p=settings-customfields"><?php echo $lang["set-formwiz-addeditdel"]; ?></a>
  <ul id="sortable2" class="connectedSortable">
    <?php
    foreach ($customfields_array as $cf) {
        if (!in_array($cf["FID"], $efid)) {
        echo '<li class="f ui-state-highlight" fid="'.$cf["FID"].'"><span id="formwiz-field-text"><b>'.$cf["Field_Name"].'</b> <span class="text-xsmall">{'.$lang["set-custom-db-ft"].': '.$cf["Field_Type"].';}</span></span></li>';
        }
    }
    ?>
  </ul>
  </div>
  </div>

</div>
