<div id="layout-body-centre" class="layout-padding form">

    <?php include 'v-settings-menu.php'; ?>
    <h2><?php echo $lang["set-general-title"]; ?></h2>
    <div class="layout-body-dialog">
    <?php
    if (isset($_POST["Save_General"])) {
    aaModelSaveSettingsGeneral($_POST["set_com_name"], $_POST["set_full_url_path"], $_POST["lang"], $_POST["set_timezone"], $_POST["set_dateformat"], $_POST["set_redirect_to"]);
    }
    ?>
    <form action="<?php echo $_SERVER["REQUEST_URI"]; ?>" method="post">

    <div class="form-field">
    <label for="set_com_name"><?php echo $lang["set-general-db-cn"]; ?> *</label>
    <input required pattern=".*\S+.*" name="set_com_name" type="text" value="<?php echo get_settings("Company_Name"); ?>" />
    </div>

    <div class="form-field">
    <label for="set_full_url_path"><?php echo $lang["set-general-db-fp"]; ?> *</label>
    <input required pattern=".*\S+.*" name="set_full_url_path" type="url" value="<?php echo get_settings("Full_URL_Path"); ?>" />
    </div>

    <div class="form-field">
    <label><?php echo $lang["set-general-db-fl"]; ?></label>
    <?php
    select_langauge ();
    ?>
    </div>

    <div class="form-field">
    <label><?php echo $lang["set-general-db-tz"]; ?></label>
    <select class="NoSearchSelect2" name="set_timezone">
    <?php
    $dbtimezone = get_settings("Timezone");

        foreach (aaModelAllTimezones() as $timezone) {

            if ($dbtimezone == $timezone) {

                echo "<option value=\"".$dbtimezone."\" selected=\"selected\">".$dbtimezone."</option>";

            } else {

                echo "<option value=\"".$timezone."\">".$timezone."</option>";

            }
        }
    ?>
    </select>
    </div>

    <?php
    $dbdateformat = get_settings("Date_Format");
    ?>
    <div class="form-field">
    <label for="set_dateformat"><?php echo $lang["set-general-db-df"]; ?> *</label>
    <input required pattern=".*\S+.*" name="set_dateformat" type="text" id="set_dateformat" value="<?php echo $dbdateformat; ?>">
    </div>
    <div class="form-field text-xsmall">
      <?php echo $lang['settings-tickets-sql-guide']; ?>
    </div>

    <div class="form-field">
    <label><?php echo $lang["set-general-db-rp"]; ?></label>
    <select class="NoSearchSelect2" name="set_redirect_to">
        <?php
        $redirect_page = get_settings("Redirect_Page");
        $pages = array($lang['nav-dashboard'] => "p.php?p=dashboard", $lang['nav-tickets'] => "p.php?p=tickets");

        foreach ($pages as $pagekey => $pagevalue) {

            if ($redirect_page == $pagevalue) {

                echo "<option value=".$pagevalue." selected=\"selected\">".$pagekey."</option>";

            } else {

                echo "<option value=".$pagevalue.">".$pagekey."</option>";

            }
        }
        ?>
    </select>
    </div>
    <p><input class="btn" name="Save_General" type="submit" value="<?php echo $lang['generic-save']; ?>" /></p>
    </form>
    </div>
</div>
