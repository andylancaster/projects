<?php
// delete custom field
function aaModelDeleteCustomField ($custom_id, $custom_name) {

	global $pdo_conn, $pdo_t;

	// run sql to delete priority record
	$sql_delete_options = "DELETE FROM ".$pdo_t['t_custom_fields']." WHERE FID = :custom_id";

	$q = $pdo_conn->prepare($sql_delete_options);
	$q->execute(array(":custom_id" => $custom_id));

	// drop table column
	$sql_drop_custom = "ALTER TABLE ".$pdo_t['t_ticket']." DROP $custom_name";
	$q = $pdo_conn->prepare($sql_drop_custom);
	$q->execute();

	// refresh page and send to priorities section
	header('Location: '.$_SERVER['REQUEST_URI']);

}
?>
