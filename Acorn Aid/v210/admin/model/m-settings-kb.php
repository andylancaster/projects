<?php
// update kb settings
function aaModelSaveKBSettings($kb_on, $kb_author, $kb_count, $kb_rating, $kb_coms, $kb_comsg, $kb_comsa, $kb_share, $kb_showx) {

	global $pdo_conn, $pdo_t, $lang;

	$kb_on = (isset($kb_on)) ? 1 : 0;
	$kb_author = (isset($kb_author)) ? 1 : 0;
	$kb_count = (isset($kb_count)) ? 1 : 0;
	$kb_rating = (isset($kb_rating)) ? 1 : 0;
	$kb_coms = (isset($kb_coms)) ? 1 : 0;
	$kb_comsg = (isset($kb_comsg)) ? 1 : 0;
	$kb_comsa = (isset($kb_comsa)) ? 1 : 0;
	$kb_share = (isset($kb_share)) ? 1 : 0;

	$sql = "UPDATE ".$pdo_t['t_settings']." SET KB_Enable=:kb_on,
	KB_Author_Allow=:kb_author,
	KB_Count=:kb_count,
	KB_Rating=:kb_rating,
	KB_Comments=:kb_coms,
	KB_Comments_Guests=:kb_comsg,
	KB_Comments_Approval=:kb_comsa,
	KB_Share=:kb_share,
	KB_Showx=:kb_showx
	LIMIT 1";
	$q = $pdo_conn->prepare($sql);

	if($q->execute(array('kb_on' => $kb_on,
	'kb_author' => $kb_author,
	'kb_count' => $kb_count,
	'kb_rating' => $kb_rating,
	'kb_coms' => $kb_coms,
	'kb_comsg' => $kb_comsg,
	'kb_comsa' => $kb_comsa,
	'kb_share' => $kb_share,
	'kb_showx' => $kb_showx))) {
		echo '<div class="success-msg">'.$lang['generic-settings-saved'].'</div>';
	}

}
?>
