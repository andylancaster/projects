<?php
// save form wizard settings
function aaModelSaveFormWizardSettings($ffdisabledef, $ffdefformname) {

	global $pdo_conn, $pdo_t, $lang;

	$ffdisabledef = (isset($ffdisabledef)) ? 1 : 0;

	$update_ffset = "UPDATE ".$pdo_t['t_settings']." SET
	FormWiz_DefaultFormDisable = :ffdisabledef,
	FormWiz_DefaultFormName = :ffdefformname
	LIMIT 1";
	$q = $pdo_conn->prepare($update_ffset );

	if ($q->execute(array("ffdisabledef" => $ffdisabledef,
						"ffdefformname" => $ffdefformname))) {

		set_session('ff_settings_saved', '<div class="success-msg">'.$lang['generic-settings-saved'].'</div>');
		header("Location:".$_SERVER["REQUEST_URI"]);
		exit();

	}

}

// disable form
function aaModelDisableForm($formid) {

	global $pdo_conn, $pdo_t;

	$sql = "UPDATE ".$pdo_t['t_formwiz_forms']." SET Disabled = CASE
				WHEN Disabled IS NULL THEN 1
				WHEN Disabled = 1 THEN NULL
				ELSE NULL
				END
				WHERE FormID = :formid";

	$q = $pdo_conn->prepare($sql);
	$q->execute(array('formid' => $formid));

	header('Location: '.$_SERVER['REQUEST_URI']);
	exit;

}

// delete form and fields
function aaModelDeleteForm($formid) {

	global $pdo_conn, $pdo_t;

	// delete form
	$sql = "DELETE FROM ".$pdo_t['t_formwiz_forms']." WHERE FormID = :formid";
	$q = $pdo_conn->prepare($sql);
	$q->execute(array('formid' => $formid));

	// delete form structure
	$sqlff = "DELETE FROM ".$pdo_t['t_formwiz_form_fields']." WHERE FormID = :formid";
	$qff = $pdo_conn->prepare($sqlff);
	$qff->execute(array('formid' => $formid));

	// delete form structure
	$sqlfv = "DELETE FROM ".$pdo_t['t_formwiz_form_values']." WHERE FormID = :formid";
	$qfv = $pdo_conn->prepare($sqlfv);
	$qfv->execute(array('formid' => $formid));

	// set form ID to NULL to remove ticket association with ticket
	$sqlut = "UPDATE ".$pdo_t['t_ticket']." SET FormID = NULL WHERE FormID = :formid";
	$qut = $pdo_conn->prepare($sqlut);
	$qut->execute(array('formid' => $formid));

	header('Location: '.$_SERVER['REQUEST_URI']);
	exit;

}
?>
