<?php
// list all timezones available
function aaModelAllTimezones() {
	return $timezone_identifiers = DateTimeZone::listIdentifiers();
}


// update general settings
function aaModelSaveSettingsGeneral($companyname, $fullurlpath, $default_lang, $timezone, $dateformat, $redirectto) {

	global $pdo_conn, $pdo_t, $lang;

	$companyname = clean($companyname,TRUE);

	// update database with new values
	$update_general = "UPDATE ".$pdo_t['t_settings']." SET Company_Name=:companyname, Full_URL_Path=:fullurlpath, Langauge=:default_lang, Timezone=:timezone, Date_Format=:dateformat, Redirect_Page=:redirectto LIMIT 1";
	$q = $pdo_conn->prepare($update_general);

	if ($q->execute(array("companyname" => $companyname, "fullurlpath" => $fullurlpath, "default_lang" => $default_lang, "timezone" => $timezone, "dateformat" => $dateformat, "redirectto" => $redirectto))) {

		echo '<div class="success-msg">'.$lang['generic-settings-saved'].'</div>';

		// write path for chat
		$filename = '../chat/aa-chat.js';
		$lines = file($filename);
		$lines[1] = 'var fullpath = "'.$fullurlpath.'/chat/";'."\n";
		$aachatjs = implode("", $lines);

		$fp = fopen($filename, 'w');
		fwrite($fp, $aachatjs);

	}

}
?>
