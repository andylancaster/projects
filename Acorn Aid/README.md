## Acorn Aid

Acorn Aid is a free downloadable help desk solution developed by myself over a number of years.

Full details can be found at [http://acornaid.com/]

Acorn Aid is built on programming languages PHP, HTML, MySQL, CSS, and JQuery